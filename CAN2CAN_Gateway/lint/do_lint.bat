@rem arguments for this batch file: 
@rem %1: The path to the project folder
@rem %2: The path to the CodeWarrior installation folder
@rem ------------------------------------------------------
@rem Path to my project folder
SET PROJ_PATH=%1
@rem Path to CodeWarrior installation folder (which is e.g. "C:\Freescale\CW MCU v10.2\eclipse\..\MCU")
SET CW_PATH=%2
@rem Path to lint-nt.exe
SET LINT_EXE=C:\lint\lint-nt.exe
@rem Path to my lint configuration files
SET LOCAL_LNT_FILES=C:\lint\fsl-lint
@rem Path to my local lint folder inside the project
SET PROJ_LINT_PATH=%PROJ_PATH%\lint
@rem Lint configuration files and includes
SET LNT_INCLUDES=-i"%LOCAL_LNT_FILES%" "%LOCAL_LNT_FILES%\co-gcc.lnt" -i%LOCAL_LNT_FILES%
@rem --------------- Run PC-lint ---------------------------
%LINT_EXE% %LNT_INCLUDES% %PROJ_LINT_PATH%\proj_options.lnt %PROJ_LINT_PATH%\proj_files.lnt -vf
@rem - %LINT_EXE% %LNT_INCLUDES% %PROJ_LINT_PATH%\proj_options.lnt %PROJ_LINT_PATH%\proj_files.lnt -vf