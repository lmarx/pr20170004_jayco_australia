/**---------------------------------------------------------------------------------------------------------------------
 * @file            Application.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Jan 23, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#include "Application.h"


// TODO
#include "stdio.h"

#include "IDS-Core\global.h"

void vApplicationInitTasks(void)
{

	vAppCAN0TaskInit();
	vAppCAN1TaskInit();

#ifdef INCLUDE_IDS_CORE_CAN
	vAppIDSCoreTaskInit();
#endif // INCLUDE_IDS_CORE_CAN

#ifdef INCLUDE_ETHERNET_SUPPORT
	vAppEthernetTaskInit();
#endif // INCLUDE_ETHERNET_SUPPORT

#ifdef DEBUG_TEST_SEQUENCE_STATE_MACHINE
	vAppSequenceTaskInit();
#endif // DEBUG_TEST_SEQUENCE_STATE_MACHINE

	vAppWatchdogTaskInit(); // Always start watchdog task last

	return;
}



// this callback gets all IDS_CAN_MESSAGE_TYPE_COMMAND messages on the bus (not just messages for us)
// we need to determine if the message is for us, and is formatted properly
// although we are 5 devices, only SETEC_POWER_MANAGER accepts commands
//
// the SETEC_POWER_MANAGER command looks like this:
//        len = 1
//        byte[0] = command
//                  0 = off ???
//                  1 = on ???

//static uint8 test_data;

void vApplication_OnIDSCanCommandMessageRx( const IDS_CAN_RX_MESSAGE *xIdsCanMsg )
{
	UBaseType_t NbItemsInQueue;
	BaseType_t xReturn;
	//xPID_DATA_LINK_WITH_DEVICE xPID_DataRead;
	xAPPCAN1_DATA xAppCan1_Data;

	ASSERT(xIdsCanMsg->MessageType == IDS_CAN_MESSAGE_TYPE_COMMAND);

	xAppCan1_Data.u8All_output_off = 0xFF;

	// ignore broadcast commands
	if (xIdsCanMsg->TargetAddress == IDS_CAN_ADDRESS_BROADCAST) return; // ignore broadcasts
	if (xIdsCanMsg->SourceAddress == IDS_CAN_ADDRESS_BROADCAST) return; // ignore broadcast commands

	// verify this is a 1 byte command message
	if (xIdsCanMsg->Length != 1)  return;

	// is the target address of the command one of our devices?
#if FALSE
	device = IDS_CAN_GetDeviceFromAddress(xIdsCanMsg->TargetAddress);
	if (device >= NUM_IDS_CAN_DEVICES) return; // out of range == message is not for a device in my firmware
	if (device != SETEC_POWER_MANAGER) return; // we only accept COMMANDs for the SETEC_POWER_MANAGER
#else
	if (IDS_CAN_GetDeviceAddress(SETEC_POWER_MANAGER) != xIdsCanMsg->TargetAddress) return;
#endif

	// ignore the command if the session is not open
	if (IDS_CAN_GetSessionOwner(SETEC_POWER_MANAGER, IDS_CAN_SESSION_ID_REMOTE_CONTROL) != xIdsCanMsg->SourceAddress) return;

	// OK, things look good so I can accept the command
	// DoSomethingWith(xIdsCanMsg->Data[0]); // handle the command payload

	NbItemsInQueue = FRTOS1_uxQueueMessagesWaiting(qAppCan1_data_Queue); // number of items that are currently in a queue.
	if (NbItemsInQueue != 0)
	{
		xReturn = FRTOS1_xQueueReceive( qAppCan1_data_Queue, &xAppCan1_Data, 0 ); // The item that is read and then removed from the queue.
		ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
	}

	if ((xIdsCanMsg->Data[0] == 0x1) && (xAppCan1_Data.u8All_output_off == 0xFF))
	{
		/* Request All outputs disabled */
		xAppCan1_Data.u8All_output_off = 0x00; /* All outputs disabled */
	}
	else
	{
		/* Request All outputs enabled */
		xAppCan1_Data.u8All_output_off = 0x01; /* All outputs enabled */
	}

	xReturn = FRTOS1_xQueueSendToFront( qAppCan1_data_Queue, &xAppCan1_Data, 0 );
	ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	return;
}


/*

// SRS0 Bit Fields
#define RCM_SRS0_WAKEUP_MASK                     0x1u
#define RCM_SRS0_WAKEUP_SHIFT                    0
#define RCM_SRS0_LVD_MASK                        0x2u
#define RCM_SRS0_LVD_SHIFT                       1
#define RCM_SRS0_LOC_MASK                        0x4u
#define RCM_SRS0_LOC_SHIFT                       2
#define RCM_SRS0_LOL_MASK                        0x8u
#define RCM_SRS0_LOL_SHIFT                       3
#define RCM_SRS0_WDOG_MASK                       0x20u
#define RCM_SRS0_WDOG_SHIFT                      5
#define RCM_SRS0_PIN_MASK                        0x40u
#define RCM_SRS0_PIN_SHIFT                       6
#define RCM_SRS0_POR_MASK                        0x80u
#define RCM_SRS0_POR_SHIFT                       7
// SRS1 Bit Fields
#define RCM_SRS1_JTAG_MASK                       0x1u
#define RCM_SRS1_JTAG_SHIFT                      0
#define RCM_SRS1_LOCKUP_MASK                     0x2u
#define RCM_SRS1_LOCKUP_SHIFT                    1
#define RCM_SRS1_SW_MASK                         0x4u
#define RCM_SRS1_SW_SHIFT                        2
#define RCM_SRS1_MDM_AP_MASK                     0x8u
#define RCM_SRS1_MDM_AP_SHIFT                    3
#define RCM_SRS1_EZPT_MASK                       0x10u
#define RCM_SRS1_EZPT_SHIFT                      4
#define RCM_SRS1_SACKERR_MASK                    0x20u
#define RCM_SRS1_SACKERR_SHIFT                   5
 */

void vAppTestCANBuffering(void)
{
	uint8 u8DataToSend[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xFrame;
	uint8 i;

	memcpy( &xFrame.u8Data[0], &u8DataToSend[0], 8 );

	xFrame.xLddCanRxFrame.Data = &xFrame.u8Data[0];
	xFrame.xLddCanRxFrame.FrameType = LDD_CAN_DATA_FRAME;
	xFrame.xLddCanRxFrame.Length = 8;
	xFrame.xLddCanRxFrame.LocPriority = 0;
	xFrame.xLddCanRxFrame.MessageID = 0x9C0300FF;
	xFrame.xLddCanRxFrame.TimeStamp = 0;

	for( i = 0; i < 40; i++ )
	{
		xFrame.u8Data[0] = i;
		vAppCANTransmitRaw( &xFrame );
	}

	return;
}


void vDoNothingFn(void)
{
	// This is for development prior to HAL abstraction
	return;
}

