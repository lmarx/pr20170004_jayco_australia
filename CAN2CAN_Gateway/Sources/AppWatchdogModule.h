/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppWatchdogModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Sep 22, 2015
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPWATCHDOGMODULE_C_
#define SOURCES_APPWATCHDOGMODULE_C_

void vAppWatchdogTaskInit(void);
TaskHandle_t xAppWatchdogGetTaskHandle(void);

#endif // SOURCES_APPWATCHDOGMODULE_C_
