/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppWatchdogModule.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Sep 22, 2015
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"
#include "IDS-Core\global.h"

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
// Task Parameters
static const uint8 cMY_TASK_PRIORITY            = ( configMAX_PRIORITIES );
static const uint16 cMY_TASK_STACK_SIZE         = 96;
static volatile uint32_t u32MyTaskWdgCheck      = FALSE;
static TaskHandle_t xMyTaskHandle               = NULL;

static const uint16 cMY_TASK_LOOP_DELAY         = ( 3000 / portTICK_PERIOD_MS );    // 3 seconds



/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskWatchdog( void *pvParams ) __attribute__ ((noreturn));
static bool boWatchdogCheckAllOtherTasks(void);
static void vWatchdogClearTaskWdgFlags(void);
//static void vTempRelayTest(void);


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */



/**---------------------------------------------------------------------------------------------------------------------
 * @func        vAppWatchdogTaskInit(void)
 *
 * @brief       Init the Watchdog checking task
 *
 * @desc        Init the Watchdog during app startup prior to starting the kernel.
 *
 * @param [in]  None
 *
 * @return      None
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vAppWatchdogTaskInit(void)
{
	BaseType_t xReturn;

	// Finally, create the RTC Task...
	xReturn = FRTOS1_xTaskCreate(             vAppTaskWatchdog,
			( const portCHAR * const )"WdgTask",
			cMY_TASK_STACK_SIZE,
			(void *)&u32MyTaskWdgCheck,
			cMY_TASK_PRIORITY,
			&xMyTaskHandle  );

	ASSERT(xReturn == pdTRUE); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	return;
}




TaskHandle_t xAppWatchdogGetTaskHandle(void)
{
	return xMyTaskHandle;
}

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */

static void vAppTaskWatchdog( void *pvParams )
{
	volatile uint32_t *pu32TaskIsRunning;
	LDD_TError xReturn;

	// set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32_t *)pvParams;

	// Let all other tasks get up and running
	FRTOS1_vTaskDelay( cMY_TASK_LOOP_DELAY );

	// Enable the Hardware Watchdog Reset
#ifndef WATCHDOG_DISABLE
	xReturn = WDog1_Enable();
	if (xReturn != ERR_OK)
	{
		// TBD
	}
#endif // WATCHDOG_DISABLE

	for(;;)
	{
		// Check each task's watchdog variable
		if( TRUE == boWatchdogCheckAllOtherTasks() )
		{
			// All other tasks are still running.
			vWatchdogClearTaskWdgFlags();
		}
		else
		{
			// Someone died!

#ifndef WATCHDOG_DISABLE
			for(;;) /* Infinite loop */
			{
				ASSERT(FALSE); /*lint !e506 !e774 #ASSERT FAIL ALL the time */
				ForceReset();
				/* HW WDT must come...*/
			}
#endif // WATCHDOG_DISABLE

		}

		// Update our watchdog variable
		portENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		portEXIT_CRITICAL();

		// Check every 3 second
		FRTOS1_vTaskDelay( cMY_TASK_LOOP_DELAY );


	} // Lather, rinse, repeat

	// Does not return
}




static bool boWatchdogCheckAllOtherTasks(void)
{
	bool boReturn = TRUE;

#ifdef  DEBUG_TEST_SEQUENCE_STATE_MACHINE
	if( TRUE != boAppSequenceWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}
#endif // DEBUG_TEST_SEQUENCE_STATE_MACHINE


	if( TRUE != boCAN0_RxWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}

	if( TRUE != boCAN0_TxWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}

	if( TRUE != boCAN1_RxWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}

	if( TRUE != boCAN1_TxWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}

	if( TRUE != boCAN1_100MsWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}


#ifdef INCLUDE_IDS_CORE_CAN
	if( TRUE != boIDSCoreWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}
#endif // INCLUDE_IDS_CORE_CAN


#ifdef INCLUDE_ETHERNET_SUPPORT
	if( TRUE != boEthernetRxWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}

	if( TRUE != boEthernetTxWatchdogTaskCheck() )
	{
		boReturn = FALSE;
	}
#endif // INCLUDE_ETHERNET_SUPPORT

	return boReturn;
}

static void vWatchdogClearTaskWdgFlags(void)
{

#ifdef  DEBUG_TEST_SEQUENCE_STATE_MACHINE
	vAppSequenceWatchdogTaskClear();
#endif // DEBUG_TEST_SEQUENCE_STATE_MACHINE

	vCAN0_RxWatchdogTaskClear();
	vCAN0_TxWatchdogTaskClear();
	vCAN1_RxWatchdogTaskClear();
	vCAN1_TxWatchdogTaskClear();
	vCAN1_100MsWatchdogTaskClear();

#ifdef INCLUDE_IDS_CORE_CAN
	vIDSCoreWatchdogTaskClear();
#endif // INCLUDE_IDS_CORE_CAN

#ifdef INCLUDE_ETHERNET_SUPPORT
	vEthernetRxWatchdogTaskClear();
	vEthernetTxWatchdogTaskClear();
#endif // INCLUDE_ETHERNET_SUPPORT

	return;
}
