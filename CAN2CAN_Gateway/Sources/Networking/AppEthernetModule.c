/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppEthernetModule
 *
 * @brief           TCP / UDP networking for IDS Production Testers and utility hardware
 *
 * @date            March 14, 2015
 *
 * @author
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"
#include "NetworkConfigDefaults.h"


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */

//#include "gpio1.h"
//#include "FreeRTOS_IP.h"
//#include "FreeRTOS_Sockets.h"
//#include "FreeRTOS_DHCP.h"
//#include "FreeRTOS_IP_Private.h"
//#include "NetworkInterface.h"
//#include <stdio.h>

#if TCP_MAX_CONNECTION_CNT > 4
#error TCP_MAX_CONNECTION_CNT must be 4 or less.
#endif

#define SOCK_INFO_QUEUE_LENGTH          TCP_MAX_CONNECTION_CNT
#define SOCK_INFO_QUEUE_ITEM_SIZE       sizeof( Socket_Conn_Info_t )
#define CAN_MSG_QUEUE_LENGTH            ( 64 )
#define CAN_MSG_QUEUE_ITEM_SIZE         sizeof( CAN_Msg_And_Socket_t )
#define RX_MSG_SOCK_INDEX               ( 0xFE )
#define IDS_CAN_TX_SOCK_INDEX           ( 0xFF )



// struct for passing connection info to threads
typedef struct
{
    Socket_t socket;
    uint8 index;
    uint8 active;
} Socket_Conn_Info_t;


// struct for storing CAN messages as well as the socket they came from
typedef struct
{
    uint8 socketIndex;
    CAN_RX_MESSAGE message;
} CAN_Msg_And_Socket_t;


// struct for the arguments of the tcp worker threads
typedef struct
{
    Socket_t socket;
    uint8 socketIndex;
    QueueHandle_t responseQueue;
} TCP_Worker_Thread_Args_t;


// struct for the arguments of the tcp tx threads
typedef struct
{
    Socket_t socket;
    uint8 socketIndex;
    QueueHandle_t canMsgQueue;
} TCP_Tx_Thread_Args_t;



/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
static const uint8  cMY_RX_TASK_PRIORITY           = ( configMAX_PRIORITIES - 3 );
static const uint16 cMY_RX_TASK_STACK_SIZE         = 200;
static volatile uint32 u32MyRxTaskWdgCheck        = FALSE;
static TaskHandle_t xMyRxTaskHandle               = NULL;

static const uint8  cMY_TX_TASK_PRIORITY           = ( configMAX_PRIORITIES - 3 );
static const uint16 cMY_TX_TASK_STACK_SIZE         = 256;
static volatile uint32 u32MyTxTaskWdgCheck        = FALSE;
static TaskHandle_t xMyTxTaskHandle               = NULL;

static QueueHandle_t Sock_Info_queue;
static QueueHandle_t CAN_Msg_queue[TCP_MAX_CONNECTION_CNT];
static uint8 gu8EthMACAddress[6] = { IDS_MAC_PREFIX_1, IDS_MAC_PREFIX_2, IDS_MAC_PREFIX_3, 0x00, 0x00, 0x00 };
static uint8 TCPConnectionCnt = 0;
static uint8 socketInActive[TCP_MAX_CONNECTION_CNT];
static uint32 NetQueueErrCnt = 0;


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskTcpRx( void *pvParams );
static void vAppTaskTcpTx( void *pvParams );
static void vSendMsgToCANQueues( CAN_Msg_And_Socket_t *msg );
static void TCP_Process_Special_Msg( const uint8 * message );
static void vTaskTcpWorker( void *pvParams );
static void vAppTaskUdpBcast( void *pvParams );
static void Network_OnCANMessageTx_Sock0( CAN_RX_MESSAGE *message );
static void Network_OnCANMessageTx_Sock1( CAN_RX_MESSAGE *message );
static void Network_OnCANMessageTx_Sock2( CAN_RX_MESSAGE *message );
static void Network_OnCANMessageTx_Sock3( CAN_RX_MESSAGE *message );


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vAppEthernetTaskInit(void)
{
    BaseType_t taskCreationStatus;
    uint8_t i = 0;

    uint8_t ip_addr[ipIP_ADDRESS_LENGTH_BYTES] = HARDCODED_IP_ADDRESS;
    uint8_t net_mask[ipIP_ADDRESS_LENGTH_BYTES] = HARDCODED_NET_MASK;
    uint8_t gw_addr[ipIP_ADDRESS_LENGTH_BYTES] = HARDCODED_GATEWAY;
    uint8_t dns_addr[ipIP_ADDRESS_LENGTH_BYTES] = HARDCODED_DNS_SERV;
    MACAddress[3] = FlashStorage.MACSuffix[0];
    MACAddress[4] = FlashStorage.MACSuffix[1];
    MACAddress[5] = FlashStorage.MACSuffix[2];

    // Initialize the networking stack
    FreeRTOS_IPInit( ip_addr, net_mask, gw_addr, dns_addr, MACAddress );

    // Create the queue for passing connection information from the rx thread to the tx thread
    Sock_Info_queue = FRTOS1_xQueueCreate( SOCK_INFO_QUEUE_LENGTH, SOCK_INFO_QUEUE_ITEM_SIZE );
    if( Sock_Info_queue == NULL )
    {
        system_unrecoverable_error();
    }

    // Create the queues for storing CAN messages
    for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
    {
        CAN_Msg_queue[i] = FRTOS1_xQueueCreate( CAN_MSG_QUEUE_LENGTH, CAN_MSG_QUEUE_ITEM_SIZE );
        if( CAN_Msg_queue[i] == NULL )
        {
            system_unrecoverable_error();
        }
    }




    // Create the Rx task
    taskCreationStatus = FRTOS1_xTaskCreate( vAppTaskTcpRx,
                                              "TCPReceive",
                                    cMY_RX_TASK_STACK_SIZE,
                              (void *)&u32MyRxTaskWdgCheck,
                                      cMY_RX_TASK_PRIORITY,
                                          &xMyRxTaskHandle );

    if( NULL == taskCreationStatus )
    {
        system_unrecoverable_error();
    }

    // Create the UDP broadcast task
    taskCreationStatus = FRTOS1_xTaskCreate(    vAppTaskUdpBcast,
                                                      "UDPBCast",
                                          cMY_TX_TASK_STACK_SIZE,
                                    (void *)&u32MyTxTaskWdgCheck,
                                            cMY_TX_TASK_PRIORITY,
                                                &xMyTxTaskHandle );

    if( NULL == taskCreationStatus )
    {
        system_unrecoverable_error();
    }

    return;
}




bool boEthernetRxWatchdogTaskCheck(void)
{
    bool boReturn = FALSE;

    // Verify that our task is still running by testing its watchdog variable
    if( u32MyRxTaskWdgCheck )
    {
        // The watchdog has changed so everything is good.
        boReturn = TRUE;
    }

    return boReturn;
}




bool boEthernetTxWatchdogTaskCheck(void)
{
    bool boReturn = FALSE;

    // Verify that our task is still running by testing its watchdog variable
    if( u32MyTxTaskWdgCheck )
    {
        // The watchdog has changed so everything is good.
        boReturn = TRUE;
    }

    return boReturn;
}




void vEthernetRxWatchdogTaskClear(void)
{
    // Reset the check variable so we can tell if it has been set by the next time around.
    u32MyRxTaskWdgCheck = FALSE;
}




void vEthernetTxWatchdogTaskClear(void)
{
    // Reset the check variable so we can tell if it has been set by the next time around.
    u32MyTxTaskWdgCheck = FALSE;
}




void Network_OnCANMessageRx( const CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;

    msgAndSock.message = *message;
    // for RX messages, use an invalid socket index.  this way,
    // we will never tell any socket that this is their echo
    msgAndSock.socketIndex = RX_MSG_SOCK_INDEX;

    vSendMsgToCANQueues( &msgAndSock );

    // toggle the amber status light every time we receive a message
    GPIO_DRV_TogglePinOutput( LED1_Control );
}




void Network_OnIDSCanMessageTx( const IDS_CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;

    msgAndSock.message.Length = message->Length;
    msgAndSock.message.Data[0] = message->Data[0];
    msgAndSock.message.Data[1] = message->Data[1];
    msgAndSock.message.Data[2] = message->Data[2];
    msgAndSock.message.Data[3] = message->Data[3];
    msgAndSock.message.Data[4] = message->Data[4];
    msgAndSock.message.Data[5] = message->Data[5];
    msgAndSock.message.Data[6] = message->Data[6];
    msgAndSock.message.Data[7] = message->Data[7];
    msgAndSock.message.Timestamp_ms = message->Timestamp_ms;
    msgAndSock.message.ID = IDS_CAN_PackID( &message->ID );

    // this is a message that was TX'd from the IDS-CAN layer, it is not
    // an echo to any source
    msgAndSock.socketIndex = IDS_CAN_TX_SOCK_INDEX;

    vSendMsgToCANQueues( &msgAndSock );
}




BaseType_t xApplicationDHCPUserHook( eDHCPCallbackQuestion_t eQuestion,
            uint32_t ulIPAddress,
            uint32_t ulNetMask )
{
    BaseType_t xResult = (BaseType_t)eDHCPContinue;

    switch( eQuestion )
    {
        case eDHCPOffer : /* Driver is about to ask for a DHCP offer. */
        case eDHCPRequest : /* Driver is about to request DHCP an IP address. */
            if( FlashStorage.UseDHCP[0] != 1 )
            {
                /* Return eDHCPUseDefaults to abort the DHCP process. */
                xResult = (BaseType_t)eDHCPUseDefaults;
            }
            break;
            /* In all other instances, return eDHCPContinue. */
    }

    return xResult;
}




const uint8* pEthernet_GetAdapterMAC( void )
{
    return &gu8EthMACAddress[0];
}




void Network_DHCP_Failed( void )
{
    // The network stack has failed to obtain a DHCP address.  If we are configured to use DHCP,
    // restart the process by forcing the stack to initialize again.
    if( FlashStorage.UseDHCP[0] == 1 )
    {
        // we are configured to use DHCP.  Force the stack to try initializing again by
        // telling it the network driver went down
        FreeRTOS_NetworkDown();
    }
}




uint8 TCP_GetConnectionCount( void )
{
    return TCPConnectionCnt;
}




uint32 TCP_GetPacketsRX( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxTotal;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTX( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxTotal;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXMissed( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxMissed;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXDiscarded( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxDiscard;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXError( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxError;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXMissed( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxMissed;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXDiscarded( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxDiscard;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXError( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxError;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXAlign( void )
{
    enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxAlign;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXFcs( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxFcs;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXTruncate( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxTruncate;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXLenErr( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxLengthGreater;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXCollision( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxCollision;

    return Reverse32( val );
}




uint32 TCP_GetPacketsRXOverrun( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsRxOverRun;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXOverflow( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxOverFlow;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXLateCollision( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxLateCollision;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXExcessCollision( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxExcessCollision;

    return Reverse32( val );
}




uint32 TCP_GetPacketsTXUnderflow( void )
{
    const enet_stats_t * stats = enet_get_stats();
    uint32 val = stats->statsTxUnderFlow;

    return Reverse32( val );
}




uint32 TCP_GetQueueErrCnt( void )
{
    return Reverse32( NetQueueErrCnt );
}



/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (Module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskTcpRx( void *pvParams )
{
    uint32_t i = 0;
    Socket_t socket;
    Socket_t socketIn[TCP_MAX_CONNECTION_CNT];
    TaskHandle_t txTaskHandles[TCP_MAX_CONNECTION_CNT] = { NULL };
    struct freertos_sockaddr socketAddress;
    struct freertos_sockaddr socketAddressIn;
    socklen_t addrLen;
    TickType_t timeout;
    uint32_t result = 0;

#define WORKER_THREAD_QUEUE_LENGTH TCP_MAX_CONNECTION_CNT
#define WORKER_THREAD_QUEUE_ITEM_SIZE sizeof(Socket_Conn_Info_t)
    QueueHandle_t Worker_Thread_Queue;

    // initialize active socket flags
    for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
    {
        socketInActive[i] = 0;
    }

    // Create the worker thread queue
    Worker_Thread_Queue = FRTOS1_xQueueCreate( WORKER_THREAD_QUEUE_LENGTH, WORKER_THREAD_QUEUE_ITEM_SIZE );
    if( Worker_Thread_Queue == NULL )
    {
        system_unrecoverable_error();
    }

    // Create the TCP socket
    socket = FreeRTOS_socket( FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP );
    if( socket == FREERTOS_INVALID_SOCKET )
    {
        system_unrecoverable_error();
    }

    timeout = app_MSEC_TO_TICK( 100 );
    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_RCVTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        system_unrecoverable_error();
    }

    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_SNDTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        system_unrecoverable_error();
    }

    // Bind connection to TCP port
    socketAddress.sin_port = FreeRTOS_htons( TCP_PORT_NUM );
    result = FreeRTOS_bind( socket, &socketAddress, sizeof ( socketAddress ) );
    if( result != 0 )
    {
        system_unrecoverable_error();
    }

    // Tell connection to go into listening mode
    result = FreeRTOS_listen( socket, TCP_MAX_CONNECTION_CNT );
    if( result != 0 )
    {
        system_unrecoverable_error();
    }

    for(;;)
    {
        uint8 indexToUse = 0;
        uint8 socketFree = 0;
        Socket_Conn_Info_t workerResponse;

        // let the watchdog module know we're still running
        vAppWatchdogResetTCPRxTaskCtr();

        // check for updates from the worker threads
        if( FRTOS1_xQueueReceive(Worker_Thread_Queue, &workerResponse, 0) == pdPASS )
        {
            // the received socket is now free
            socketInActive[workerResponse.index] = 0;

            // inform the watchdog module that both the worker and tx task are now inactive
            vAppWatchdogUpdateTxTaskStatus( workerResponse.index, FALSE );
            vAppWatchdogUpdateWorkerTaskStatus( workerResponse.index, FALSE );

            // kill the Tx task
            FRTOS1_vTaskDelete( txTaskHandles[workerResponse.index] );
        }

        // find a free socket
        indexToUse = 0;
        socketFree = 0;

        for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
        {
            if( socketInActive[i] == 0 )
            {
                indexToUse = i;
                socketFree = 1;
                break;
            }
        }

        if( socketFree != 0 )
        {
            // attempt to get a connection on this socket
            addrLen = sizeof ( socketAddressIn );
            socketIn[indexToUse] = FreeRTOS_accept( socket, &socketAddressIn, &addrLen );

            /* Process the new connection. */
            if( ( socketIn[indexToUse] != FREERTOS_INVALID_SOCKET ) && ( socketIn[indexToUse] != NULL ) )
            {
                Socket_Conn_Info_t sockInfo;
                uint32_t result = 0;

                socketInActive[indexToUse] = 1;

                // create the worker thread for this connection
                TCP_Worker_Thread_Args_t worker_args;
                worker_args.socket = socketIn[indexToUse];
                worker_args.socketIndex = indexToUse;
                worker_args.responseQueue = Worker_Thread_Queue;

                result = FRTOS1_xTaskCreate( vTaskTcpWorker,
                                                "TCPWorker",
                                   configMINIMAL_STACK_SIZE,
                                       (void *)&worker_args,
                                 (configMAX_PRIORITIES - 2),
                                        (xTaskHandle *)NULL  );

                if( result != pdPASS )
                {
                    system_unrecoverable_error();
                }

                // to make sure this socket doesn't get stale CAN data, empty out its queue
                FRTOS1_xQueueReset( CAN_Msg_queue[indexToUse] );

                // Create the Tx task
                TCP_Tx_Thread_Args_t tx_args;
                tx_args.socket = socketIn[indexToUse];
                tx_args.socketIndex = indexToUse;
                tx_args.canMsgQueue = CAN_Msg_queue[indexToUse];

                result = FRTOS1_xTaskCreate(    vAppTaskTcpTx,
                                                "TCPTxThread",
                              (configMINIMAL_STACK_SIZE + 50),
                                             (void *)&tx_args,
                                 ( configMAX_PRIORITIES - 2 ),
                               &( txTaskHandles[indexToUse] )  );

                if( pdPASS != result )
                {
                    system_unrecoverable_error();
                }

                // inform the watchdog module that the worker and tx thread are now active
                vAppWatchdogUpdateTxTaskStatus( indexToUse, TRUE );
                vAppWatchdogUpdateWorkerTaskStatus( indexToUse, TRUE );
            }
        }
        else
        {
            // no free sockets at this time, kill some time
            FRTOS1_vTaskDelay( app_MSEC_TO_TICK( 100 ) );
        }

        // update the connection count variable
        uint8 conCnt = 0;
        for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
        {
            if( socketInActive[i] == 1 )
            {
                conCnt++;
            }
        }
        TCPConnectionCnt = conCnt;
    }

    // Task function does not return
}




static void vAppTaskTcpTx( void *pvParams )
{
    CAN_Msg_And_Socket_t canMsgAndSock;
    uint8_t fwd[1 + 4 + 8] = { 0 };
    uint8_t fwdEcho[1 + 4 + 8] = { 0 };
    uint8_t cobs[128];
    uint8_t cobsEcho[128];
    uint8_t len = 0;
    uint8_t lenEcho = 0;
    uint8_t i = 0;
    uint32_t result = 0;

    // get our arguments 
    TCP_Tx_Thread_Args_t *arguments = (TCP_Tx_Thread_Args_t *)pvParams;
    Socket_t socket = arguments->socket;
    uint8 socketIndex = arguments->socketIndex;
    QueueHandle_t CANMsgQueue = arguments->canMsgQueue;

    // sanity check the index
    if( socketIndex >= TCP_MAX_CONNECTION_CNT )
    {
        // something has gone wrong
        system_unrecoverable_error();
    }

    // update this socket's send timeout before we use it the first time
    TickType_t timeout = app_MSEC_TO_TICK( TCP_THREAD_TX_TIMEOUT );

    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_SNDTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        system_unrecoverable_error();
    }

    for(;;)
    {
        // let the watchdog module know we're still running
        vAppWatchdogResetTCPTxTaskCtr( socketIndex );

        // wait up to 10ms for a new CAN message
        if( FRTOS1_xQueueReceive(CANMsgQueue, &canMsgAndSock, app_MSEC_TO_TICK(10)) == pdPASS )
        {
            // we will assemble two messages.  if we are echoing the message back to the socket that
            // sent it down to us, we add 0x10 to the first byte as a flag to the sender that this
            // is the echo of their message
            fwd[0] = canMsgAndSock.message.Length;
            fwdEcho[0] = canMsgAndSock.message.Length + 0x10;

            if( canMsgAndSock.message.ID & CAN_ID_EXTENDED )
            {
                fwd[1] = fwdEcho[1] = (uint8) ( canMsgAndSock.message.ID >> 24 );
                fwd[2] = fwdEcho[2] = (uint8) ( canMsgAndSock.message.ID >> 16 );
                fwd[3] = fwdEcho[3] = (uint8) ( canMsgAndSock.message.ID >> 8 );
                fwd[4] = fwdEcho[4] = (uint8) ( canMsgAndSock.message.ID >> 0 );

                for( i = 0; i < canMsgAndSock.message.Length; i++ )
                {
                    fwd[i + 5] = fwdEcho[i + 5] = canMsgAndSock.message.Data[i];
                }

                len = COBS_TxBuffer( cobs, sizeof ( cobs ), ( 5 + canMsgAndSock.message.Length ), fwd );
                lenEcho = COBS_TxBuffer( cobsEcho, sizeof ( cobsEcho ), ( 5 + canMsgAndSock.message.Length ), fwdEcho );
            }
            else
            {
                fwd[1] = fwdEcho[1] = (uint8) ( canMsgAndSock.message.ID >> 8 );
                fwd[2] = fwdEcho[2] = (uint8) ( canMsgAndSock.message.ID >> 0 );

                for( i = 0; i < canMsgAndSock.message.Length; i++ )
                {
                    fwd[i + 3] = fwdEcho[i + 3] = canMsgAndSock.message.Data[i];
                }

                len = COBS_TxBuffer( cobs, sizeof ( cobs ), ( 3 + canMsgAndSock.message.Length ), fwd );
                lenEcho = COBS_TxBuffer( cobsEcho, sizeof ( cobsEcho ), ( 3 + canMsgAndSock.message.Length ), fwdEcho );
            }

            // send this message to this socket
            uint8_t bytesToSend = 0;
            uint8_t *txBuf;
            int32_t bytesSentThisCall = 0;
            int32_t bytesSentTotal = 0;
            uint32_t inactiveTimeout = 0;

            // determine which message to send
            if( socketIndex == canMsgAndSock.socketIndex )
            {
                // use the echo message
                bytesToSend = lenEcho;
                txBuf = cobsEcho;
            }
            else
            {
                // use the normal message
                bytesToSend = len;
                txBuf = cobs;
            }

            while( bytesSentTotal < len )
            {
                // FreeRTOS_send will send as many bytes as possible, up to bytesToSend.  However, it may send
                // less.  We need to track this and call _send again with the unsent data.
                bytesSentThisCall = FreeRTOS_send( socket, & ( txBuf[bytesSentTotal] ), ( bytesToSend - bytesSentTotal ), 0 );
                if( bytesSentThisCall >= 0 )
                {
                    bytesSentTotal += bytesSentThisCall;
                }
                else
                {
                    // Error transmitting, abort this message
                    break;
                }

                if( bytesSentThisCall == 0 )
                {
                    // monitor for a dead connection
                    inactiveTimeout++;

                    if( inactiveTimeout >= TCP_THREAD_TX_IDLE_TIMEOUT )
                    {
                        // abort this message
                        break;
                    }
                }
                else
                {
                    inactiveTimeout = 0;
                }
            }
        }
    }

    // Task function does not return
}




static void vSendMsgToCANQueues( CAN_Msg_And_Socket_t *msg )
{
    uint8_t i = 0;

    for( i = 0; i < TCP_MAX_CONNECTION_CNT; i++ )
    {
        // only add a message to this queue if its socket is active
        if( socketInActive[i] == 1 )
        {
            if( FRTOS1_xQueueSendToBack(CAN_Msg_queue[i], msg, 0) != pdTRUE )
            {
                NetQueueErrCnt++;
            }
        }
    }
}




static void TCP_Process_Special_Msg( const uint8 * message )
{
    switch( message[0] )
    {
        case 0xFB :
            ForceReset();
            break;

        default :
            break;
    }
}




static void vTaskTcpWorker( void *pvParams )
{
    typedef struct
    {
        uint8 Data[64];
    } SOCKETBUF;
    SOCKETBUF RxBuffer[1];
#define RECV_BUF_SIZE 128
    uint8_t recvData[RECV_BUF_SIZE];
    int32_t recvBytes;
    uint32_t i = 0;
    TickType_t timeout;
    uint32_t inactive = 0;
    uint32_t result = 0;

    TCP_Worker_Thread_Args_t *arguments = (TCP_Worker_Thread_Args_t *)pvParams;
    Socket_t socket = arguments->socket;
    uint8 socketIndex = arguments->socketIndex;
    QueueHandle_t responseQueue = arguments->responseQueue;

    const uint8 * message;

    COBS_InitRxBuffer( &RxBuffer[0], sizeof(SOCKETBUF) );

    timeout = app_MSEC_TO_TICK( TCP_THREAD_RX_TIMEOUT );
    result = FreeRTOS_setsockopt( socket, 0, FREERTOS_SO_RCVTIMEO, &timeout, 0 );
    if( result != 0 )
    {
        system_unrecoverable_error();
    }

    do
    {
        // let the watchdog module know we're still running
        vAppWatchdogResetTCPWorkerTaskCtr( socketIndex );

        recvBytes = FreeRTOS_recv( socket, &recvData, RECV_BUF_SIZE, 0 );

        if( recvBytes == 0 )
        {
            // no activity received, increment our timeout variable
            inactive++;

            if( inactive >= TCP_THREAD_RX_IDLE_TIMEOUT )
            {
                // close this socket
                break;
            }
        }
        else if( recvBytes > 0 )
        {
            // activity received
            inactive = 0;

            for( i = 0; i < recvBytes; i++ )
            {
                CAN_TX_MESSAGE txMsg;
                uint8 dataStart = 0;
                uint32 canID = 0;
                uint8 extended = 0;
                uint8 msgValid = TRUE;

                message = COBS_RxByte( ( recvData[i] ), &RxBuffer[0] );

                if( message != NULL )
                {
                    // message 0 is total length
                    // message 1 is DLC
                    // message 2-3 or 2-5 are ID bytes
                    // rest is data

                    // messages with byte 0 >= 0x20 are reserved for special messages
                    // handle these separately
                    if( message[0] >= 0x20 )
                    {
                        TCP_Process_Special_Msg( message );

                        // no further processing needed for these messages
                        break;
                    }

                    switch( message[0] - message[1] )
                    {
                        case 1 + 2 : // 2 byte identifier
                            txMsg.ID = ( ( message[2] << 8 ) |
                                        ( message[3] ) );
                            txMsg.ID &= ~CAN_ID_EXTENDED;
                            dataStart = 4;
                            break;
                        case 1 + 4 : // 4 byte extended identifier
                            txMsg.ID = ( ( message[2] << 24 ) |
                                        ( message[3] << 16 ) |
                                        ( message[4] << 8 ) |
                                        ( message[5] ) );
                            txMsg.ID |= CAN_ID_EXTENDED;
                            dataStart = 6;
                            break;
                        default :
                            // this is in invalid message, don't try to parse it
                            msgValid = FALSE;
                            break;
                    }

                    // sanity check the DLC
                    if( message[1] > 8 )
                    {
                        msgValid = FALSE;
                    }

                    if( msgValid == TRUE )
                    {
                        txMsg.Length = message[1];

                        uint8 index = 0;
                        for( index = 0; index < txMsg.Length; index++ )
                        {
                            txMsg.Data[index] = message[dataStart + index];
                        }

                        CAN_Msg_And_Socket_t msgAndSock;

                        msgAndSock.message.Length = txMsg.Length;
                        msgAndSock.message.ID = txMsg.ID;
                        msgAndSock.message.Data[0] = txMsg.Data[0];
                        msgAndSock.message.Data[1] = txMsg.Data[1];
                        msgAndSock.message.Data[2] = txMsg.Data[2];
                        msgAndSock.message.Data[3] = txMsg.Data[3];
                        msgAndSock.message.Data[4] = txMsg.Data[4];
                        msgAndSock.message.Data[5] = txMsg.Data[5];
                        msgAndSock.message.Data[6] = txMsg.Data[6];
                        msgAndSock.message.Data[7] = txMsg.Data[7];
                        msgAndSock.message.Timestamp_ms = (uint16_t) ( app_TICKS_TO_MSEC( FRTOS1_xTaskGetTickCount() ) );
                        msgAndSock.socketIndex = socketIndex;

                        // check if the CAN bus is working.  If not, instead of
                        // sending this message to the physical network, directly
                        // insert it into the local queue so it is sent back out to
                        // all sockets
                        if( CAN_IsEnabled() )
                        {
                            switch( socketIndex )
                            {
                                case 0 :
                                    CAN_Tx( &txMsg, &Network_OnCANMessageTx_Sock0 );
                                    break;
                                case 1 :
                                    CAN_Tx( &txMsg, &Network_OnCANMessageTx_Sock1 );
                                    break;
                                case 2 :
                                    CAN_Tx( &txMsg, &Network_OnCANMessageTx_Sock2 );
                                    break;
                                case 3 :
                                    CAN_Tx( &txMsg, &Network_OnCANMessageTx_Sock3 );
                                    break;
                            }
                        }
                        else
                        {
                            vSendMsgToCANQueues( &msgAndSock );

                            // send this message to the internal IDS-CAN module as well
                            IDS_CAN_OnCanMessageRx( &msgAndSock.message );
                        }
                    }
                }
            }
        }

    } while( ( recvBytes >= 0 ) && ( socketIndex < TCP_MAX_CONNECTION_CNT ) ); // exit loop on error (negative value returned by FreeRTOS_recv() )
                                                                               // or if something happens to the socketIndex value that puts it out of range

    /* Close connection and discard connection identifier. */
    FreeRTOS_shutdown( socket, FREERTOS_SHUT_RDWR );

    uint32_t shutdownTimeout = 0;

    while( FreeRTOS_recv( socket, &recvData, RECV_BUF_SIZE, 0 ) >= 0 )
    {
        /* Wait for shutdown to complete.  If a receive block time is used then
         this delay will not be necessary as FreeRTOS_recv() will place the RTOS task
         into the Blocked state anyway. */
        FRTOS1_vTaskDelay( app_MSEC_TO_TICK( 10 ) );

        /* Note - real applications should implement a timeout here, not just
         loop forever. */

        // let the watchdog module know we're still running
        vAppWatchdogResetTCPWorkerTaskCtr( socketIndex );

        // if the socket has not shutdown after a timeout, force it closed
        shutdownTimeout++;
        if( shutdownTimeout >= TCP_THREAD_RX_SHUTDOWN_TIMEOUT )
        {
            break;
        }
    }

    FreeRTOS_closesocket( socket );

    // inform the rx thread that we are closing this socket
    Socket_Conn_Info_t msg;
    msg.socket = socket;
    msg.index = socketIndex;
    msg.active = 0;

    FRTOS1_xQueueSendToBack( responseQueue, &msg, 0 );

    // delete this thread (the parameter NULL means delete the task that called FRTOS1_vTaskDelete)
    FRTOS1_vTaskDelete( NULL );
}




static void vAppTaskUdpBcast( void *pvParams )
{
    Socket_t udpSocket;
    struct freertos_sockaddr udpDestinationAddress;
    uint8_t broadcastMsg[200] = "";

    udpDestinationAddress.sin_port = FreeRTOS_htons( UDP_PORT_NUM );

    /* Create the socket. */
    udpSocket = FreeRTOS_socket( FREERTOS_AF_INET,
                FREERTOS_SOCK_DGRAM, /*FREERTOS_SOCK_DGRAM for UDP.*/
                FREERTOS_IPPROTO_UDP );

    for(;;)
    {
        // this will set the destination address to the broadcast address for the network
        udpDestinationAddress.sin_addr = FreeRTOS_GetIPAddress() | ~ ( FreeRTOS_GetNetmask() );

        broadcastMsg[0] = '\0';
        sprintf( broadcastMsg, "{\"mfg\":\"%s\", \"product\":\"%s\", \"name\":\"%s\", \"port\":\"%d\" }", UDP_MFG_STRING,
                    UDP_PRODUCT_STRING, FlashStorage.UDPName, TCP_PORT_NUM );

        FreeRTOS_sendto( udpSocket,
                    broadcastMsg,
                    strlen( broadcastMsg ),
                    0,
                    &udpDestinationAddress,
                    sizeof ( udpDestinationAddress ) );

        FRTOS1_vTaskDelay( app_MSEC_TO_TICK( UDP_BROADCAST_PERIOD ) );
    }

    // task function does not return
}



static void Network_OnCANMessageTx_Sock0( CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;

    msgAndSock.message = *message;
    msgAndSock.socketIndex = 0x00;

    vSendMsgToCANQueues( &msgAndSock );

    IDS_CAN_OnCanMessageRx( message );

    return;
}




static void Network_OnCANMessageTx_Sock1( CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;

    msgAndSock.message = *message;
    msgAndSock.socketIndex = 0x01;

    vSendMsgToCANQueues( &msgAndSock );

    IDS_CAN_OnCanMessageRx( message );

    return;
}




static void Network_OnCANMessageTx_Sock2( CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;

    msgAndSock.message = *message;
    msgAndSock.socketIndex = 0x02;

    vSendMsgToCANQueues( &msgAndSock );

    IDS_CAN_OnCanMessageRx( message );

    return;
}




static void Network_OnCANMessageTx_Sock3( CAN_RX_MESSAGE *message )
{
    CAN_Msg_And_Socket_t msgAndSock;

    msgAndSock.message = *message;
    msgAndSock.socketIndex = 0x03;

    vSendMsgToCANQueues( &msgAndSock );

    IDS_CAN_OnCanMessageRx( message );

    return;
}

