/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppEthernetModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Mar 15, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPETHERNETMODULE_H_
#define SOURCES_APPETHERNETMODULE_H_

void vAppEthernetTaskInit(void);
bool boEthernetRxWatchdogTaskCheck(void);
bool boEthernetTxWatchdogTaskCheck(void);
void vEthernetRxWatchdogTaskClear(void);
void vEthernetTxWatchdogTaskClear(void);

void Network_OnCANMessageRx(const xCAN_RX_MESSAGE_TYPE *message);

// TODO - Do we still need this?
//void Network_OnIDSCanMessageTx(const IDS_CAN_RX_MESSAGE *message);

BaseType_t xApplicationDHCPUserHook(    eDHCPCallbackQuestion_t eQuestion,
                                        uint32_t ulIPAddress,
                                        uint32_t ulNetMask );


const uint8* pEthernet_GetAdapterMAC(void);
void Network_DHCP_Failed(void);
uint8 TCP_GetConnectionCount(void);
uint32 TCP_GetPacketsRX(void);
uint32 TCP_GetPacketsTX(void);
uint32 TCP_GetPacketsRXMissed(void);
uint32 TCP_GetPacketsRXDiscarded(void);
uint32 TCP_GetPacketsRXError(void);
uint32 TCP_GetPacketsTXMissed(void);
uint32 TCP_GetPacketsTXDiscarded(void);
uint32 TCP_GetPacketsTXError(void);
uint32 TCP_GetPacketsRXAlign(void);
uint32 TCP_GetPacketsRXFcs(void);
uint32 TCP_GetPacketsRXTruncate(void);
uint32 TCP_GetPacketsRXLenErr(void);
uint32 TCP_GetPacketsRXCollision(void);
uint32 TCP_GetPacketsRXOverrun(void);
uint32 TCP_GetPacketsTXOverflow(void);
uint32 TCP_GetPacketsTXLateCollision(void);
uint32 TCP_GetPacketsTXExcessCollision(void);
uint32 TCP_GetPacketsTXUnderflow(void);
uint32 TCP_GetQueueErrCnt(void);


#endif // SOURCES_APPETHERNETMODULE_H_
