#include "FreeRTOS.h"
#include "FreeRTOS_IP.h"
#include "FreeRTOS_IP_Private.h"
#include "NetworkBufferManagement.h"

/** Alignment macro */
#define __ALIGN_MASK(x,mask)    (((x)+(mask))&~(mask))
#define ALIGN32(x,a)              __ALIGN_MASK(x,(uint32_t)(a)-1)

/** Parameters of ethernet buffers. */
// ARH - changed
//#define ENET_RXBD_NUM                           (8)
#define ENET_RXBD_NUM                           (8)
// ARH - changed
//#define ENET_TXBD_NUM                           (4)
#define ENET_TXBD_NUM                           (8)
#define ENET_EXTRXBD_NUM                        (4)
#define ENET_RXBUFF_SIZE                        (kEnetMaxFrameSize)
#define ENET_TXBUFF_SIZE                        (kEnetMaxFrameSize)
#define ENET_RX_BUFFER_ALIGNMENT                (16)
#define ENET_TX_BUFFER_ALIGNMENT                (16)
/**
 * Kinetis requires 16-byte align for its buffers. Although it's a hardware requirement and
 * has a significance during MAC buffer init, FreeRTOS IP stack will also be happy
 * with this alignment.
 */
#define ENET_BD_ALIGNMENT                       (16)
#define ENET_RXBuffSizeAlign(n)                 ALIGN32(n, ENET_RX_BUFFER_ALIGNMENT)
#define ENET_TXBuffSizeAlign(n)                 ALIGN32(n, ENET_TX_BUFFER_ALIGNMENT)

#if FSL_FEATURE_ENET_SUPPORT_PTP
#define ENET_PTP_TXTS_RING_LEN                  (25)
#define ENET_PTP_RXTS_RING_LEN                  (25)
#endif

#define ENET_CABLE_DETECT_TO                    300     /**< [ms] time for detecting whether the cable is plugged in */

/** Minimum size of a network buffer. */
#define BUFFER_SIZE                             (ipTOTAL_ETHERNET_FRAME_SIZE + ipBUFFER_PADDING)
static LDD_TUserData gxLocalEthernetPointer;
static LDD_TDeviceData *gpEth0LowLevelDeviceState;

static uint32_t enet_buffer_init(enet_dev_if_t *itf, enet_buff_config_t *buff);
static uint32_t enet_rx_cb(void *enet_ptr, enet_mac_packet_buffer_t *packet);
static void     enet_rx_cb_deferred(void *param);
static void     *pvPortMallocZero(uint32_t size);


/**
 * Allocates memory for buffers.
 *
 * If using BufferAllocation_1.c (see Makefile), then the network buffers must be
 * allocated somewhere in the initialization phase. This function is called
 * sooner than @c xNetworkInterfaceInitialise, so the buffers must already be
 * ready.
 *
 * The FreeRTOS docs says it must be allocated in compile time, but it's done here
 * instead in runtime when ethernet is initialized.
 */
void vNetworkInterfaceAllocateRAMToBuffers(NetworkBufferDescriptor_t net_buffs[ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS])
{
    void *ptr;
    uint8_t *start;
    uint32_t tmp;
    uint32_t size;

    if (nstate.init_done != 0)
        return;

    memset((void *)&nstate, 0, sizeof(nstate));

    /* Network buffers of the IP stack. Allocating one huge memory
    block and splitting it internally up. It'd be a good idea to
    allocate separate buffer for each network buffer, since this
    way memdbg could trace each buffer for overwrite, but this
    would cause some extra memory use. Assuming the TCP/IP stack
    is mature enough to avoid memory overwrite... */

    /* all buffers must begin on an aligned address and
    all buffers must have an aligned size */
    size  = ALIGN32(BUFFER_SIZE, ENET_BD_ALIGNMENT);
    size *= ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS;
    size  = ALIGN32(size, ENET_BD_ALIGNMENT);
    /* some extra space is needed, since malloc may return with a non-correctly
    aligned base address */
    size += ENET_BD_ALIGNMENT;

    /* allocate and align base address */
    ptr = pvPortMalloc(size);
    
	if(ptr == NULL) {
		system_unrecoverable_error();
	}
    
	start = (uint8_t *)ALIGN32((uint32_t)ptr, ENET_BD_ALIGNMENT);

    for (tmp = 0; tmp < ipconfigNUM_NETWORK_BUFFER_DESCRIPTORS; tmp++) {
        /* pucEthernetBuffer points to the start of an ethernet frame, so its size must
        be according */
        net_buffs[tmp].pucEthernetBuffer = start + ipBUFFER_PADDING;
        *((uint32_t *)start) = (uint32_t)&net_buffs[tmp];
        start += ALIGN32(BUFFER_SIZE, ENET_BD_ALIGNMENT);
    }
}

/**
 * Ethernet peripheral initialization.
 *
 * If returns with anything else than pdPASS, the TCP/IP stack doesn't give up
 * and calls this function further.
 */
BaseType_t xNetworkInterfaceInitialise(void)
{
	// Initialize the Ethernet Controller
    gpEth0LowLevelDeviceState = ETH0_Init( &gxLocalEthernetPointer );
	
    if (result != kStatus_ENET_Success) {
		system_unrecoverable_error();
	}
	
	    /* counting semaphore to control when the deferred RX task should run */
    nstate.rxq_run = xSemaphoreCreateCounting(ENET_RXBD_NUM + 1, 0);
    if (nstate.rxq_run == NULL) {
		system_unrecoverable_error();
	}

    /* create deferred task to process RX packets */
	if(ipconfigIP_TASK_PRIORITY <= 0) {
		system_unrecoverable_error();
	}
	
	result = xTaskCreate(enet_rx_cb_deferred, "rx_cb_deferred", configMINIMAL_STACK_SIZE, NULL, ipconfigIP_TASK_PRIORITY - 1, NULL);
	
    if (result != pdPASS) {
		system_unrecoverable_error();
	}

    nstate.init_done = 1;
    return pdPASS;
}

#define EXIT(code) do { \
    if (release_after_send != pdFALSE) \
        vReleaseNetworkBufferAndDescriptor(net_buff); \
     return code; \
} while (0)
/**
 * Sends out an ethernet frame.
 *
 * Although the function has a prototype, the TCP/IP stack never cares the
 * return value.
 */
BaseType_t xNetworkInterfaceOutput(NetworkBufferDescriptor_t * const net_buff, BaseType_t release_after_send)
{
    LDD_ETH_TBufferDesc xFrameDescriptor;

    if( ERR_OK == ETH0_SendFrame( gpEth0LowLevelDeviceState, &xFrameDataToSend, 1 )
    {
        // Success!
    };

    iptraceNETWORK_INTERFACE_TRANSMIT();

    EXIT(pdPASS);
}
#undef EXIT

/**
 * Ethernet TX IRQ handler.
 */
void ENET_Transmit_IRQHandler(void)
{
    INT_SYS_DisableIRQ(ENET_Transmit_IRQn);
    ENET_DRV_TxIRQHandler(0);
    INT_SYS_EnableIRQ(ENET_Transmit_IRQn);
}

/**
 * Ethernet RX IRQ handler.
 */
void ENET_Receive_IRQHandler(void)
{
    /* Sometimes it appeared that multiple IRQs arrive (?), so IRQ is
    disabled for the duration of processing the frame. */
    INT_SYS_DisableIRQ(ENET_Receive_IRQn);
    ENET_DRV_RxIRQHandler(0);
    INT_SYS_EnableIRQ(ENET_Receive_IRQn);
}

void ENET_1588_Timer_IRQHandler(void)
{
    ENET_DRV_TsIRQHandler(0);
}

/******************************************************************************
 * Static functions.
 ******************************************************************************/
/**
 * Taken from ethernetif.c:ENET_buffer_init(...) (lwip port in KSDK).
 */
static uint32_t enet_buffer_init(enet_dev_if_t *itf, enet_buff_config_t *buff)
{
    uint32_t rxBufferSizeAlign, txBufferSizeAlign;
    uint8_t  *txBufferAlign, *rxBufferAlign;
    volatile enet_bd_struct_t *txBdPtrAlign, *rxBdPtrAlign;

    /* Check input parameter*/
    if ((!itf) || (!buff))
        return kStatus_ENET_InvalidInput;

    /* Allocate ENET receive buffer descriptors*/
    nstate.txBdPtr = (uint8_t *)pvPortMallocZero(ENET_TXBD_NUM * sizeof(enet_bd_struct_t) + ENET_BD_ALIGNMENT);
    if (!nstate.txBdPtr)
        return kStatus_ENET_MemoryAllocateFail;
    txBdPtrAlign = (volatile enet_bd_struct_t *)ENET_ALIGN((uint32_t)nstate.txBdPtr, ENET_BD_ALIGNMENT);

    nstate.rxBdPtr = (uint8_t *)pvPortMallocZero(ENET_RXBD_NUM * sizeof(enet_bd_struct_t) + ENET_BD_ALIGNMENT);
    if (!nstate.rxBdPtr) {
		system_unrecoverable_error();
	}
    rxBdPtrAlign = (volatile enet_bd_struct_t *)ENET_ALIGN((uint32_t)nstate.rxBdPtr, ENET_BD_ALIGNMENT);

    /* Allocate the transmit and receive data buffers*/
    rxBufferSizeAlign = ENET_RXBuffSizeAlign(ENET_RXBUFF_SIZE);
    nstate.rxBuffer = (uint8_t *)pvPortMallocZero(ENET_RXBD_NUM * rxBufferSizeAlign  + ENET_RX_BUFFER_ALIGNMENT);
    if (!nstate.rxBuffer) {
		system_unrecoverable_error();
	}
    rxBufferAlign = (uint8_t *)ENET_ALIGN((uint32_t)nstate.rxBuffer, ENET_RX_BUFFER_ALIGNMENT);

    txBufferSizeAlign = ENET_RXBuffSizeAlign(ENET_TXBUFF_SIZE);
    nstate.txBuffer = pvPortMallocZero(ENET_TXBD_NUM * txBufferSizeAlign + ENET_TX_BUFFER_ALIGNMENT);
    if (!nstate.txBuffer) {
		system_unrecoverable_error();
	}
    txBufferAlign = (uint8_t *)ENET_ALIGN((uint32_t)nstate.txBuffer, ENET_TX_BUFFER_ALIGNMENT);

#if FSL_FEATURE_ENET_SUPPORT_PTP
    nstate.ptpTsRxData = (enet_mac_ptp_ts_data_t *)pvPortMallocZero(sizeof (enet_mac_ptp_ts_data_t) * ENET_PTP_RXTS_RING_LEN);
    if (nstate.ptpTsRxData == NULL) {
		system_unrecoverable_error();
	}
    nstate.ptpTsTxData = (enet_mac_ptp_ts_data_t *)pvPortMallocZero(sizeof (enet_mac_ptp_ts_data_t) * ENET_PTP_TXTS_RING_LEN);
    if (nstate.ptpTsTxData == NULL) {
		system_unrecoverable_error();
	}
    buff->ptpTsRxDataPtr = nstate.ptpTsRxData;
    buff->ptpTsRxBuffNum = ENET_PTP_RXTS_RING_LEN;
    buff->ptpTsTxDataPtr = nstate.ptpTsTxData;
    buff->ptpTsTxBuffNum = ENET_PTP_TXTS_RING_LEN;
#endif

    buff->extRxBuffQue = NULL;
    buff->extRxBuffNum = 0;

    buff->rxBdNumber      = ENET_RXBD_NUM;
    buff->rxBdPtrAlign    = rxBdPtrAlign;
    buff->rxBufferAlign   = rxBufferAlign;
    buff->rxBuffSizeAlign = rxBufferSizeAlign;
    buff->txBdNumber      = ENET_TXBD_NUM;
    buff->txBdPtrAlign    = txBdPtrAlign;
    buff->txBufferAlign   = txBufferAlign;
    buff->txBuffSizeAlign = txBufferSizeAlign;
    return kStatus_ENET_Success;
}

/**
 * Callback function for received ethernet frames.
 */
static uint32_t enet_rx_cb(void *enet_ptr, enet_mac_packet_buffer_t *packet)
{
    uint16_t *ptr;
    BaseType_t hi_pri = pdFALSE;

    if (nstate.init_done == 0)
        return kStatus_ENET_Success;
    if (packet->length == 0)
        return kStatus_ENET_Success;

    /* Save ethernet packet into temporary queue, since this function is in
    IRQ context and the packet can't be directly passed to the IP stack.
    The deferred task will pass the packet to the IP stack. */
	
	// ARH - todo - update this size when we fix it above
	if(packet->length > (BUFFER_SIZE + 32)) {
		// this message is somehow bigger than the buffer it is intended for
		system_unrecoverable_error();
	}
	
	nstate.rxq_len = packet->length;
	memcpy(nstate.rxq, packet->data, packet->length);

    /* wake up deferred task */
    xSemaphoreGiveFromISR(nstate.rxq_run, &hi_pri);

    portEND_SWITCHING_ISR(hi_pri);
    return kStatus_ENET_Success;
}

/**
 * Ethernet RX deferred processing.
 */
static void enet_rx_cb_deferred(void *param)
{
    IPStackEvent_t event;
    NetworkBufferDescriptor_t *net_buff;

    for(;;)
    {
        if( xSemaphoreTake(nstate.rxq_run, portMAX_DELAY ) != pdPASS )
            continue;

        /* obtain network buffer */
        net_buff = pxGetNetworkBufferWithDescriptor(nstate.rxq_len, 0);
        if (net_buff != NULL) {
            /* does the stack need it at all? */
            if (eConsiderFrameForProcessing(nstate.rxq) == eProcessBuffer) {
                /* send the ethernet frame as an event to the stack */
                memcpy(net_buff->pucEthernetBuffer, nstate.rxq, nstate.rxq_len);
                event.eEventType = eNetworkRxEvent;
                event.pvData     = (void *)net_buff;
                if (xSendEventStructToIPTask(&event, 0) != pdPASS) {
                    vReleaseNetworkBufferAndDescriptor(net_buff);
                    iptraceETHERNET_RX_EVENT_LOST();
                }
                else
                    iptraceNETWORK_INTERFACE_RECEIVE();
            }
            else
                vReleaseNetworkBufferAndDescriptor(net_buff);
        }
        else {
            iptraceETHERNET_RX_EVENT_LOST();
        }
    }
}

/**
 * Allocates some memory and initializes with zeroes.
 */
static void *pvPortMallocZero(uint32_t size)
{
    void *ptr = pvPortMalloc(size);

    if (ptr == NULL)
        return NULL;
    memset(ptr, 0, size);
    return ptr;
}

const enet_stats_t* enet_get_stats(void)
{
    return &nstate.itf.stats;
}
