/**---------------------------------------------------------------------------------------------------------------------
 * @file            HAL.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 6, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_HAL_H_
#define SOURCES_HAL_H_

#if 1
extern void vDoNothingFn(void);


#define u8HAL_EXPANDER_CHIP_A_ADDRESS   (uint8)( eDRV_I2C_SLAVE_ADDRESS_IOEXPANDER_U2 )
#define u8HAL_EXPANDER_CHIP_B_ADDRESS   (uint8)( eDRV_I2C_SLAVE_ADDRESS_IOEXPANDER_U3 )

#define HAL_STIM_CHIP_FOR_LAMP1         eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_LAMP1         ePORT1_3

#define HAL_STIM_CHIP_FOR_LAMP2         eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_LAMP2         ePORT1_2

#define HAL_STIM_CHIP_FOR_LAMP3         eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_LAMP3         ePORT1_1

#define HAL_STIM_CHIP_FOR_LAMP4         eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_LAMP4         ePORT1_0

#define HAL_STIM_CHIP_FOR_MOTOR1_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR1_RET    ePORT0_6
#define HAL_STIM_CHIP_FOR_MOTOR1_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR1_EXT    ePORT1_2

#define HAL_STIM_CHIP_FOR_MOTOR2_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR2_RET    ePORT0_5
#define HAL_STIM_CHIP_FOR_MOTOR2_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR2_EXT    ePORT1_1

#define HAL_STIM_CHIP_FOR_MOTOR3_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR3_RET    ePORT0_4
#define HAL_STIM_CHIP_FOR_MOTOR3_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR3_EXT    ePORT1_0

#define HAL_STIM_CHIP_FOR_MOTOR4_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR4_RET    ePORT0_3
#define HAL_STIM_CHIP_FOR_MOTOR4_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR4_EXT    ePORT0_6

#define HAL_STIM_CHIP_FOR_MOTOR5_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR5_RET    ePORT0_2
#define HAL_STIM_CHIP_FOR_MOTOR5_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR5_EXT    ePORT0_5

#define HAL_STIM_CHIP_FOR_MOTOR6_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR6_RET    ePORT0_1
#define HAL_STIM_CHIP_FOR_MOTOR6_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR6_EXT    ePORT0_4

#define HAL_STIM_CHIP_FOR_MOTOR7_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR7_RET    ePORT0_0
#define HAL_STIM_CHIP_FOR_MOTOR7_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR7_EXT    ePORT0_3

#define HAL_STIM_CHIP_FOR_MOTOR8_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR8_RET    ePORT1_6
#define HAL_STIM_CHIP_FOR_MOTOR8_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR8_EXT    ePORT0_2

#define HAL_STIM_CHIP_FOR_MOTOR9_RET    eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR9_RET    ePORT1_5
#define HAL_STIM_CHIP_FOR_MOTOR9_EXT    eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR9_EXT    ePORT0_1

#define HAL_STIM_CHIP_FOR_MOTOR10_RET   eEXPANDER_CHIP_B
#define HAL_STIM_CHAN_FOR_MOTOR10_RET   ePORT1_4
#define HAL_STIM_CHIP_FOR_MOTOR10_EXT   eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_MOTOR10_EXT   ePORT0_0

#define HAL_STIM_CHIP_FOR_IPDM_SIG1     eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_IPDM_SIG1     ePORT1_6

#define HAL_STIM_CHIP_FOR_IPDM_SIG2     eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_IPDM_SIG2     ePORT1_5

#define HAL_STIM_CHIP_FOR_IPDM_SIG3     eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_IPDM_SIG3     ePORT1_4

#define HAL_STIM_CHIP_FOR_IPDM_SIG4     eEXPANDER_CHIP_A
#define HAL_STIM_CHAN_FOR_IPDM_SIG4     ePORT1_3


#define HAL_RELAY_OUT_LAMP1_ON          LIGHT_1_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_LAMP2_ON          LIGHT_2_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_LAMP3_ON          LIGHT_3_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_LAMP4_ON          LIGHT_4_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV1_ON           REV_1_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV2_ON           REV_2_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV3_ON           REV_3_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV4_ON           REV_4_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV5_ON           REV_5_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV6_ON           REV_6_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV7_ON           REV_7_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV8_ON           REV_8_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV9_ON           REV_9_LOAD_CTRL_SetVal
#define HAL_RELAY_OUT_REV10_ON          REV_10_LOAD_CTRL_SetVal

#define HAL_RELAY_OUT_LAMP1_OFF         LIGHT_1_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_LAMP2_OFF         LIGHT_2_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_LAMP3_OFF         LIGHT_3_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_LAMP4_OFF         LIGHT_4_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV1_OFF          REV_1_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV2_OFF          REV_2_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV3_OFF          REV_3_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV4_OFF          REV_4_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV5_OFF          REV_5_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV6_OFF          REV_6_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV7_OFF          REV_7_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV8_OFF          REV_8_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV9_OFF          REV_9_LOAD_CTRL_ClrVal
#define HAL_RELAY_OUT_REV10_OFF         REV_10_LOAD_CTRL_ClrVal


#define HAL_FAULT_MUX_A_DISABLE         MUX_U10_ENA_ClrVal
#define HAL_FAULT_MUX_A_ADDR_0          MUX_U10_SEL_A0_PutVal
#define HAL_FAULT_MUX_A_ADDR_1          MUX_U10_SEL_A1_PutVal
#define HAL_FAULT_MUX_A_ADDR_2          MUX_U10_SEL_A2_PutVal
#define HAL_FAULT_MUX_A_ENABLE          MUX_U10_ENA_SetVal

#define HAL_FAULT_MUX_B_DISABLE         MUX_U11_ENA_ClrVal
#define HAL_FAULT_MUX_B_ADDR_0          MUX_U11_SEL_A0_PutVal
#define HAL_FAULT_MUX_B_ADDR_1          MUX_U11_SEL_A1_PutVal
#define HAL_FAULT_MUX_B_ADDR_2          MUX_U11_SEL_A2_PutVal
#define HAL_FAULT_MUX_B_ENABLE          MUX_U11_ENA_SetVal

#define HAL_FAULT_MUX_C_DISABLE         MUX_U12_ENA_ClrVal
#define HAL_FAULT_MUX_C_ADDR_0          MUX_U12_SEL_A0_PutVal
#define HAL_FAULT_MUX_C_ADDR_1          MUX_U12_SEL_A1_PutVal
#define HAL_FAULT_MUX_C_ADDR_2          MUX_U12_SEL_A2_PutVal
#define HAL_FAULT_MUX_C_ENABLE          MUX_U12_ENA_SetVal

#define HAL_FAULT_MUX_Z_DISABLE         MUX_U9_ENA_ClrVal
#define HAL_FAULT_MUX_Z_ADDR_0          MUX_U9_SEL_A0_PutVal
#define HAL_FAULT_MUX_Z_ADDR_1          MUX_U9_SEL_A1_PutVal
#define HAL_FAULT_MUX_Z_ADDR_2          MUX_U9_SEL_A2_PutVal
#define HAL_FAULT_MUX_Z_ENABLE          MUX_U9_ENA_SetVal



// Fault insertions on MUX IC   U12
#define HAL_FAULT_SEL_LAMP4             ( 1 )
#define HAL_FAULT_MUX_LAMP4             ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_LAMP3             ( 2 )
#define HAL_FAULT_MUX_LAMP3             ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_LAMP2             ( 3 )
#define HAL_FAULT_MUX_LAMP2             ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_LAMP1             ( 4 )
#define HAL_FAULT_MUX_LAMP1             ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_REV9_PLUS         ( 5 )
#define HAL_FAULT_MUX_REV9_PLUS         ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_REV9_MINUS        ( 6 )
#define HAL_FAULT_MUX_REV9_MINUS        ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_REV10_PLUS        ( 7 )
#define HAL_FAULT_MUX_REV10_PLUS        ( eMUX_CHIP_C )

#define HAL_FAULT_SEL_REV10_MINUS       ( 8 )
#define HAL_FAULT_MUX_REV10_MINUS       ( eMUX_CHIP_C )



// Fault insertions on MUX IC   U11
#define HAL_FAULT_SEL_REV8_MINUS        ( 1 )
#define HAL_FAULT_MUX_REV8_MINUS        ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV8_PLUS         ( 2 )
#define HAL_FAULT_MUX_REV8_PLUS         ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV7_MINUS        ( 3 )
#define HAL_FAULT_MUX_REV7_MINUS        ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV7_PLUS         ( 4 )
#define HAL_FAULT_MUX_REV7_PLUS         ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV5_PLUS         ( 5 )
#define HAL_FAULT_MUX_REV5_PLUS         ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV5_MINUS        ( 6 )
#define HAL_FAULT_MUX_REV5_MINUS        ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV6_PLUS         ( 7 )
#define HAL_FAULT_MUX_REV6_PLUS         ( eMUX_CHIP_B )

#define HAL_FAULT_SEL_REV6_MINUS        ( 8 )
#define HAL_FAULT_MUX_REV6_MINUS        ( eMUX_CHIP_B )



// Fault insertions on MUX IC   U10
#define HAL_FAULT_SEL_REV4_MINUS        ( 1 )
#define HAL_FAULT_MUX_REV4_MINUS        ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV4_PLUS         ( 2 )
#define HAL_FAULT_MUX_REV4_PLUS         ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV3_MINUS        ( 3 )
#define HAL_FAULT_MUX_REV3_MINUS        ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV3_PLUS         ( 4 )
#define HAL_FAULT_MUX_REV3_PLUS         ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV1_PLUS         ( 5 )
#define HAL_FAULT_MUX_REV1_PLUS         ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV1_MINUS        ( 6 )
#define HAL_FAULT_MUX_REV1_MINUS        ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV2_PLUS         ( 7 )
#define HAL_FAULT_MUX_REV2_PLUS         ( eMUX_CHIP_A )

#define HAL_FAULT_SEL_REV2_MINUS        ( 8 )
#define HAL_FAULT_MUX_REV2_MINUS        ( eMUX_CHIP_A )

#define HAL_DUT_POWER_ON                DUT_POWER_CTRL_OUT_SetVal
#define HAL_DUT_POWER_OFF               DUT_POWER_CTRL_OUT_ClrVal

#define Assert_ASK_DATA                 RF_SER_DATA_OUT_SetVal
#define Deassert_ASK_DATA               RF_SER_DATA_OUT_ClrVal

#endif

#endif // SOURCES_HAL_H_
