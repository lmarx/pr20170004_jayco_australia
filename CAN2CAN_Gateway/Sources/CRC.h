/*
 * CRC.h
 *
 *  Created on: Dec 15, 2014
 *      Author: tprohaszka
 */

#ifndef CRC_H_
#define CRC_H_

uint8 CRC8_Calculate(const uint8 * data, uint16 size);
uint8 CRC8_Reset(void);
uint8 CRC8_Update( uint8 crc, uint8 byte );
uint8 CRC8_UpdateRange(uint8 crc, const uint8 * data, uint16 size);


#endif /* CRC_H_ */
