/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppCANModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 7, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPCAN0MODULE_H_
#define SOURCES_APPCAN0MODULE_H_

typedef struct xCanRxTxMessageType
{
    uint32 ID;
    uint8 Data[8];
    uint8 Length;
    uint16 Timestamp_ms; // holds timestamp of rx message
}xCAN_TX_MESSAGE_TYPE, xCAN_RX_MESSAGE_TYPE, CAN_TX_MESSAGE, CAN_RX_MESSAGE;


// Create a type for the Callback pointer.
typedef void (*pvFN_PTR_TYPE)(const xCAN_RX_MESSAGE_TYPE *);
typedef  pvFN_PTR_TYPE CAN_CALLBACK;

// This is an LDD_CAN_TFrame with an 8-byte payload tacked on to the end.
typedef struct xCanWithPayloadType
{
    LDD_CAN_TFrame xLddCanRxFrame;
    uint8 u8Data[8];
    pvFN_PTR_TYPE pFnCallback;
}xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE;

typedef struct xPID_Data
{
    uint32 u32Data[8];
}xPID_DATA_LINK_WITH_DEVICE;

extern QueueHandle_t qPID_BatCap_DataQueue_R;
extern QueueHandle_t qPID_BatCap_DataQueue_W;

typedef union {
struct {
//Swapped cause i am little endian
uint8 u8q; /* low byte - value after the point */
uint8 u8i; /* High byte - value before the point */
} val;
uint16 all; /* half-word */
} UNION_U16_FIXED_POINT_8_8;

/*!!! working with bits, then use 32 bits to have the right bit defined !!!*/
typedef union {
struct {
uint32    	capacity : 7, /* bit0-bit6 */
		charging : 1, /* bit 7 */
		reserved : 24; /* not used */
} bits;
uint32 all; /*  */
} UNION_U32_CAPACITY_CHARGE;

typedef struct {
UNION_U16_FIXED_POINT_8_8 u16Voltage;
UNION_U16_FIXED_POINT_8_8 u16Current;
UNION_U32_CAPACITY_CHARGE u8Val;
uint16 u16Time_discharge;
} STRUCT_DC_POWER_MONITOR;

/*!!! working with bits, then use 32 bits to have the right bit defined !!!*/
typedef union {
struct {
uint32 	u4l : 4, /* bit0-bit3 */
		reserved : 4, /* bit4-bit7 */
		reserved0 : 24; /* not used */
} bits;
uint32 all; /*  */
} UNION_U32_QUALITY;

typedef struct {
UNION_U16_FIXED_POINT_8_8 u16Voltage;
UNION_U16_FIXED_POINT_8_8 u16Current;
UNION_U32_QUALITY 	  u8Val;
} STRUCT_AC_POWER_MONITOR;

typedef struct {
	uint8 u8Mode;
} STRUCT_SETEC_POWER_MANAGER;

typedef struct {
	STRUCT_AC_POWER_MONITOR ac;
	STRUCT_DC_POWER_MONITOR dc_bat;
	STRUCT_DC_POWER_MONITOR dc_bat_aux;
	STRUCT_DC_POWER_MONITOR dc_solar;
	STRUCT_SETEC_POWER_MANAGER manager;
} STRUCT_J35_POWER_MONITOR_DATA;

#define cMFG_RESERVED_FIXED_COMMAND_ID          ( (uint32)( 0x1C0300FF ) )  // The DUT accepts mfg commands with this ID
#define cMFG_RESERVED_FIXED_COMMAND_ID_MASK     ( (uint32)( 0x1FFFFFFE ) )  // A "1" in a bit position says we must match.  A "0" is "don't care"

#define cMFG_RESERVED_FIXED_RESPONSE_ID         ( (uint32)( 0x1C0300FE ) )  // The DUT sends response to mfg commands with this ID
#define cMFG_RESERVED_FIXED_RESPONSE_ID_MASK    ( (uint32)( 0x1FFFFFFE ) )  // A "1" in a bit position says we must match.  A "0" is "don't care"

#define cCAN_TO_HOST_PAYLOAD_LENGTH             ( 8 )
#define CAN_ID_EXTENDED                    0x80000000
#define CAN_ID_REMOTE_TRANSMISSION_REQUEST 0x40000000 /* data length must be 0 when sending this request */

// Housekeeping API
void vAppCAN0TaskInit(void);
bool boCAN0_RxWatchdogTaskCheck(void);
bool boCAN0_TxWatchdogTaskCheck(void);
void vCAN0_RxWatchdogTaskClear(void);
void vCAN0_TxWatchdogTaskClear(void);

// CAN transmit message functions
uint8 CAN_Tx( const xCAN_TX_MESSAGE_TYPE *pIdsCanMessage, pvFN_PTR_TYPE pFnCallback );
void vAppCANTransmitRaw( xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *pxCanFrameToSend );

void vISR_AppCAN0_ModuleOnRxEvent( LDD_CAN_TMBIndex BufferIdx );
void vISR_AppCAN0_ModuleOnTxBufferEmpty( LDD_CAN_TMBIndex u8TxBufferIdx );

#endif // SOURCES_APPCAN0MODULE_H_
