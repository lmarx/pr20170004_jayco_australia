#include "CPU.h"
#include "Events.h"
#include "CAN0.h"
#include "WDog1.h"
#include "WatchDogLdd1.h"
#include "HF1.h"
#include "RF_ENABLE_OUT.h"
#include "BitIoLdd1.h"
#include "RF_TIMER.h"
#include "TimerIntLdd1.h"
#include "CAN0_STANDBY_OUT.h"
#include "BitIoLdd2.h"
#include "FRTOS1.h"
#include "MCUC1.h"
#include "UTIL1.h"
#include "SYS1.h"
#include "RTT1.h"
#include "WAIT1.h"
#include "CLS1.h"
#include "XF1.h"
#include "CS1.h"
#include "TU1.h"
#include "CAN1.h"
#include "CAN1_STANDBY_OUT.h"
#include "BitIoLdd3.h"
#include "DEBUG_SPARE_GPIO1.h"
#include "BitIoLdd4.h"
#include "DEBUG_SPARE_GPIO2.h"
#include "BitIoLdd5.h"
#include "DEBUG_SPARE_GPIO3.h"
#include "BitIoLdd6.h"
#include "DEBUG_SPARE_GPIO4.h"
#include "BitIoLdd7.h"
#include "DEBUG_SPARE_GPIO5.h"
#include "BitIoLdd8.h"
#include "DEBUG_SPARE_GPIO6.h"
#include "BitIoLdd9.h"
#include "DEBUG_SPARE_GPIO7.h"
#include "BitIoLdd10.h"
#include "DEBUG_SPARE_GPIO8.h"
#include "BitIoLdd11.h"
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

/* ###################################################################
**     Filename    : main.c
**     Project     : MFOmegaTesterSW
**     Processor   : MK60DN512VLL10
**     Version     : Driver 01.01
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-02-03, 17:08, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.01
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         

/* User includes (#include below this line is not maintained by Processor Expert) */

#include "IDS-Core\global.h"
#include "Application.h"

/*lint -save  -e970 Disable MISRA rule (6.3) checking. */
int main(void)
/*lint -restore Enable MISRA rule (6.3) checking. */
{
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  vApplicationInitTasks();

  //IDS_CAN_Init();

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/


/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
