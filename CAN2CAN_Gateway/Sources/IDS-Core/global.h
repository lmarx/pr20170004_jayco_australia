// Global.h
// Global header for 20205 LCI MyRV Ethernet Gateway SW
// (c) 2012, 2013, 2014, 2015, 2016 Innovative Design Solutions, Inc.
// All rights reserved

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifndef HEADER
#define HEADER

//lint ++flb  we are in a library

#include "Application.h"
#include "IDS-CAN_PRODUCT_ID.h"
#include "IDS-CAN_FUNCTION_NAME.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define REVISION "A"
#define PART_NUMBER "21401-" REVISION
#define DESCRIPTION "LCI MFOmega Tester SW"

#if FALSE /* go in reset if ASSERT is TRUE */
#define IDSCORE_HALT_ON_ASSERT_FAILURE
#else	/* write in memory the name of the file and the line where the assert occurred */
#define IDSCORE_LOG_ON_ASSERT_FAILURE /* create a log in RAM at address */
#define IDSCORE_ASSERT_LOG_SIZE   32 /* 32 entries in the assert failure log */
#endif

#define IDS_CAN_NUM_DAQ_CHANNELS            ( 4 )

// standard IDS part number
#ifdef PART_NUMBER
extern const char IDS_PartNumber[];
#define IDS_PartNumberDigit(n) IDS_PartNumber[n]
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// basic operating environment
#define CPU_MK60DN512VLL10
#define __LITTLE_ENDIAN__
#define FBUS 							100000000

// include header information 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// In order to safely use FreeRTOS APIs within interrupts their priority needs
// to be set to a lower priority than the FreeRTOS macro configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY.
// Note that on ARM M cores, a lower numerical value is a higher priority.  We will set the default
// user interrupt priority to one step lower than the maximum possible to leave room for a higher priority
// user interrupt if necessary.
#define USER_INTERRUPT_PRIOR			( configMAX_PRIORITIES - 3 )

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Low-level Raw CAN processing

#define IDS_CAN_MESSAGE_FILTERS \
    IDS_CAN_RX_MESSAGE(IDS_CAN_MESSAGE_TYPE_COMMAND, vApplication_OnIDSCanCommandMessageRx) \


/**
 * TODO Ethernet Implementation:  If we want to accept CAN packets over Ethernet, we need the following:
    IDS_CAN_TX_MESSAGE(0xFF, Network_OnIDSCanMessageTx) \
*/
// ** OPTIONAL **   comment out this block if PIDs support is not needed
//
// declare all PIDs supported, format is any combination of
// read only:
//     PID_READ_ONLY(id, read_expression)
// read/write (RAM):
//     PID_READ_WRITE(id, read_expression, session_id, write_expression)
// read/write (non-volatile)
//     PID_READ_WRITE_NVR(id, read_expression, session_id, write_expression)
// where:
//                 id : the 16-bit PID defined in the IDS-CAN specification
//         session_id : a session ID that must be unlocked/open to allow writing
//                      set to zero to allow writing at any time
//    read_expression : an expression which returns the value of the PID
//   write_expression : an expression which sets the value of the PID
#define IDS_CAN_PID_SUPPORT \
PID_READ_WRITE_NVR(IDS_CAN_PID_PRODUCTION_BYTES,            VAL_U8 = u8MyRVDeviceAskProductionByte()            ,  IDS_CAN_SESSION_ID_MANUFACTURING,    vMyRVDeviceSetProductionByte(VAL_I8);           ) \
PID_READ_WRITE_NVR( IDS_CAN_PID_CAN_ADAPTER_MAC,           VAL_U48 = *(U48*)IDS_CAN_GetAdapterMAC()             ,  IDS_CAN_SESSION_ID_MANUFACTURING,    IDS_CAN_SetAdapterMAC(&val.byte[0]);         ) \
PID_READ_WRITE_NVR( IDS_CAN_PID_IDS_CAN_CIRCUIT_ID,        VAL_U32 = IDS_CAN_GetCircuitID(device)               ,  IDS_CAN_SESSION_ID_DIAGNOSTIC,       IDS_CAN_SetCircuitID(device, VAL_I32);          ) \
PID_READ_WRITE_NVR( IDS_CAN_PID_IDS_CAN_FUNCTION_NAME,     VAL_U16 = IDS_CAN_GetFunctionName(device)            ,  IDS_CAN_SESSION_ID_DIAGNOSTIC,       IDS_CAN_SetFunctionName(device, VAL_I16);    ) \
PID_READ_WRITE_NVR( IDS_CAN_PID_IDS_CAN_FUNCTION_INSTANCE,  VAL_U8 = IDS_CAN_GetFunctionInstance(device)        ,  IDS_CAN_SESSION_ID_DIAGNOSTIC,       IDS_CAN_SetFunctionInstance(device, VAL_I8);    ) \
PID_READ_WRITE(IDS_CAN_PID_SHORE_POWER_AMP_RATING,          VAL_U32 = CAN0_ReadShorePower(device),   	IDS_CAN_SESSION_ID_DIAGNOSTIC    , CAN0_WriteShorePower(device, VAL_U32))\
PID_READ_WRITE(IDS_CAN_PID_BATTERY_CAPACITY_AMP_HOURS,      VAL_U32 = CAN0_ReadBatteryCapacity(device), IDS_CAN_SESSION_ID_DIAGNOSTIC    , CAN0_WriteBatteryCapacity(device, VAL_U32))\
PID_READ_ONLY(IDS_CAN_PID_SYSTEM_UPTIME_MS,                 VAL_U32 = IDS_CAN_GetUptime())

// ** OPTIONAL **   comment out this block if session support is not needed
//
// declare all SESSION_IDs supported, format is
//     SESSION(id, cypher, session_open_callback, session_close_callback)
// where:
//                        id :  the 16-bit SESSION_ID defined in the IDS-CAN specification
//                    cypher :  a unique 32-bit value that is used when generating the security key
//                              this value should match the value in the IDS-CAN specification
//  session_allowed_callback :  function that is called to determine if a session is allowed, can be NULL
//                              function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
//     session_open_callback :  function that is called when the session is opened, can be NULL
//                              function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
//     session_close_callback : function that is called when the session is opened, can be NULL
//                              function prototype is CALLBACK(IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id);
#define IDS_CAN_SESSION_SUPPORT \
SESSION(IDS_CAN_SESSION_ID_MANUFACTURING,  0xB16BA115, NULL, NULL,                        NULL) \
SESSION(IDS_CAN_SESSION_ID_DIAGNOSTIC,     0xBABECAFE, NULL, NULL,                        NULL) \
SESSION(IDS_CAN_SESSION_ID_REMOTE_CONTROL, 0xB16B00B5, NULL, Logic_OnIDSCANSessionOpened, Logic_OnIDSCANSessionClosed)



// declare all devices that are supported
// IDS_CAN_DEVICE(name, address, DEVICE(device_type, device_instance, device_capabilities) )
//   name = local name passed to functions
//   address = network address of the device
//             0 = address claim
//             non-zero = fixed address (only used during debugging)
//   device_id = network ID of the device
#define DECLARE_IDS_CAN_DEVICES \
			DECLARE_IDS_CAN_DEVICE( AC_POWER_MONITOR,    DEVICE( IDS_CAN_DEVICE_TYPE_AC_POWER_MONITOR,    0, 0x0 )) \
			DECLARE_IDS_CAN_DEVICE( DC_POWER_MONITOR_1,  DEVICE( IDS_CAN_DEVICE_TYPE_DC_POWER_MONITOR,    1, 0x0 )) \
			DECLARE_IDS_CAN_DEVICE( DC_POWER_MONITOR_2,  DEVICE( IDS_CAN_DEVICE_TYPE_DC_POWER_MONITOR,    2, 0x0 )) \
			DECLARE_IDS_CAN_DEVICE( DC_POWER_MONITOR_3,  DEVICE( IDS_CAN_DEVICE_TYPE_DC_POWER_MONITOR,    3, 0x0 )) \
			DECLARE_IDS_CAN_DEVICE( SETEC_POWER_MANAGER, DEVICE( IDS_CAN_DEVICE_TYPE_SETEC_POWER_MANAGER, 0, 0x0 ))


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Flash Storage parameters

#define FLASH_USER_BLOCK_START			0x0007F800
#define FLASH_USER_BLOCK_SIZE			0x800

#define UseDHCPDefault 		{0x00}
#define MACSuffixDefault 	{0x00, 0x00, 0x00}
#define UDPNameDefault 		{'N', 'o', 'N', 'a', 'm', 'e', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'}

#define FLASH_STORAGE \
            ELEMENT(1, 		UseDHCP, 		UseDHCPDefault) 		\
            ELEMENT(3, 		MACSuffix, 		MACSuffixDefault) 		\
            ELEMENT(16, 	UDPName, 		UDPNameDefault)			\

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define DisableInterrupts               taskDISABLE_INTERRUPTS
#define ServiceWatchdog                 WDog1_Clear
#define EnterCriticalSection            FRTOS1_taskENTER_CRITICAL
#define LeaveCriticalSection            FRTOS1_taskEXIT_CRITICAL


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#include "IDSCore.c"
#include "IDSCore.Assert.c"
#include "IDS-CAN.c"

#include "..\Application.h"

void Logic_OnIDSCANSessionOpened( IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id );
void Logic_OnIDSCANSessionClosed( IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id );
uint32 CAN0_ReadShorePower(IDS_CAN_DEVICE device);
uint32 CAN0_ReadBatteryCapacity(IDS_CAN_DEVICE device);
void CAN0_WriteShorePower(IDS_CAN_DEVICE device, uint32 val);
void CAN0_WriteBatteryCapacity(IDS_CAN_DEVICE device, uint32 val);
uint32 IDS_CAN_GetCircuitID( IDS_CAN_DEVICE device );
void IDS_CAN_SetCircuitID( IDS_CAN_DEVICE device, uint32 circuit_id );

void IDS_CAN_SetAdapterMAC( const uint8 *pMac );

void IDS_CAN_SetFunctionName( IDS_CAN_DEVICE device, IDS_CAN_FUNCTION_NAME function_name );
IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName( IDS_CAN_DEVICE device );

void IDS_CAN_SetFunctionInstance( IDS_CAN_DEVICE device, uint8 function_instance );
uint8 IDS_CAN_GetFunctionInstance( IDS_CAN_DEVICE device );

uint8 u8MyRVDeviceAskProductionByte(void);
void vMyRVDeviceSetProductionByte(uint8 u8NewProdByte );
void vApplication_OnIDSCanCommandMessageRx( const IDS_CAN_RX_MESSAGE *xIdsCanMsg );


//lint --flb  we have left the library
#undef HEADER


#endif // GLOBAL_H_
