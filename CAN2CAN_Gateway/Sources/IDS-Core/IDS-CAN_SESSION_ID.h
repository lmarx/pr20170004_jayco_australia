// IDS-CAN SESSION_ID.h
// Version 2.7
// (c) 2012, 2013, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
//
// Version 1.0  First version
// Version 1.1  Updated to March 2013 level
// Version 1.1b Correcting comments
// Version 2.7  IDS-CAN v2.7 Apr 2017

#ifndef IDS_CAN_SESSION_ID_H
#define IDS_CAN_SESSION_ID_H

typedef uint16 IDS_CAN_SESSION_ID;

#define IDS_CAN_SESSION_TIMEOUT_MS    3500

#define IDS_CAN_SESSION_ID_UNKNOWN            0x0000 /* Reserved, do not use */
#define IDS_CAN_SESSION_ID_MANUFACTURING      0x0001 /* Used to enable manufacturing features */
#define IDS_CAN_SESSION_ID_DIAGNOSTIC         0x0002 /* Used to enable diagnostic tool features */
#define IDS_CAN_SESSION_ID_REPROGRAMMING      0x0003 /* Used when reprogramming a device */
#define IDS_CAN_SESSION_ID_REMOTE_CONTROL     0x0004 /* Used when enabling remote control of the device */
#define IDS_CAN_SESSION_ID_DAQ                0x0005 /* Used to enable DAQ features */

#endif // IDS_CAN_SESSION_ID_H
