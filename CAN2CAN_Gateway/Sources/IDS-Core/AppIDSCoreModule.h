/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppIDSCoreModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Apr 5, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_IDS_CAN_APPIDSCOREMODULE_H_
#define SOURCES_IDS_CAN_APPIDSCOREMODULE_H_

#include "IDS-CORE\IDS-CAN_PRODUCT_ID.h"
#include "IDS-CORE\IDS-CAN_FUNCTION_NAME.h"

typedef uint8 HAL_CAN_MAC_ADDR_TYPE;

// IDS-CAN message filters
// messages that match these identifiers will trigger the appropriate callback
// only MASK bits set to 1 are matched -- other bits are assumed to match
// filters are scanned in the order listed here
//
// there are two types of filters, TX and RX
// IDS_CAN_TX_MESSAGE(message_type, callback)
// IDS_CAN_RX_MESSAGE(message_type, callback)
//    filter match occurs when message_type matches the message
//    message_type = 0xFF implies that all messages match
//    callback is called if tx/rx match is made

extern QueueHandle_t qJ35Monitor_Queue;

void vAppIDSCoreTaskInit(void);
bool boIDSCoreWatchdogTaskCheck(void);
void vIDSCoreWatchdogTaskClear(void);

bool boAppIDSCoreAcquireMux( TickType_t u16Timeout );
void vAppIDSCoreReleaseMux(void);

IDS_CAN_PRODUCT_ID IDS_CAN_GetProductID(void);
uint32 IDS_CAN_GetUptime(void);
const uint8 * IDS_CAN_GetAdapterMAC(void);

#endif // SOURCES_IDS_CAN_APPIDSCOREMODULE_H_
