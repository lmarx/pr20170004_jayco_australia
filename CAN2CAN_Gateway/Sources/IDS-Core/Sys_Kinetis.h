// Sys_Kinetis.h
// Standard IDS system functions for NXP Kinetis microcontrollers
// Version 1.8
// (c) 2010, 2012, 2013, 2016 Innovative Design Solutions, Inc.
// All rights reserved
#ifndef SYS_KINETIS_H
#define SYS_KINETIS_H


// history
// 1.0  First revision - multithreading is not fully debugged and should
//      not be used
// 1.1  Added Random16() to generate a random 16-bit number
// 1.2  Added sprintf() support
// 1.3  Added Randomize16() to initialize random number seed
// 1.4  Added Random32() and Randomize32()
//      Updated Wait() routine to support 24 MHz bus
//      Moved common routines to non-banked memory
// 1.5  Updated Wait() routine to support 25 MHz bus
// 1.6  Moved RNG seed value into NO_INIT RAM
// 1.7  Debugged multithreading support
// 1.8  Multithreading is now interrput aware, switches contexts to a
//      stack dedicated to interrupt use
// 1.9  Adapted from Sys HC12.c to work with Kinetis parts.

// NOTES:
//
// This module is intended to work with the Metrowerks CodeWarrior for HC12
// compiler.
//
// Microcontroller reset and C environment startup code is provided by the
// compiler.  Hence it is not included here.  User code begins executing
// at main()
//
// This module defines the standard data types (uint8, uint16, etc...)
// 
// This module provides a portable wait routine
//     Wait()
//
// This module provides a portable reset routine
//     ForceReset()
// NOTE: the force reset routine relies on the COP and will not perform
//       a true reset if the COP is disabled
//
// This module provides standard critical section support via
//     EnterCriticalSection();
//     LeaveCriticalSection();
//
// This module provides stlib functions memclr, memset and memcpy
// It also provides for atomic reads/writes

typedef signed char             int8;
typedef signed short int        int16;
typedef signed long int         int32;

typedef unsigned char           uint8;
typedef unsigned short int      uint16;
typedef unsigned long int       uint32;

// BYTE structure
typedef uint8 BYTE;


#ifndef NULL
  #define NULL 0
#endif
#ifndef TRUE
  #define TRUE 1
#endif
#ifndef FALSE
  #define FALSE 0
#endif

#define elementsof(n) (sizeof(n)/(sizeof((n)[0])))

uint32 app_TICKS_TO_MSEC( uint32 u32Ticks );
uint32 OS_GetElapsedMilliseconds32(void);
uint16 OS_GetElapsedMilliseconds16(void);
const uint8 *pSystemAskSoftwarePartnum(void);
const uint8 *pSystemAskSoftwareBuildDate(void);
const uint8 *pSystemAskSoftwareBuildTime(void);

// Macros to convert between system ticks and msec - copied from FreeRTOS private code
#define app_MSEC_TO_TICK(msec)  (((uint32_t)(msec)+500uL/(uint32_t)configTICK_RATE_HZ) \
                             *(uint32_t)configTICK_RATE_HZ/1000uL)

#define app_TICKS_TO_MSEC(tick) ((uint32)(((uint64_t)(tick)*1000uL)/(uint32_t)configTICK_RATE_HZ))


// portable reset routine
void ForceReset(void);

void system_unrecoverable_error(void);

// standard IDS part number

extern const char IDS_PartNumber[];

// software build date
uint8 SoftwareBuildYear(void);
uint8 SoftwareBuildMonth(void);
uint8 SoftwareBuildDay(void);
uint8 SoftwareBuildHour(void);
uint8 SoftwareBuildMinute(void);
uint8 SoftwareBuildSecond(void);
extern const char SoftwareBuildDateString[];
extern const char SoftwareBuildTimeString[];

// atomic read/write operations
void AtomicRead(const void * src, void * dst, uint16 count);
#define AtomicRead16(address) (*((uint16*)(address)))
uint32 AtomicRead32(const void * address);
#define AtomicWrite(dst, src, count) AtomicRead(src, dst, count)
#define AtomicWrite16(address, data) (*((uint16*)(address)) = (data))
void AtomicWrite32(void * address, uint32 data);

// standard library functions
#define memclr(address, count) (void)memset(address, 0, count)
//void memclr(void * address, uint16 count);
//void memset(void * address, uint8 byte, uint16 count)
//void memcpy(void * dst, const void * src, uint16 count)


void Randomize(uint32 seed);

// endianess reverse functions
uint32 Reverse32(uint32 input);
uint16 Reverse16(uint16 input);

#endif // SYS_KINETIS_H

