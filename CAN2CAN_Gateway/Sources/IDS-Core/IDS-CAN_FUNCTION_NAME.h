// IDS-CAN FUNCTION_NAME.h
// Version 2.8.0
// (c) 2013, 2014, 2015, 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
//
// Version 1.0   First version
// Version 1.0a  Update March 2013
// Version 1.1   Added more definitions
// Version 1.2   Updated to April 2013 level
// Version 1.3   Updated to May 2013 level
// Version 1.4   Updated to June 2013 level
// Version 1.5   Updated to IDS-CAN v1.2 May 2014
// Version 1.5a  Minor corrections
// Version 1.6   Updated to IDS-CAN v1.3 March 2015
// Version 1.7   Updated to IDS-CAN v1.4 June 2015
// Version 1.7a  Updated to IDS-CAN v1.5 July 2015
// Version 2.0   Updated to IDS-CAN v2.0 Feb 2016
// Version 2.2   Updated to IDS-CAN v2.2 Apr 2016
// Version 2.3   Updated to IDS-CAN v2.3 May 2016
// Version 2.4   Updated to IDS-CAN v2.4 Jun 2016
// Version 2.4a  Updated to IDS-CAN v2.4a Oct 2016
// Version 2.4b  Updated to IDS-CAN v2.4b Nov 2016
// Version 2.5   Updated to IDS-CAN v2.5 Feb 2017
// Version 2.7a  Updated to IDS-CAN v2.7a May 2017
// Version 2.8.0 Names as of 7/13/17

#ifndef IDS_CAN_FUNCTION_NAME_H
#define IDS_CAN_FUNCTION_NAME_H

typedef uint16 IDS_CAN_FUNCTION_NAME;

#define IDS_CAN_FUNCTION_NAME_UNKNOWN                                0x0000
#define IDS_CAN_FUNCTION_NAME_DIAGNOSTIC_TOOL                        0x0001
#define IDS_CAN_FUNCTION_NAME_MYRV_TABLET                            0x0002
#define IDS_CAN_FUNCTION_NAME_GAS_WATER_HEATER                       0x0003
#define IDS_CAN_FUNCTION_NAME_ELECTRIC_WATER_HEATER                  0x0004
#define IDS_CAN_FUNCTION_NAME_WATER_PUMP                             0x0005
#define IDS_CAN_FUNCTION_NAME_BATH_VENT                              0x0006
#define IDS_CAN_FUNCTION_NAME_LIGHT                                  0x0007
#define IDS_CAN_FUNCTION_NAME_FLOOD_LIGHT                            0x0008
#define IDS_CAN_FUNCTION_NAME_WORK_LIGHT                             0x0009
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_CEILING_LIGHT            0x000A
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_OVERHEAD_LIGHT           0x000B
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_VANITY_LIGHT             0x000C
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_SCONCE_LIGHT             0x000D
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_LOFT_LIGHT               0x000E
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_CEILING_LIGHT             0x000F
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_OVERHEAD_LIGHT            0x0010
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_VANITY_LIGHT              0x0011
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_SCONCE_LIGHT              0x0012
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_LOFT_LIGHT                0x0013
#define IDS_CAN_FUNCTION_NAME_LOFT_LIGHT                             0x0014
#define IDS_CAN_FUNCTION_NAME_FRONT_HALL_LIGHT                       0x0015
#define IDS_CAN_FUNCTION_NAME_REAR_HALL_LIGHT                        0x0016
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_LIGHT                   0x0017
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_VANITY_LIGHT            0x0018
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_CEILING_LIGHT           0x0019
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_SHOWER_LIGHT            0x001A
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_SCONCE_LIGHT            0x001B
#define IDS_CAN_FUNCTION_NAME_REAR_BATHROOM_VANITY_LIGHT             0x001C
#define IDS_CAN_FUNCTION_NAME_REAR_BATHROOM_CEILING_LIGHT            0x001D
#define IDS_CAN_FUNCTION_NAME_REAR_BATHROOM_SHOWER_LIGHT             0x001E
#define IDS_CAN_FUNCTION_NAME_REAR_BATHROOM_SCONCE_LIGHT             0x001F
#define IDS_CAN_FUNCTION_NAME_KITCHEN_CEILING_LIGHT                  0x0020
#define IDS_CAN_FUNCTION_NAME_KITCHEN_SCONCE_LIGHT                   0x0021
#define IDS_CAN_FUNCTION_NAME_KITCHEN_PENDANTS_LIGHT                 0x0022
#define IDS_CAN_FUNCTION_NAME_KITCHEN_RANGE_LIGHT                    0x0023
#define IDS_CAN_FUNCTION_NAME_KITCHEN_COUNTER_LIGHT                  0x0024
#define IDS_CAN_FUNCTION_NAME_KITCHEN_BAR_LIGHT                      0x0025
#define IDS_CAN_FUNCTION_NAME_KITCHEN_ISLAND_LIGHT                   0x0026
#define IDS_CAN_FUNCTION_NAME_KITCHEN_CHANDELIER_LIGHT               0x0027
#define IDS_CAN_FUNCTION_NAME_KITCHEN_UNDER_CABINET_LIGHT            0x0028
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_CEILING_LIGHT              0x0029
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_SCONCE_LIGHT               0x002A
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_PENDANTS_LIGHT             0x002B
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_BAR_LIGHT                  0x002C
#define IDS_CAN_FUNCTION_NAME_GARAGE_CEILING_LIGHT                   0x002D
#define IDS_CAN_FUNCTION_NAME_GARAGE_CABINET_LIGHT                   0x002E
#define IDS_CAN_FUNCTION_NAME_SECURITY_LIGHT                         0x002F
#define IDS_CAN_FUNCTION_NAME_PORCH_LIGHT                            0x0030
#define IDS_CAN_FUNCTION_NAME_AWNING_LIGHT                           0x0031
#define IDS_CAN_FUNCTION_NAME_BATHROOM_LIGHT                         0x0032
#define IDS_CAN_FUNCTION_NAME_BATHROOM_VANITY_LIGHT                  0x0033
#define IDS_CAN_FUNCTION_NAME_BATHROOM_CEILING_LIGHT                 0x0034
#define IDS_CAN_FUNCTION_NAME_BATHROOM_SHOWER_LIGHT                  0x0035
#define IDS_CAN_FUNCTION_NAME_BATHROOM_SCONCE_LIGHT                  0x0036
#define IDS_CAN_FUNCTION_NAME_HALL_LIGHT                             0x0037
#define IDS_CAN_FUNCTION_NAME_BUNK_ROOM_LIGHT                        0x0038
#define IDS_CAN_FUNCTION_NAME_BEDROOM_LIGHT                          0x0039
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_LIGHT                      0x003A
#define IDS_CAN_FUNCTION_NAME_KITCHEN_LIGHT                          0x003B
#define IDS_CAN_FUNCTION_NAME_LOUNGE_LIGHT                           0x003C
#define IDS_CAN_FUNCTION_NAME_CEILING_LIGHT                          0x003D
#define IDS_CAN_FUNCTION_NAME_ENTRY_LIGHT                            0x003E
#define IDS_CAN_FUNCTION_NAME_BED_CEILING_LIGHT                      0x003F
#define IDS_CAN_FUNCTION_NAME_BEDROOM_LAV_LIGHT                      0x0040
#define IDS_CAN_FUNCTION_NAME_SHOWER_LIGHT                           0x0041
#define IDS_CAN_FUNCTION_NAME_GALLEY_LIGHT                           0x0042
#define IDS_CAN_FUNCTION_NAME_FRESH_TANK                             0x0043
#define IDS_CAN_FUNCTION_NAME_GREY_TANK                              0x0044
#define IDS_CAN_FUNCTION_NAME_BLACK_TANK                             0x0045
#define IDS_CAN_FUNCTION_NAME_FUEL_TANK                              0x0046
#define IDS_CAN_FUNCTION_NAME_GENERATOR_FUEL_TANK                    0x0047
#define IDS_CAN_FUNCTION_NAME_AUXILLIARY_FUEL_TANK                   0x0048
#define IDS_CAN_FUNCTION_NAME_FRONT_BATH_GREY_TANK                   0x0049
#define IDS_CAN_FUNCTION_NAME_FRONT_BATH_FRESH_TANK                  0x004A
#define IDS_CAN_FUNCTION_NAME_FRONT_BATH_BLACK_TANK                  0x004B
#define IDS_CAN_FUNCTION_NAME_REAR_BATH_GREY_TANK                    0x004C
#define IDS_CAN_FUNCTION_NAME_REAR_BATH_FRESH_TANK                   0x004D
#define IDS_CAN_FUNCTION_NAME_REAR_BATH_BLACK_TANK                   0x004E
#define IDS_CAN_FUNCTION_NAME_MAIN_BATH_GREY_TANK                    0x004F
#define IDS_CAN_FUNCTION_NAME_MAIN_BATH_FRESH_TANK                   0x0050
#define IDS_CAN_FUNCTION_NAME_MAIN_BATH_BLACK_TANK                   0x0051
#define IDS_CAN_FUNCTION_NAME_GALLEY_GREY_TANK                       0x0052
#define IDS_CAN_FUNCTION_NAME_GALLEY_FRESH_TANK                      0x0053
#define IDS_CAN_FUNCTION_NAME_GALLEY_BLACK_TANK                      0x0054
#define IDS_CAN_FUNCTION_NAME_KITCHEN_GREY_TANK                      0x0055
#define IDS_CAN_FUNCTION_NAME_KITCHEN_FRESH_TANK                     0x0056
#define IDS_CAN_FUNCTION_NAME_KITCHEN_BLACK_TANK                     0x0057
#define IDS_CAN_FUNCTION_NAME_LANDING_GEAR                           0x0058
#define IDS_CAN_FUNCTION_NAME_FRONT_STABILIZER                       0x0059
#define IDS_CAN_FUNCTION_NAME_REAR_STABILIZER                        0x005A
#define IDS_CAN_FUNCTION_NAME_TV_LIFT                                0x005B
#define IDS_CAN_FUNCTION_NAME_BED_LIFT                               0x005C
#define IDS_CAN_FUNCTION_NAME_BATH_VENT_COVER                        0x005D
#define IDS_CAN_FUNCTION_NAME_DOOR_LOCK                              0x005E
#define IDS_CAN_FUNCTION_NAME_GENERATOR                              0x005F
#define IDS_CAN_FUNCTION_NAME_SLIDE                                  0x0060
#define IDS_CAN_FUNCTION_NAME_MAIN_SLIDE                             0x0061
#define IDS_CAN_FUNCTION_NAME_BEDROOM_SLIDE                          0x0062
#define IDS_CAN_FUNCTION_NAME_GALLEY_SLIDE                           0x0063
#define IDS_CAN_FUNCTION_NAME_KITCHEN_SLIDE                          0x0064
#define IDS_CAN_FUNCTION_NAME_CLOSET_SLIDE                           0x0065
#define IDS_CAN_FUNCTION_NAME_OPTIONAL_SLIDE                         0x0066
#define IDS_CAN_FUNCTION_NAME_DOOR_SIDE_SLIDE                        0x0067
#define IDS_CAN_FUNCTION_NAME_OFF_DOOR_SLIDE                         0x0068
#define IDS_CAN_FUNCTION_NAME_AWNING                                 0x0069
#define IDS_CAN_FUNCTION_NAME_LEVEL_UP_LEVELER                       0x006A
#define IDS_CAN_FUNCTION_NAME_WATER_TANK_HEATER                      0x006B
#define IDS_CAN_FUNCTION_NAME_MYRV_TOUCHSCREEN                       0x006C
#define IDS_CAN_FUNCTION_NAME_LEVELER                                0x006D
#define IDS_CAN_FUNCTION_NAME_VENT_COVER                             0x006E
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_VENT_COVER               0x006F
#define IDS_CAN_FUNCTION_NAME_BEDROOM_VENT_COVER                     0x0070
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_VENT_COVER              0x0071
#define IDS_CAN_FUNCTION_NAME_MAIN_BATHROOM_VENT_COVER               0x0072
#define IDS_CAN_FUNCTION_NAME_REAR_BATHROOM_VENT_COVER               0x0073
#define IDS_CAN_FUNCTION_NAME_KITCHEN_VENT_COVER                     0x0074
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_VENT_COVER                 0x0075
#define IDS_CAN_FUNCTION_NAME_FOUR_LEG_TRUCK_CAMPLER_LEVELER         0x0076
#define IDS_CAN_FUNCTION_NAME_SIX_LEG_HALL_EFFECT_EJ_LEVELER         0x0077
#define IDS_CAN_FUNCTION_NAME_PATIO_LIGHT                            0x0078
#define IDS_CAN_FUNCTION_NAME_HUTCH_LIGHT                            0x0079
#define IDS_CAN_FUNCTION_NAME_SCARE_LIGHT                            0x007A
#define IDS_CAN_FUNCTION_NAME_DINETTE_LIGHT                          0x007B
#define IDS_CAN_FUNCTION_NAME_BAR_LIGHT                              0x007C
#define IDS_CAN_FUNCTION_NAME_OVERHEAD_LIGHT                         0x007D
#define IDS_CAN_FUNCTION_NAME_OVERHEAD_BAR_LIGHT                     0x007E
#define IDS_CAN_FUNCTION_NAME_FOYER_LIGHT                            0x007F
#define IDS_CAN_FUNCTION_NAME_RAMP_DOOR_LIGHT                        0x0080
#define IDS_CAN_FUNCTION_NAME_ENTERTAINMENT_LIGHT                    0x0081
#define IDS_CAN_FUNCTION_NAME_REAR_ENTRY_DOOR_LIGHT                  0x0082
#define IDS_CAN_FUNCTION_NAME_CEILING_FAN_LIGHT                      0x0083
#define IDS_CAN_FUNCTION_NAME_OVERHEAD_FAN_LIGHT                     0x0084
#define IDS_CAN_FUNCTION_NAME_BUNK_SLIDE                             0x0085
#define IDS_CAN_FUNCTION_NAME_BED_SLIDE                              0x0086
#define IDS_CAN_FUNCTION_NAME_WARDROBE_SLIDE                         0x0087
#define IDS_CAN_FUNCTION_NAME_ENTERTAINMENT_SLIDE                    0x0088
#define IDS_CAN_FUNCTION_NAME_SOFA_SLIDE                             0x0089
#define IDS_CAN_FUNCTION_NAME_PATIO_AWNING                           0x008A
#define IDS_CAN_FUNCTION_NAME_REAR_AWNING                            0x008B
#define IDS_CAN_FUNCTION_NAME_SIDE_AWNING                            0x008C
#define IDS_CAN_FUNCTION_NAME_JACKS                                  0x008D
#define IDS_CAN_FUNCTION_NAME_LEVELER_2                              0x008E
#define IDS_CAN_FUNCTION_NAME_EXTERIOR_LIGHT                         0x008F
#define IDS_CAN_FUNCTION_NAME_LOWER_ACCENT_LIGHT                     0x0090
#define IDS_CAN_FUNCTION_NAME_UPPER_ACCENT_LIGHT                     0x0091
#define IDS_CAN_FUNCTION_NAME_DS_SECURITY_LIGHT                      0x0092
#define IDS_CAN_FUNCTION_NAME_ODS_SECURITY_LIGHT                     0x0093
#define IDS_CAN_FUNCTION_NAME_SLIDE_IN_SLIDE                         0x0094
#define IDS_CAN_FUNCTION_NAME_HITCH_LIGHT                            0x0095
#define IDS_CAN_FUNCTION_NAME_CLOCK                                  0x0096
#define IDS_CAN_FUNCTION_NAME_TV                                     0x0097
#define IDS_CAN_FUNCTION_NAME_DVD                                    0x0098
#define IDS_CAN_FUNCTION_NAME_BLU_RAY                                0x0099
#define IDS_CAN_FUNCTION_NAME_VCR                                    0x009A
#define IDS_CAN_FUNCTION_NAME_PVR                                    0x009B
#define IDS_CAN_FUNCTION_NAME_CABLE                                  0x009C
#define IDS_CAN_FUNCTION_NAME_SATELLITE                              0x009D
#define IDS_CAN_FUNCTION_NAME_AUDIO                                  0x009E
#define IDS_CAN_FUNCTION_NAME_CD_PLAYER                              0x009F
#define IDS_CAN_FUNCTION_NAME_TUNER                                  0x00A0
#define IDS_CAN_FUNCTION_NAME_RADIO                                  0x00A1
#define IDS_CAN_FUNCTION_NAME_SPEAKERS                               0x00A2
#define IDS_CAN_FUNCTION_NAME_GAME                                   0x00A3
#define IDS_CAN_FUNCTION_NAME_CLOCK_RADIO                            0x00A4
#define IDS_CAN_FUNCTION_NAME_AUX                                    0x00A5
#define IDS_CAN_FUNCTION_NAME_CLIMATE_ZONE                           0x00A6
#define IDS_CAN_FUNCTION_NAME_FIREPLACE                              0x00A7
#define IDS_CAN_FUNCTION_NAME_THERMOSTAT                             0x00A8
#define IDS_CAN_FUNCTION_NAME_FRONT_CAP_LIGHT                        0x00A9
#define IDS_CAN_FUNCTION_NAME_STEP_LIGHT                             0x00AA
#define IDS_CAN_FUNCTION_NAME_DS_FLOOD_LIGHT                         0x00AB
#define IDS_CAN_FUNCTION_NAME_INTERIOR_LIGHT                         0x00AC
#define IDS_CAN_FUNCTION_NAME_FRESH_TANK_HEATER                      0x00AD
#define IDS_CAN_FUNCTION_NAME_GREY_TANK_HEATER                       0x00AE
#define IDS_CAN_FUNCTION_NAME_BLACK_TANK_HEATER                      0x00AF
#define IDS_CAN_FUNCTION_NAME_LP_TANK                                0x00B0
#define IDS_CAN_FUNCTION_NAME_STALL_LIGHT                            0x00B1
#define IDS_CAN_FUNCTION_NAME_MAIN_LIGHT                             0x00B2
#define IDS_CAN_FUNCTION_NAME_BATH_LIGHT                             0x00B3
#define IDS_CAN_FUNCTION_NAME_BUNK_LIGHT                             0x00B4
#define IDS_CAN_FUNCTION_NAME_BED_LIGHT                              0x00B5
#define IDS_CAN_FUNCTION_NAME_CABINET_LIGHT                          0x00B6
#define IDS_CAN_FUNCTION_NAME_NETWORK_BRIDGE                         0x00B7
#define IDS_CAN_FUNCTION_NAME_ETHERNET_BRIDGE                        0x00B8
#define IDS_CAN_FUNCTION_NAME_WIFI_BRIDGE                            0x00B9
#define IDS_CAN_FUNCTION_NAME_IN_TRANSIT_POWER_DISCONNECT            0x00BA
#define IDS_CAN_FUNCTION_NAME_LEVEL_UP_UNITY                         0x00BB
#define IDS_CAN_FUNCTION_NAME_TT_LEVELER                             0x00BC
#define IDS_CAN_FUNCTION_NAME_TRAVEL_TRAILER_LEVELER                 0x00BD
#define IDS_CAN_FUNCTION_NAME_FIFTH_WHEEL_LEVELER                    0x00BE
#define IDS_CAN_FUNCTION_NAME_FUEL_PUMP                              0x00BF
#define IDS_CAN_FUNCTION_NAME_MAIN_CLIMATE_ZONE                      0x00C0
#define IDS_CAN_FUNCTION_NAME_BEDROOM_CLIMATE_ZONE                   0x00C1
#define IDS_CAN_FUNCTION_NAME_GARAGE_CLIMATE_ZONE                    0x00C2
#define IDS_CAN_FUNCTION_NAME_COMPARTMENT_LIGHT                      0x00C3
#define IDS_CAN_FUNCTION_NAME_TRUNK_LIGHT                            0x00C4
#define IDS_CAN_FUNCTION_NAME_BAR_TV                                 0x00C5
#define IDS_CAN_FUNCTION_NAME_BATHROOM_TV                            0x00C6
#define IDS_CAN_FUNCTION_NAME_BEDROOM_TV                             0x00C7
#define IDS_CAN_FUNCTION_NAME_BUNK_ROOM_TV                           0x00C8
#define IDS_CAN_FUNCTION_NAME_EXTERIOR_TV                            0x00C9
#define IDS_CAN_FUNCTION_NAME_FRONT_BATHROOM_TV                      0x00CA
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_TV                       0x00CB
#define IDS_CAN_FUNCTION_NAME_GARAGE_TV                              0x00CC
#define IDS_CAN_FUNCTION_NAME_KITCHEN_TV                             0x00CD
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_TV                         0x00CE
#define IDS_CAN_FUNCTION_NAME_LOFT_TV                                0x00CF
#define IDS_CAN_FUNCTION_NAME_LOUNGE_TV                              0x00D0
#define IDS_CAN_FUNCTION_NAME_MAIN_TV                                0x00D1
#define IDS_CAN_FUNCTION_NAME_PATIO_TV                               0x00D2
#define IDS_CAN_FUNCTION_NAME_REAR_BATHROOM_TV                       0x00D3
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_TV                        0x00D4
#define IDS_CAN_FUNCTION_NAME_BATHROOM_DOOR_LOCK                     0x00D5
#define IDS_CAN_FUNCTION_NAME_BEDROOM_DOOR_LOCK                      0x00D6
#define IDS_CAN_FUNCTION_NAME_FRONT_DOOR_LOCK                        0x00D7
#define IDS_CAN_FUNCTION_NAME_GARAGE_DOOR_LOCK                       0x00D8
#define IDS_CAN_FUNCTION_NAME_MAIN_DOOR_LOCK                         0x00D9
#define IDS_CAN_FUNCTION_NAME_PATIO_DOOR_LOCK                        0x00DA
#define IDS_CAN_FUNCTION_NAME_REAR_DOOR_LOCK                         0x00DB
#define IDS_CAN_FUNCTION_NAME_ACCENT_LIGHT                           0x00DC
#define IDS_CAN_FUNCTION_NAME_BATHROOM_ACCENT_LIGHT                  0x00DD
#define IDS_CAN_FUNCTION_NAME_BEDROOM_ACCENT_LIGHT                   0x00DE
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_ACCENT_LIGHT             0x00DF
#define IDS_CAN_FUNCTION_NAME_GARAGE_ACCENT_LIGHT                    0x00E0
#define IDS_CAN_FUNCTION_NAME_KITCHEN_ACCENT_LIGHT                   0x00E1
#define IDS_CAN_FUNCTION_NAME_PATIO_ACCENT_LIGHT                     0x00E2
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_ACCENT_LIGHT              0x00E3
#define IDS_CAN_FUNCTION_NAME_BEDROOM_RADIO                          0x00E4
#define IDS_CAN_FUNCTION_NAME_BUNK_ROOM_RADIO                        0x00E5
#define IDS_CAN_FUNCTION_NAME_EXTERIOR_RADIO                         0x00E6
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_RADIO                    0x00E7
#define IDS_CAN_FUNCTION_NAME_GARAGE_RADIO                           0x00E8
#define IDS_CAN_FUNCTION_NAME_KITCHEN_RADIO                          0x00E9
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_RADIO                      0x00EA
#define IDS_CAN_FUNCTION_NAME_LOFT_RADIO                             0x00EB
#define IDS_CAN_FUNCTION_NAME_PATIO_RADIO                            0x00EC
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_RADIO                     0x00ED
#define IDS_CAN_FUNCTION_NAME_BEDROOM_ENTERTAINMENT_SYSTEM           0x00EE
#define IDS_CAN_FUNCTION_NAME_BUNK_ROOM_ENTERTAINMENT_SYSTEM         0x00EF
#define IDS_CAN_FUNCTION_NAME_ENTERTAINMENT_SYSTEM                   0x00F0
#define IDS_CAN_FUNCTION_NAME_EXTERIOR_ENTERTAINMENT_SYSTEM          0x00F1
#define IDS_CAN_FUNCTION_NAME_FRONT_BEDROOM_ENTERTAINMENT_SYSTEM     0x00F2
#define IDS_CAN_FUNCTION_NAME_GARAGE_ENTERTAINMENT_SYSTEM            0x00F3
#define IDS_CAN_FUNCTION_NAME_KITCHEN_ENTERTAINMENT_SYSTEM           0x00F4
#define IDS_CAN_FUNCTION_NAME_LIVING_ROOM_ENTERTAINMENT_SYSTEM       0x00F5
#define IDS_CAN_FUNCTION_NAME_LOFT_ENTERTAINMENT_SYSTEM              0x00F6
#define IDS_CAN_FUNCTION_NAME_MAIN_ENTERTAINMENT_SYSTEM              0x00F7
#define IDS_CAN_FUNCTION_NAME_PATIO_ENTERTAINMENT_SYSTEM             0x00F8
#define IDS_CAN_FUNCTION_NAME_REAR_BEDROOM_ENTERTAINMENT_SYSTEM      0x00F9
#define IDS_CAN_FUNCTION_NAME_LEFT_STABILIZER                        0x00FA
#define IDS_CAN_FUNCTION_NAME_RIGHT_STABILIZER                       0x00FB
#define IDS_CAN_FUNCTION_NAME_STABILIZER                             0x00FC
#define IDS_CAN_FUNCTION_NAME_SOLAR                                  0x00FD
#define IDS_CAN_FUNCTION_NAME_SOLAR_POWER                            0x00FE
#define IDS_CAN_FUNCTION_NAME_BATTERY                                0x00FF
#define IDS_CAN_FUNCTION_NAME_MAIN_BATTERY                           0x0100
#define IDS_CAN_FUNCTION_NAME_AUX_BATTERY                            0x0101
#define IDS_CAN_FUNCTION_NAME_SHORE_POWER                            0x0102
#define IDS_CAN_FUNCTION_NAME_AC_POWER                               0x0103
#define IDS_CAN_FUNCTION_NAME_AC_MAINS                               0x0104
#define IDS_CAN_FUNCTION_NAME_AUX_POWER                              0x0105
#define IDS_CAN_FUNCTION_NAME_OUTPUTS                                0x0106

#endif // IDS_CAN_FUNCTION_NAME_H
