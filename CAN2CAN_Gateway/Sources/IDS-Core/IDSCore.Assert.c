// IDSCore.Assert.c
// Debug assertions for small scale embedded systems
// Version 1.0
// (c) 2017 Innovative Design Solutions, Inc.
// All rights reserved

#include "global.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

#if defined IDSCORE_HALT_ON_ASSERT_FAILURE || defined IDSCORE_LOG_ON_ASSERT_FAILURE
	void _IDSCore_AssertFailed(const char *file, uint16 line);
	#define ASSERT(exp)   if (exp) {} else _IDSCore_AssertFailed(__FILE__, (uint16)__LINE__)
#elif defined IDSCORE_DISABLE_ASSERT_CHECKING
	#define ASSERT(exp) /* no asserts */
#else
	#error IDSCore.Assert.c: must define one of IDSCORE_HALT_ON_ASSERT_FAILURE, IDSCORE_LOG_ON_ASSERT_FAILURE, or IDSCORE_DISABLE_ASSERT_CHECKING
#endif

#else

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined IDSCORE_HALT_ON_ASSERT_FAILURE && defined IDSCORE_LOG_ON_ASSERT_FAILURE
#error IDSCore.Assert.c: cannot define both IDSCORE_HALT_ON_ASSERT_FAILURE and IDSCORE_LOG_ON_ASSERT_FAILURE
#endif

#if defined IDSCORE_HALT_ON_ASSERT_FAILURE && defined IDSCORE_DISABLE_ASSERT_CHECKING
#error IDSCore.Assert.c: cannot define both IDSCORE_HALT_ON_ASSERT_FAILURE and IDSCORE_DISABLE_ASSERT_CHECKING
#endif

#if defined IDSCORE_LOG_ON_ASSERT_FAILURE && defined IDSCORE_DISABLE_ASSERT_CHECKING
#error IDSCore.Assert.c: cannot define both IDSCORE_LOG_ON_ASSERT_FAILURE and IDSCORE_DISABLE_ASSERT_CHECKING
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////


#ifdef IDSCORE_HALT_ON_ASSERT_FAILURE

void _IDSCore_AssertFailed(const char *file, uint16 line)
{
	for (;;)
	{
		DisableInterrupts();
		ServiceWatchdog();
	}
}

#endif // IDSCORE_HALT_ON_ASSERT_FAILURE

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////


#ifdef IDSCORE_LOG_ON_ASSERT_FAILURE

// ensure IDSCORE_ASSERT_LOG_SIZE is defined and is a power of two
#if ! defined IDSCORE_ASSERT_LOG_SIZE
  #error IDSCore.Assert.c: Must define IDSCORE_ASSERT_LOG_SIZE
#elif ((IDSCORE_ASSERT_LOG_SIZE & (IDSCORE_ASSERT_LOG_SIZE - 1)) != 0)
  #error IDSCore.Assert.c: IDSCORE_ASSERT_LOG_SIZE must be a power of two
#endif

static const char * File[IDSCORE_ASSERT_LOG_SIZE];
static uint16 Line[IDSCORE_ASSERT_LOG_SIZE];

#if IDSCORE_ASSERT_LOG_SIZE <= 256
static uint8 Index = 0;
#else
static uint16 Index = 0;
#endif

void _IDSCore_AssertFailed(const char *file, uint16 line)
{
  File[Index & (IDSCORE_ASSERT_LOG_SIZE-1)] = file;
  Line[Index & (IDSCORE_ASSERT_LOG_SIZE-1)] = line;
  Index++;
}

#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
