/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppIDSCoreModule.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Apr 5, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#include "Application.h"
#include "global.h"
#ifdef INCLUDE_IDS_CORE_CAN


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */
// Task parameters
static const uint8  cMY_TASK_PRIORITY           = ( configMAX_PRIORITIES - 4 );
static const uint16 cMY_TASK_STACK_SIZE         = 240;
static volatile uint32 u32MyTaskWdgCheck        = FALSE;
static TaskHandle_t xMyTaskHandle               = NULL;
// Mutex
static xSemaphoreHandle muxIDSCore = NULL;

static uint8 gu8ProductionByte = 0;
static uint8 gu8CAN0MACAddress[6] = { 0x73, 0x31, 0x03, 0xb6, 0x91, 0xdb };   // TODO For real-world use, you need to provide a way to randomize and store these variables in non-volatile storage.
static uint32 gu32CanCircuitId[NUM_IDS_CAN_DEVICES];
static uint16 gu16CanFuncName[NUM_IDS_CAN_DEVICES] = {
		IDS_CAN_FUNCTION_NAME_AC_POWER,
		IDS_CAN_FUNCTION_NAME_MAIN_BATTERY,
		IDS_CAN_FUNCTION_NAME_AUX_POWER,
		IDS_CAN_FUNCTION_NAME_SOLAR_POWER,
		IDS_CAN_FUNCTION_NAME_OUTPUTS};
static uint8 gu8CanFuncInstance[NUM_IDS_CAN_DEVICES];
//static uint8 gu8Status[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

static const HAL_CAN_MAC_ADDR_TYPE gxCanMacAddress[] = { 0x55, 0x55, 0x55, 0x55, 0x55, 0x55 };
static const uint16 cWAIT_UP_TO_5_MS           = ( 5 );

QueueHandle_t qJ35Monitor_Queue = NULL;
static const uint8 cJ35MONITOR_QUEUE_ITEM_SIZE = sizeof(STRUCT_J35_POWER_MONITOR_DATA);
static const uint8 cJ35MONITOR_QUEUE_LENGTH = 2;

#if defined(USE_DBG_GLOBAL_VAR)
STRUCT_J35_POWER_MONITOR_DATA J35_Monitor_dbg;
//static uint8 gu8Status_dbg[4][8];
//static uint8 index_gu8Status_dbg;
#endif

/*********************************/

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskIDSCore( void *pvParams ) __attribute__ ((noreturn));

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vAppIDSCoreTaskInit(void)
{
	BaseType_t xReturn;

	qJ35Monitor_Queue = FRTOS1_xQueueCreate( cJ35MONITOR_QUEUE_LENGTH, cJ35MONITOR_QUEUE_ITEM_SIZE );
	if (qJ35Monitor_Queue != NULL)
	{
		FRTOS1_vQueueAddToRegistry( qJ35Monitor_Queue, "J35_MONITOR" );
	}

	muxIDSCore = FRTOS1_xSemaphoreCreateMutex();
	FRTOS1_vQueueAddToRegistry( muxIDSCore, "muxIDSCore" );

	// Setup the task...
	xReturn = FRTOS1_xTaskCreate(  vAppTaskIDSCore,
			"IDS_Core",
			cMY_TASK_STACK_SIZE,
			(void *)&u32MyTaskWdgCheck,
			cMY_TASK_PRIORITY,
			&xMyTaskHandle );

	if (xReturn == pdTRUE)
	{
		/* task has been created successfully */
	}
	else
	{
		/* task has not been created because there is insufficient heap memory available */
		// ASSERT
	}
	return;
}

bool boIDSCoreWatchdogTaskCheck(void)
{
	bool boReturn = FALSE;

	// Verify that our task is still running by testing its watchdog variable
	if( u32MyTaskWdgCheck )
	{
		// The watchdog has changed so everything is good.
		boReturn = TRUE;
	}

	return boReturn;
}

void vIDSCoreWatchdogTaskClear(void)
{
	// Reset the check variable so we can tell if it has been set by the next time around.
	u32MyTaskWdgCheck = FALSE;
}

bool boAppIDSCoreAcquireMux( TickType_t u16Timeout )
{
	bool boReturn = FALSE;

	if( NULL != muxIDSCore ) // Make sure the mux is already created
	{
		// Acquire the mutex so new the core functions aren't interrupted during their processing
		if( FRTOS1_xSemaphoreTake( muxIDSCore, u16Timeout ) )
		{
			boReturn = TRUE;
		}
	}

	return boReturn;
}

void vAppIDSCoreReleaseMux(void)
{
	BaseType_t xReturn = pdFALSE;

	if( NULL != muxIDSCore ) // Make sure the mux is already created
	{
		xReturn = FRTOS1_xSemaphoreGive( muxIDSCore );
	}

	if (xReturn == pdPASS)
	{
		/* task has been created successfully */
	}
	else
	{
		/* task has not been created because there is insufficient heap memory available */
		// ASSERT
	}

	return;
}

/**
 * @brief   IDS-CAN Integration functions
 * @param device    unused
 * @return
 */



uint32 IDS_CAN_GetCircuitID( IDS_CAN_DEVICE device )
{
	uint32 u32Return = 0;

	if( device < NUM_IDS_CAN_DEVICES )
	{
		u32Return = gu32CanCircuitId[device];
	}

	return u32Return;
}


IDS_CAN_PRODUCT_ID IDS_CAN_GetProductID(void)
{
	return IDS_CAN_PRODUCT_ID_SIMULATED_PRODUCT;
}

/* LSB:	identifies the function name instance (kitchen slide 1, 2, 3�)	 */
uint8 IDS_CAN_GetFunctionInstance( IDS_CAN_DEVICE device )
{
	uint8 u8Return = 0;

	if( device < NUM_IDS_CAN_DEVICES )
	{
		u8Return = gu8CanFuncInstance[device];
	}

	return u8Return;
}

uint32 IDS_CAN_GetUptime(void)
{
	uint32 time;

	time = app_TICKS_TO_MSEC(FRTOS1_xTaskGetTickCount());

	return time;
}

const uint8 * IDS_CAN_GetAdapterMAC(void)
{
	return &gxCanMacAddress[0];
}

uint8 IDS_CAN_GetDeviceNetworkStatus(IDS_CAN_DEVICE device)
{
	(void)device;

	return 0; // john TODO -- this is where ERROR status goes, among other things
}

void Logic_OnIDSCANSessionOpened( IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id )
{
	(void) device;
	(void) host_address;
	(void) id;
}

void Logic_OnIDSCANSessionClosed( IDS_CAN_DEVICE device, uint8 host_address, IDS_CAN_SESSION_ID id )
{
	(void)device;
	(void)host_address;
	(void)id;

	//if( device < NUM_IDS_CAN_DEVICES && CAN.Active[device] )
	//   CAN.Active[device] = 1; // force an immediate timeout in the task below
}


uint32 CAN0_ReadShorePower(IDS_CAN_DEVICE device)
{
	/* This is just to test the read part, need to be updated ASAP */
	(void)device;
	return (0x5555);
}

uint32 CAN0_ReadBatteryCapacity(IDS_CAN_DEVICE device)
{
	/* limit between 50 and 600, convert FIXED POINT 16:16 */
	uint32 u32Temp;
	uint32 u32result;
	BaseType_t xReturn;
	xPID_DATA_LINK_WITH_DEVICE xPID_DataRead;

	if (device == DC_POWER_MONITOR_1)
	{
		xReturn = FRTOS1_xQueuePeek( qPID_BatCap_DataQueue_R, &xPID_DataRead, 0 ); // Receive an item from a queue without the item being removed.

		if (xReturn == pdPASS) {} else  { /* ASSERT */ }
		/* 1 - DC_POWER_MONITOR_1 */
		u32result = xPID_DataRead.u32Data[(uint8) device];

		u32Temp = (u32result / 1000) & 0xFFFF;
		u32result = u32result - (u32Temp * 1000);
		u32result = ((u32result * 2^16) / 1000) & 0xFFFF;
		u32Temp = (u32Temp << 16) | u32result;
	}
	else
	{
		u32Temp = 0xFFFFFFFF;
	}

	return u32Temp;
}

void CAN0_WriteShorePower(IDS_CAN_DEVICE device, uint32 val)
{
	(void)device;
	(void)val;
}

void CAN0_WriteBatteryCapacity(IDS_CAN_DEVICE device, uint32 val)
{
	xPID_DATA_LINK_WITH_DEVICE xPID_DataWrite;
	BaseType_t xReturn;

	if (device == DC_POWER_MONITOR_1)
	{
		xPID_DataWrite.u32Data[device] = val;  /* DEVICE_DC_POWER_MONITOR_1 */
		if( NULL != qPID_BatCap_DataQueue_W )
		{
			xReturn = FRTOS1_xQueueSendToBack( qPID_BatCap_DataQueue_W, &xPID_DataWrite, 0 );
			if (xReturn == pdPASS) {} else  { /* ASSERT */ }
		}
	}
}


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskIDSCore( void *pvParams )
{
	BaseType_t xReturn;
	volatile uint32 *pu32TaskIsRunning;
	// Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32 *)pvParams;
	//portTickType xLastWakeTime = FRTOS1_xTaskGetTickCount();

	STRUCT_J35_POWER_MONITOR_DATA J35_Monitor;
	uint8 gu8Status[8];
	//uint8 _10mSeconds = 0;
	uint8 IndexMsgToSend;

	IDS_CAN_EnableDevice(AC_POWER_MONITOR);
	IDS_CAN_EnableDevice(DC_POWER_MONITOR_1);
	IDS_CAN_EnableDevice(DC_POWER_MONITOR_2);
	IDS_CAN_EnableDevice(DC_POWER_MONITOR_3);
	IDS_CAN_EnableDevice(SETEC_POWER_MANAGER);

	//IDS_CAN_SetDeviceStatus( AC_POWER_MONITOR, 1, &gu8Status[0] );  // Transmit status
	//IDS_CAN_SetDeviceStatus( DC_POWER_MONITOR, 1, &gu8Status[0] );  // Transmit status
	//IDS_CAN_SetDeviceStatus( SETEC_POWER_MANAGER, 1, &gu8Status[0] );  // Transmit status

	xReturn = FRTOS1_xSemaphoreGive( muxIDSCore ); // Be sure we don't own the mutex
	if (xReturn == pdPASS) {} else  { /* ASSERT */ }
	IndexMsgToSend = (uint8)AC_POWER_MONITOR;

	for(;;)
	{
		if( FRTOS1_xSemaphoreTake( muxIDSCore, cWAIT_UP_TO_5_MS ) )   // We'll wait for the mutex 5mS max
		{
			// Only run low-level stuff when we have the mutex
			IDS_CAN_Task(); // Pump the IDS-CAN / Core subsystem
			xReturn = FRTOS1_xSemaphoreGive( muxIDSCore );
			if (xReturn == pdPASS) {} else  { /* ASSERT */ }
		}

		// Set status.  The network layer will build and transmit the status message at the appropriate timing
		if (qJ35Monitor_Queue != NULL)
		{
			if( xQueueReceive( qJ35Monitor_Queue, &J35_Monitor, 0 ) ) // ( 100 / portTICK_PERIOD_MS ) ) )
			{
				// update structure J35_Monitor
				//lint -e644 #Disable because Variable 'J35_Monitor' (line 303) is initialized with xQueueReceive
#if defined(USE_DBG_GLOBAL_VAR)
				memcpy( &J35_Monitor_dbg, &J35_Monitor, sizeof(J35_Monitor) ); /* just to debug */
#endif
			}
		}

		switch(IndexMsgToSend)
		{
		case AC_POWER_MONITOR:
			gu8Status[0] = J35_Monitor.ac.u16Voltage.val.u8i; //lint +e644 #enable warning 644
			gu8Status[1] = J35_Monitor.ac.u16Voltage.val.u8q;
			gu8Status[2] = J35_Monitor.ac.u16Current.val.u8i;
			gu8Status[3] = J35_Monitor.ac.u16Current.val.u8q;
			gu8Status[4] = (uint8)J35_Monitor.ac.u8Val.bits.u4l;
			IDS_CAN_SetDeviceStatus( AC_POWER_MONITOR, 5, &gu8Status[0] );
			IndexMsgToSend = (uint8)DC_POWER_MONITOR_1;
			break;

		case DC_POWER_MONITOR_1:
			gu8Status[0] = J35_Monitor.dc_bat.u16Voltage.val.u8i;
			gu8Status[1] = J35_Monitor.dc_bat.u16Voltage.val.u8q;
			gu8Status[2] = J35_Monitor.dc_bat.u16Current.val.u8i;
			gu8Status[3] = J35_Monitor.dc_bat.u16Current.val.u8q;
			gu8Status[4] = (uint8)J35_Monitor.dc_bat.u8Val.all;
			gu8Status[5] = (uint8)(J35_Monitor.dc_bat.u16Time_discharge >> 8);
			gu8Status[6] = (uint8)J35_Monitor.dc_bat.u16Time_discharge;
			IDS_CAN_SetDeviceStatus( DC_POWER_MONITOR_1, 7, &gu8Status[0] );
			IndexMsgToSend = (uint8)DC_POWER_MONITOR_2;
			break;

		case DC_POWER_MONITOR_2:
			gu8Status[0] = J35_Monitor.dc_bat_aux.u16Voltage.val.u8i;
			gu8Status[1] = J35_Monitor.dc_bat_aux.u16Voltage.val.u8q;
			gu8Status[2] = J35_Monitor.dc_bat_aux.u16Current.val.u8i;
			gu8Status[3] = J35_Monitor.dc_bat_aux.u16Current.val.u8q;
			gu8Status[4] = (uint8)J35_Monitor.dc_bat_aux.u8Val.all;
			gu8Status[5] = (uint8)(J35_Monitor.dc_bat_aux.u16Time_discharge >> 8);
			gu8Status[6] = (uint8)J35_Monitor.dc_bat_aux.u16Time_discharge;
			IDS_CAN_SetDeviceStatus( DC_POWER_MONITOR_2, 7, &gu8Status[0] );
			IndexMsgToSend = (uint8)DC_POWER_MONITOR_3;
			break;

		case DC_POWER_MONITOR_3:
			gu8Status[0] = J35_Monitor.dc_solar.u16Voltage.val.u8i;
			gu8Status[1] = J35_Monitor.dc_solar.u16Voltage.val.u8q;
			gu8Status[2] = J35_Monitor.dc_solar.u16Current.val.u8i;
			gu8Status[3] = J35_Monitor.dc_solar.u16Current.val.u8q;
			gu8Status[4] = (uint8)J35_Monitor.dc_solar.u8Val.all;
			gu8Status[5] = (uint8)(J35_Monitor.dc_solar.u16Time_discharge >> 8);
			gu8Status[6] = (uint8)J35_Monitor.dc_solar.u16Time_discharge;
			IDS_CAN_SetDeviceStatus( DC_POWER_MONITOR_3, 7, &gu8Status[0] );
			IndexMsgToSend = (uint8)SETEC_POWER_MANAGER;
			break;

		case SETEC_POWER_MANAGER:
			gu8Status[0] = J35_Monitor.manager.u8Mode;
			IDS_CAN_SetDeviceStatus( SETEC_POWER_MANAGER, 1, &gu8Status[0] );
			IndexMsgToSend = (uint8)AC_POWER_MONITOR;
			break;

		default:
			break;
		}

		// Kick the dog
		FRTOS1_taskENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		FRTOS1_taskEXIT_CRITICAL();

		FRTOS1_vTaskDelay(1);
	}

	// Task function does not return
}

void IDS_CAN_SetAdapterMAC( const uint8 *pMac )
{
	memcpy( &gu8CAN0MACAddress[0], pMac, 6 );

	return;
}

void IDS_CAN_SetCircuitID( IDS_CAN_DEVICE device, uint32 circuit_id )
{
	if( device < NUM_IDS_CAN_DEVICES )
	{
		gu32CanCircuitId[device] = circuit_id;
	}

	return;
}

IDS_CAN_FUNCTION_NAME IDS_CAN_GetFunctionName( IDS_CAN_DEVICE device )
{
	uint16 u16Return = IDS_CAN_FUNCTION_NAME_UNKNOWN;

	if( device < NUM_IDS_CAN_DEVICES )
	{
		u16Return = gu16CanFuncName[device];
	}

	return u16Return;
}

void IDS_CAN_SetFunctionInstance( IDS_CAN_DEVICE device, uint8 function_instance )
{
	if( device < NUM_IDS_CAN_DEVICES )
	{
		gu8CanFuncInstance[device] = function_instance;
	}

	return;
}

void IDS_CAN_SetFunctionName( IDS_CAN_DEVICE device, IDS_CAN_FUNCTION_NAME function_name )
{
	if( device < NUM_IDS_CAN_DEVICES )
	{
		gu16CanFuncName[device] = function_name;
	}

	return;
}

uint8 u8MyRVDeviceAskProductionByte(void)
{
	return gu8ProductionByte;
}




void vMyRVDeviceSetProductionByte( uint8 u8NewProdByte )
{
	gu8ProductionByte = u8NewProdByte;

	return;
}

#endif // INCLUDE_IDS_CORE_CAN




