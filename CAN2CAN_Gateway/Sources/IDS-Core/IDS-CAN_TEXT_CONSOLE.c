// IDS-CAN_TEXT_CONSOLE.c
// IDS-CAN Text console
// Version 2.7
// (c) 2016, 2017 Innovative Design Solutions, Inc.
// All rights reserved

// History
// Version 2.1   Initial version of TEXT_CONSOLE from IDS-CAN v2.1 Jan 2016
// Version 2.4   Minor update
// Version 2.7   Minor update

#include "global.h"

#ifdef DECLARE_IDS_CAN_TEXT_CONSOLES

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#ifdef HEADER

// management routines
void _IDS_CAN_TEXT_CONSOLE_Init(void);     // private function do not call, instead call IDS_CAN_Init()
void _IDS_CAN_TEXT_CONSOLE_Task10ms(void); // private function do not call, instead call IDS_CAN_Task()

// updates the text in the console
// strings points to an array of strings
//    strings[0] = console line 1
//    strings[1] = console line 2
//    ...
//    strings[n] = console line n
// strings can end with NULL, which indicates remaining console lines are blanked out
void IDS_CAN_SetConsoleText(IDS_CAN_DEVICE device, const char * const * strings);

// gets current text in console
const char * IDS_CAN_GetConsoleText(IDS_CAN_DEVICE device, uint8 row);

#else // HEADER

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#endif

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#undef SECONDS
#define SECONDS(s)                  (uint16)((s) / .010 + 0.5)

// verify console size is valid
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) ((w) < 1) || ((w) > 64) || ((h) < 1) || ((h) > 32) ||
#if DECLARE_IDS_CAN_TEXT_CONSOLES FALSE
	#error IDS-CAN TEXT_CONSOLE size invalid: console must be between 1 x 1 and 64 x 32 characters
#endif

// enumerate the consoles that are present
typedef enum {
	#undef DECLARE_IDS_CAN_TEXT_CONSOLE
	#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) CONSOLE_##d,
	DECLARE_IDS_CAN_TEXT_CONSOLES
	NUM_CONSOLES
} CONSOLE;

// allocate RAM for each console
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) static char Text_##d[(w)*(h)];
DECLARE_IDS_CAN_TEXT_CONSOLES

// console management
static int8 Timer[NUM_CONSOLES];        // timer that manages when text messages are sent from the console
static uint8 TxRow[NUM_CONSOLES];       // keeps track of the current row being transmitted by the console
static uint8 TxCol[NUM_CONSOLES];       // keeps track of the current column being transmitted by the console
static uint8 TextChanged[NUM_CONSOLES]; // keeps track of whether text in the console has changed or not

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// ROM tables

// IDS_CAN_DEVICE associated with each console
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) (d),
static const IDS_CAN_DEVICE Device[NUM_CONSOLES] = { DECLARE_IDS_CAN_TEXT_CONSOLES };

// width of each console
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) (w),
static const uint8 Width[NUM_CONSOLES] = { DECLARE_IDS_CAN_TEXT_CONSOLES };

// height of each console
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) (h),
static const uint8 Height[NUM_CONSOLES] = { DECLARE_IDS_CAN_TEXT_CONSOLES };

// maximum scan column for each console
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) (((w) - 1) / 8),
static const uint8 TxColMax[NUM_CONSOLES] = { DECLARE_IDS_CAN_TEXT_CONSOLES };

// pointer to working text RAM
#undef DECLARE_IDS_CAN_TEXT_CONSOLE
#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) Text_##d,
static char * const Text[NUM_CONSOLES] = { DECLARE_IDS_CAN_TEXT_CONSOLES };

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// lookup the index to the console that supports the given IDS_CAN_DEVICE
static CONSOLE GetConsoleForDevice(IDS_CAN_DEVICE device)
{
	#undef DECLARE_IDS_CAN_TEXT_CONSOLE
	#define DECLARE_IDS_CAN_TEXT_CONSOLE(d, w, h) if (device == (d)) return CONSOLE_##d;
	DECLARE_IDS_CAN_TEXT_CONSOLES
	return NUM_CONSOLES;
}

// gets a pointer to the text in RAM, or NULL if request is out of range
static char * GetText(CONSOLE c, uint8 row)
{
	if (c >= NUM_CONSOLES)
		return NULL;
	if (row >= Height[c])
		return NULL;
	return Text[c] + (row * Width[c]);
}

// returns TRUE if text was set, FALSE if unable to set text (invalid line)
static uint8 SetText(CONSOLE c, uint8 row, const char * src)
{
	char * dst;
	uint8 n;

	dst = GetText(c, row);
	if (dst == NULL)
		return FALSE;

	// look for changed text
	for (n = Width[c];;)
	{
		// is the text changing?
		if (*dst != *src)
		{
			TextChanged[c] = TRUE; // text has changed
			(void) memcpy(dst, src, n); // copy remaining text
			break;
		}

		if (!*src)
			break; // stop when EOL is reached

		if (!--n)
			break; // no more characters

		src++;
		dst++;
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const char * IDS_CAN_GetConsoleText(IDS_CAN_DEVICE device, uint8 row)
{
	char * text = GetText(GetConsoleForDevice(device), row);
	if (text == NULL)
		return "";
	return text;
}

void IDS_CAN_SetConsoleText(IDS_CAN_DEVICE device, const char * const * strings)
{
	CONSOLE c = GetConsoleForDevice(device);
	if (c < NUM_CONSOLES)
	{
		const char * text;
		uint8 row = 0;

		// copy text line by line
		do
		{
			// get next line of text
			text = *strings;

			// make sure the text is valid
			if (text == NULL)
				text = "";
			else
				strings++;
		} while (SetText(c, row++, text)); // set the text and exit loop after all rows updated
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// re-initializes all text consoles
void _IDS_CAN_TEXT_CONSOLE_Init(void)
{
	CONSOLE c;

	// forces all frames to rescan
	(void)memset(TextChanged, TRUE, sizeof(TextChanged));

	// clear the text in all consoles
	for (c=(CONSOLE)0; c<NUM_CONSOLES; c++)
	{
		uint8 row = 0;

		for (;; row++)
		{
			char * text = GetText(c, row);
			if (text == NULL)
				break;
			*text = 0;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#pragma INLINE
static uint8 TransmitTextMessage(CONSOLE c)
{
	CAN_TX_MESSAGE msg;
	const char * text;
	uint8 col;

	if (c >= NUM_CONSOLES)
		return TRUE;

	// get pointer to text
	text = GetText(c, TxRow[c]);
	ASSERT(text != NULL);
	if (text == NULL)
		return TRUE; // something is broke, pretend like we sent a message and move on

	// create outgoing message ID
	msg.ID = IDS_CAN_CreateCanId29(IDS_CAN_MESSAGE_TYPE_TEXT_CONSOLE, IDS_CAN_GetDeviceAddress(Device[c]), IDS_CAN_ADDRESS_BROADCAST, (TxRow[c] << 3) + TxCol[c]);

	// offset to appropriate column
	col = TxCol[c] * 8;
	text += col;

	// how many payload bytes are available?
	ASSERT(col < Width[c]);
	if (col >= Width[c])
		return TRUE; // something is broke, pretend like we sent a message and move on
	msg.Length = Width[c] - col;
	if (msg.Length > 8)
		msg.Length = 8; // all 8 bytes of payload are valid

	// copy text into message payload
	(void) memcpy(msg.Data, text, msg.Length); // copy text

	// attempt transmit
	return IDS_CAN_Tx(Device[c], &msg);
}

// determines the current maximum for for text being transmitted
#pragma INLINE
static uint8 FindLastVisibleRow(CONSOLE c)
{
	if (c < NUM_CONSOLES)
	{
		uint8 row = Height[c];
		while (row > 0)
		{
			if (*GetText(c, --row))
				return row; // non-zero text means that this row is displayed
		}
	}
	return 0;
}

#pragma INLINE
static void TransmitConsoleText(CONSOLE c)
{
	if (c >= NUM_CONSOLES)
		return;

	// start over when value is out of range
	if (TxRow[c] >= Height[c] || TxCol[c] > TxColMax[c])
	{
		TxRow[c] = FindLastVisibleRow(c);
		TxCol[c] = TxColMax[c];
	}

	while (TransmitTextMessage(c))
	{
		// move to next row/col
		if (TxCol[c])
		{
			// move to next column
			TxCol[c]--;
		}
		else if (TxRow[c])
		{
			// move to next row
			TxRow[c]--;
			TxCol[c] = TxColMax[c];
			return;
		}
		else
		{
			// end of scan
			TxRow[c] = 0xFF; // restart scan
			Timer[c] += SECONDS(1); // next scan starts 1 second from now
			if (Timer[c] < 0)
				Timer[c] = 0;
			return;
		}
	}
}

void _IDS_CAN_TEXT_CONSOLE_Task10ms(void)
{
	// update all consoles
	CONSOLE c;
	for (c = (CONSOLE)0; c < NUM_CONSOLES; c++)
	{
		// make sure device is online
		if (!IDS_CAN_IsDeviceOnline(Device[c]))
		{
			// restart scanning when device comes back online
			TextChanged[c] = TRUE;
			continue;
		}

		// respond to text changes
		if (TextChanged[c])
		{
			TextChanged[c] = FALSE;
			TxRow[c] = 0xFF; // restart scan
			Timer[c] = 0; // immediately transmit new frame
		}

		if (Timer[c] > -127)
			Timer[c]--;

		// is it time to transmit?
		if (Timer[c] <= 0)
			TransmitConsoleText(c);
	}
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#endif // HEADER
#endif // DECLARE_IDS_CAN_TEXT_CONSOLES
