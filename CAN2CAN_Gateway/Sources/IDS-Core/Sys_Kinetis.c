// Sys_Kinetis.c
// Standard IDS system functions for NXP Kinetis microcontrollers
// Version 1.8
// (c) 2010, 2012, 2013, 2016 Innovative Design Solutions, Inc.
// All rights reserved

// history
// 1.0  First revision - multithreading is not fully debugged and should
//      not be used
// 1.1  Added Random16() to generate a random 16-bit number
// 1.2  Added sprintf() support
// 1.3  Added Randomize16() to initialize random number seed
// 1.4  Added Random32() and Randomize32()
//      Updated Wait() routine to support 24 MHz bus
//      Moved common routines to non-banked memory
// 1.5  Updated Wait() routine to support 25 MHz bus
// 1.6  Moved RNG seed value into NO_INIT RAM
// 1.7  Debugged multithreading support
// 1.8  Multithreading is now interrput aware, switches contexts to a
//      stack dedicated to interrupt use
// 1.9  Adapted from Sys HC12.c to work with Kinetis parts.

// NOTES:
//
// This module is intended to work with the Metrowerks CodeWarrior for HC12
// compiler.
//
// Microcontroller reset and C environment startup code is provided by the
// compiler.  Hence it is not included here.  User code begins executing
// at main()
//
// This module defines the standard data types (uint8, uint16, etc...)
// 
// This module provides a portable wait routine
//     Wait()
//
// This module provides a portable reset routine
//     ForceReset()
// NOTE: the force reset routine relies on the COP and will not perform
//       a true reset if the COP is disabled
//
// This module provides standard critical section support via
//     EnterCriticalSection();
//     LeaveCriticalSection();
//
// This module provides stlib functions memclr, memset and memcpy
// It also provides for atomic reads/writes

#include "global.h"
#include "Application.h"

// support legacy OS time functions using FreeRTOS (MUST USE 1ms Tick for RTOS)
//#if( configTICK_RATE_HZ == 1000 )
#if( 1 )
uint16 OS_GetElapsedMilliseconds16(void)
{
    TickType_t ticks;

    ticks = FRTOS1_xTaskGetTickCount();

    return (uint16)( ticks );
}
uint32 OS_GetElapsedMilliseconds32(void)
{
    TickType_t ticks;

    ticks = FRTOS1_xTaskGetTickCount();
    return ( (uint32)( ticks ) );
}
#else
#error "!!! RTOS Tick Rate must be 1KHz !!!"
#endif // check RTOS tick rate


const char IDS_PartNumber[] = { '2', '1', '9', '2', '4', '-', 'A' };


//  0123456789012345678901
// "Feb dd yyyy hh:mm:ss
#pragma MESSAGE DISABLE C3303
const char SoftwareBuildDateString[] = __DATE__;
const char SoftwareBuildTimeString[] = __TIME__;
#pragma MESSAGE DEFAULT C3303

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

const uint8 *pSystemAskSoftwarePartnum(void)
{
    return &IDS_PartNumber[0];
}
const uint8 *pSystemAskSoftwareBuildDate(void)
{
    return &SoftwareBuildDateString[0];
}
const uint8 *pSystemAskSoftwareBuildTime(void)
{
    return &SoftwareBuildTimeString[0];
}


#define GetDigit(buf, pos)    (uint8)(((buf[pos] - '0') << 4) + buf[pos+1] - '0')
uint8 SoftwareBuildYear(void)   { return GetDigit(SoftwareBuildDateString, 9); }
uint8 SoftwareBuildDay(void)    { return GetDigit(SoftwareBuildDateString, 4); }
uint8 SoftwareBuildHour(void)   { return GetDigit(SoftwareBuildTimeString, 0); }
uint8 SoftwareBuildMinute(void) { return GetDigit(SoftwareBuildTimeString, 3); }
uint8 SoftwareBuildSecond(void) { return GetDigit(SoftwareBuildTimeString, 6); }

#pragma MESSAGE DISABLE C4000
#pragma MESSAGE DISABLE C4001
#pragma MESSAGE DISABLE C5660
uint8 SoftwareBuildMonth(void)
{
	// via process of elimination
	// NOTE: this may be optimized on some compilers!

  if (SoftwareBuildDateString[0] == 'F')
		return 0x02; // Feb

	if (SoftwareBuildDateString[0] == 'S')
		return 0x09; // Sep

	if (SoftwareBuildDateString[0] == 'O')
		return 0x10; // Oct

	if (SoftwareBuildDateString[0] == 'N')
		return 0x11; // Nov

	if (SoftwareBuildDateString[0] == 'D')
		return 0x12; // Dec

	if (SoftwareBuildDateString[1] == 'p')
		return 0x04; // Apr

	if (SoftwareBuildDateString[2] == 'r')
		return 0x03; // Mar

	if (SoftwareBuildDateString[2] == 'y')
		return 0x05; // May

	if (SoftwareBuildDateString[2] == 'l')
		return 0x07; // Jul

	if (SoftwareBuildDateString[2] == 'g')
		return 0x08; // Aug

	if (SoftwareBuildDateString[1] == 'a')
		return 0x01; // Jan

	return 0x06; // Jun
}
#pragma MESSAGE DEFAULT C4000
#pragma MESSAGE DEFAULT C4001
#pragma MESSAGE DEFAULT C5660

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

#ifdef DESCRIPTION
const char _Description[] = DESCRIPTION;
#endif
const char _Copyright[] = "Copyright Innovative Design Solutions, Inc.";

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

void ForceReset(void)
{
    #if defined( MCU_MKE06Z4 ) || defined( CPU_MK60DN512VLL10 )
        CPU_SystemReset();
    #else
        #error No reset function defined for this processor.
    #endif
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
void system_unrecoverable_error(void)
{	
	// called to reset the processor from an error condition - gives a single point to breakpoint while debugging	
	taskDISABLE_INTERRUPTS();
	
    #if defined( MCU_MKE06Z4 ) || defined( CPU_MK60DN512VLL10 )
        CPU_SystemReset();
    #else
        #error No reset function defined for this processor.
    #endif
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////


void AtomicRead(const void * src, void * dst, uint16 count)
{
  EnterCriticalSection();
	(void)memcpy(dst, src, count);
	LeaveCriticalSection();  
}

uint32 AtomicRead32(const void * address)
{
  uint32 value;
	AtomicRead(address, &value, sizeof(value));
	return value;
}

void AtomicWrite32(void * address, uint32 data)
{
  AtomicWrite(address, &data, sizeof(data));
}

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////


uint32 Reverse32(uint32 input)
{
    uint32 ret = 0;
    
    ret = (input & 0xFF000000) >> 24;
    ret |= (input & 0x00FF0000) >> 8;
    ret |= (input & 0x0000FF00) << 8;
    ret |= (input & 0x000000FF) << 24;
    
    return ret;
}

uint16 Reverse16(uint16 input)
{
    uint16 ret = 0;
    
    ret = (input & 0xFF00) >> 8;
    ret |= (input & 0x00FF) << 8;
    
    return ret;
}

