/**---------------------------------------------------------------------------------------------------------------------
 * @file            Application.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 3, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPLICATION_H_
#define SOURCES_APPLICATION_H_

#define     CPU_MK60DN512VLL10
#define     __LITTLE_ENDIAN__

// DEBUGGING AND TESTING MACROS:
//#define     WATCHDOG_DISABLE
//#define     DEBUG_CAN_RX
//#define     DEBUG_CAN_TX


#define     INCLUDE_IDS_CORE_CAN
//#define     INCLUDE_ETHERNET_SUPPORT
#define     USE_COBS_FOR_HOST_SERIAL_TRAFFIC

// Global PEx includes
#include "CPU.h"
#include "Events.h"
#include "CAN0.h"
#include "WDog1.h"
#include "WatchDogLdd1.h"
#include "HF1.h"
#include "RF_ENABLE_OUT.h"
#include "BitIoLdd1.h"
#include "RF_TIMER.h"
#include "TimerIntLdd1.h"
#include "CAN0_STANDBY_OUT.h"
#include "CAN1_STANDBY_OUT.h"
#include "BitIoLdd2.h"
#include "FRTOS1.h"
#include "MCUC1.h"
#include "UTIL1.h"
#include "SYS1.h"
#include "RTT1.h"
#include "WAIT1.h"
#include "CLS1.h"
#include "XF1.h"
#include "CS1.h"
#include "TU1.h"
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"


#include <stdio.h>
#include <string.h>

#include "IDS-Core/Sys_Kinetis.h"

// App module includes
#include "HAL.h"
#include "CRC.h"

// IDS-Core functionality
#ifdef INCLUDE_IDS_CORE_CAN
#include "IDS-Core/AppIDSCoreModule.h"
#endif // INCLUDE_IDS_CORE_CAN

#include "AppCAN0Module.h"
#include "AppCAN1Module.h"

#ifdef INCLUDE_ETHERNET_SUPPORT
// Ethernet
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_DHCP.h"
#include "FreeRTOS_IP_Private.h"
#include "AppEthernetModule.h"
#endif // INCLUDE_ETHERNET_SUPPORT

// Watchdog
#include "AppWatchdogModule.h"

#ifdef DEBUG_TEST_SEQUENCE_STATE_MACHINE
  #include "AppRunTestSequence.h"
#endif // DEBUG_TEST_SEQUENCE_STATE_MACHINE


typedef uint8 HAL_ETHERNET_MAC_ADDR_TYPE;

//#define USE_DBG_GLOBAL_VAR		1
//#define USE_DBG_PIN				1
#define DEBUG_WITH_ASSERT

void vApplicationInitTasks(void);


void vAppDebugPrintAnnouncement(void);
void vAppDebugPrintThisNum( uint8 u8Num );
void vAppDebugDisplayActiveOuts( uint32 u32Num );
void vAppDebugShowOutputVoltageOnUART( uint8 eWhichOutput );
void vAppTestCANBuffering(void);

#endif // SOURCES_APPLICATION_H_
