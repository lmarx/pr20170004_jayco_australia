/**---------------------------------------------------------------------------------------------------------------------
 * @file            TesterCANModule.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 7, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "Application.h"
#include "IDS-CORE\global.h"

/**
 *===================================================================================*
    29-bit  message identifier (point-to-point) for IDS-CAN protocol implementation
    (rout)   (typH)    (Source )        (typL)     (Target)    (Extra Data Byte)
    Byte 3                 Byte 2               Byte 1          Byte 0
    --------------------   ------------------    ---------------  ---------------
    3 3 2   2 2 2   2 2   2 2 2 2 1 1    1 1    1 1 1 1 1 1
    1 0 9   8 7 6   5 4   3 2 1 0 9 8    7 6    5 4 3 2 1 0 9 8  7 6 5 4 3 2 1 0

    x x x   t t t   s s   s s s s s s    t t    d d d d d d d d  p p p p p p p p
    N       | | |   | |   | | | | | |    | |    | | | | | | | |  | | | | | | | |
    o       | | |   | |   | | | | | |    | |    | | | | | | | |  \_\_\_\_\_\_\_\___  extended payload data (9th byte of payload)
    t       | | |   | |   | | | | | |    | |    | | | | | | | |
      i     | | |   | |   | | | | | |    | |    \_\_\_\_\_\_\_\____________________  destination address of message
      n     | | |   | |   | | | | | |    | |
        H   | | |   | |   | | | | | |    \_\_______________________________________  message type (lsbs)
        a   | | |   | |   | | | | | |
        r   | | |   \_\___\_\_\_\_\_\______________________________________________  source address of message
        d   | | |
        w   \_\_\__________________________________________________________________  message type (msbs)
        a
        r
        e
 */

/**
 *===================================================================================*
     11-bit message identifier (broadcast) for IDS-CAN protocol implementation
     (rout)  (type)    (Source )
     Byte 1          Byte 0
     -----------  -------- -----------------
     1 1 1 1 1    1
     5 4 3 2 1    0 9 8   7 6 5 4 3 2 1 0

     x x x x x    t t t   s s s s s s s s
     N           | | |   | | | | | | | |
     o           | | |   | | | | | | | |
     t           | | |   | | | | | | | |
        i        | | |   | | | | | | | |
        n        | | |   | | | | | | | |
           H     | | |   | | | | | | | |
           a     | | |   | | | | | | | |
           r     | | |   \_\_\_\_\_\_\_\___  source address of message
           d     | | |
           w     \_\_\_____________________  message type
           a
           r
           e
 */

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) macro definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) variables
 * ---------------------------------------------------------------------------------------------------------------------
 */

// Task parameters
static const uint8 cCAN0_RX_TASK_PRIORITY = ( configMAX_PRIORITIES - 2 );
static const uint8 cCAN0_TX_TASK_PRIORITY = ( configMAX_PRIORITIES - 2 );
static const uint16 cCAN0_RX_TASK_STACK_SIZE = 240;
static const uint16 cCAN0_TX_TASK_STACK_SIZE = 180;
static const uint16 cCAN0_WAIT_UP_TO_100_MS = ( 100 / portTICK_PERIOD_MS );

static volatile uint32 u32CAN0_RxTaskWdgCheck = FALSE;
static volatile uint32 u32CAN0_TxTaskWdgCheck = FALSE;
static TaskHandle_t xCAN0_RxTaskHandle = NULL;
static TaskHandle_t xCAN0_TxTaskHandle = NULL;


// Input and output queues:
static QueueHandle_t qCan0_RxQueue = NULL;
static QueueHandle_t qCan0_TxQueue = NULL;

static const uint8 cCAN0_RX_QUEUE_ITEM_SIZE = sizeof(xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE);
static const uint8 cCAN0_RX_QUEUE_LENGTH = 32;

static const uint8 cCAN0_TX_QUEUE_ITEM_SIZE = sizeof(xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE);
static const uint8 cCAN0_TX_QUEUE_LENGTH = 32;

static LDD_TDeviceData *gpCAN0_LowLevelDeviceState = NULL;

#if defined( DEBUG_CAN_RX ) || defined( DEBUG_CAN_TX )
static uint8 gstrOutData[64];
#endif // DEBUG_CAN_RX or DEBUG_CAN_TX

static const uint8 cCAN0_TRANSMIT_BUFFER = 8;   // The number of the CAN message buffer used for transmitting
static uint16 gu16LastTxTime;

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function declarations
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskCan0_Rx( void *pvParams ) __attribute__ ((noreturn));
static void vAppTaskCan0_Tx( void *pvParams ) __attribute__ ((noreturn));


#ifdef DEBUG_CAN_TX // Debug helpers
static void vAppDisplayCanTxError( uint8 u8CanError );
static void vAppDisplayTimeoutError(void);
#endif // DEBUG_CAN_TX

#if defined( DEBUG_CAN_RX ) || defined( DEBUG_CAN_TX )
static void vAppDisplayRawCanPacket( xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *pxCanPacket, bool boTx );
#endif // DEBUG_CAN_RX || DEBUG_CAN_TX

#if defined( DEBUG_WITH_ASSERT )
static void vAppCan0TxError( LDD_TError u16CanError );
#endif

/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Public function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
void vAppCAN0TaskInit(void)
{   
	BaseType_t xReturn;

	qCan0_RxQueue = FRTOS1_xQueueCreate( cCAN0_RX_QUEUE_LENGTH, cCAN0_RX_QUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qCan0_RxQueue, "CAN_Rx" );

	qCan0_TxQueue = FRTOS1_xQueueCreate( cCAN0_TX_QUEUE_LENGTH, cCAN0_TX_QUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qCan0_TxQueue, "CAN_Tx" );

	// Setup the Receive task
	xReturn = FRTOS1_xTaskCreate( vAppTaskCan0_Rx,
			"CAN_RxProc",
			cCAN0_RX_TASK_STACK_SIZE,
			(void *)&u32CAN0_RxTaskWdgCheck,
			cCAN0_RX_TASK_PRIORITY,
			&xCAN0_RxTaskHandle );

	ASSERT(xReturn == pdTRUE); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	// Setup the Transmit task
	xReturn = FRTOS1_xTaskCreate( vAppTaskCan0_Tx,
			"CAN_TxProc",
			cCAN0_TX_TASK_STACK_SIZE,
			(void *)&u32CAN0_TxTaskWdgCheck,
			cCAN0_TX_TASK_PRIORITY,
			&xCAN0_TxTaskHandle );

	ASSERT(xReturn == pdTRUE); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	// Initialize the low-level driver
	gpCAN0_LowLevelDeviceState = (LDD_TDeviceData *) CAN0_DeviceData;
	return;
}

bool boCAN0_RxWatchdogTaskCheck(void)
{
	bool boReturn = FALSE;

	// Verify that our task is still running by testing its watchdog variable
	if( u32CAN0_RxTaskWdgCheck )
	{
		// The watchdog has changed so everything is good.
		boReturn = TRUE;
	}

	return boReturn;
}

bool boCAN0_TxWatchdogTaskCheck(void)
{
	bool boReturn = FALSE;

	// Verify that our task is still running by testing its watchdog variable
	if( u32CAN0_TxTaskWdgCheck )
	{
		// The watchdog has changed so everything is good.
		boReturn = TRUE;
	}

	return boReturn;
}

void vCAN0_RxWatchdogTaskClear(void)
{
	// Reset the check variable so we can tell if it has been set by the next time around.
	u32CAN0_RxTaskWdgCheck = FALSE;
}

void vCAN0_TxWatchdogTaskClear(void)
{
	// Reset the check variable so we can tell if it has been set by the next time around.
	u32CAN0_TxTaskWdgCheck = FALSE;
}

// The following routine accepts packets for transmittal from the IDS-CAN layer
uint8 CAN_Tx( const xCAN_TX_MESSAGE_TYPE *pIdsCanMessage, pvFN_PTR_TYPE pFnCallback )
{

#ifdef INCLUDE_IDS_CORE_CAN
	//uint8 index;
	BaseType_t xReturn;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xOutput;

	// Convert an IDS-CAN message into our universal format.
	xOutput.xLddCanRxFrame.Length = pIdsCanMessage->Length;
	xOutput.xLddCanRxFrame.MessageID = ( pIdsCanMessage->ID );  // pre-convert to CAN cell IDR format

	memcpy( &xOutput.u8Data[0], & ( pIdsCanMessage->Data[0] ), pIdsCanMessage->Length ); // Copy the data
	xOutput.xLddCanRxFrame.Data = & ( xOutput.u8Data[0] ); // load the data pointer into the packet header
	xOutput.pFnCallback = pFnCallback;
	vAppCANTransmitRaw( &xOutput );    // Transmit it on the bus

	gu16LastTxTime = (uint16)FRTOS1_xTaskGetTickCount(); // update timestamp variable

	// handle the finished transmission
	xOutput.xLddCanRxFrame.TimeStamp = gu16LastTxTime;

	if( NULL != qCan0_RxQueue )   // Don't try to use a non-existent queue
	{
		xReturn = FRTOS1_xQueueSendToBack( qCan0_RxQueue, &xOutput, 0 ); // queue the transmitted CAN frame into the receive queue
		ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
	}
	else
	{
		// Queue is not there?
	}

	return TRUE;
#else
	return FALSE; // We aren't compiled to support the IDS-CAN layer
#endif // INCLUDE_IDS_CORE_CAN

}


void vAppCANTransmitRaw( xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *pxCanFrameToSend )
{
	BaseType_t xReturn;
	LDD_TError u16Status;

	if( pxCanFrameToSend->xLddCanRxFrame.MessageID & ( CAN_ID_REMOTE_TRANSMISSION_REQUEST ) )
	{
		pxCanFrameToSend->xLddCanRxFrame.FrameType = LDD_CAN_REMOTE_FRAME;
	}
	else
	{
		pxCanFrameToSend->xLddCanRxFrame.FrameType = LDD_CAN_DATA_FRAME;
	}

	pxCanFrameToSend->xLddCanRxFrame.LocPriority = 0;
	pxCanFrameToSend->xLddCanRxFrame.Data = & ( pxCanFrameToSend->u8Data[0] ); // Put the address of the data buffer into the low-level frame
	u16Status = CAN0_SendFrame( gpCAN0_LowLevelDeviceState, cCAN0_TRANSMIT_BUFFER, & ( pxCanFrameToSend->xLddCanRxFrame ) );

	if( ERR_OK == u16Status )
	{
		// TODO callback?
	}
	else if( ERR_BUSY == u16Status )
	{
		// Queue it for now..
		xReturn = FRTOS1_xQueueSendToBack( qCan0_TxQueue, pxCanFrameToSend, 0 ); // Don't wait for a slot to become available in the queue
		ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
	}
	else
	{
#ifdef DEBUG_WITH_ASSERT
				vAppCan0TxError(u16Status); // a CAN Send Error occurred.
#endif
	}
}


void vISR_AppCAN0_ModuleOnRxEvent( LDD_CAN_TMBIndex u8RxBufferIdx )
{
	BaseType_t xReturn;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCanRxReadFrame;
	LDD_TError u16Status;

	xCanRxReadFrame.xLddCanRxFrame.Data = &xCanRxReadFrame.u8Data[0]; // set the payload pointer to our byte array
	u16Status = CAN0_ReadFrame( gpCAN0_LowLevelDeviceState, u8RxBufferIdx, &xCanRxReadFrame.xLddCanRxFrame );

	if( ERR_OK == u16Status )
	{
		xReturn = FRTOS1_xQueueSendToBackFromISR( qCan0_RxQueue, &xCanRxReadFrame, 0 ); // queue the CAN frame
		ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
	}
	else
	{
#ifdef DEBUG_WITH_ASSERT
				vAppCan0TxError(u16Status); // a CAN Send Error occurred.
#endif
	}
}

void vISR_AppCAN0_ModuleOnTxBufferEmpty( LDD_CAN_TMBIndex u8TxBufferIdx )
{
	(void)u8TxBufferIdx;

	// Not currently relying on this for anything.

	return;
}


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
static void vAppTaskCan0_Rx( void *pvParams )
{
	volatile uint32 *pu32TaskIsRunning;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCanFrameToProcess;

#ifdef INCLUDE_IDS_CORE_CAN
	xCAN_RX_MESSAGE_TYPE xLegacyPacket;
#endif // INCLUDE_IDS_CORE_CAN

	// Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32 *)pvParams;

	for(;;)
	{
		if( FRTOS1_xQueueReceive( qCan0_RxQueue, &xCanFrameToProcess, 3 ) ) // Wait for up to 3mS for data.
		{
			// Check for a manufacturing packet...
			// Must be this fixed id which is in reserved range we are using for Production Test responses via CAN
			if( ( cMFG_RESERVED_FIXED_RESPONSE_ID & cMFG_RESERVED_FIXED_RESPONSE_ID_MASK )
					== ( xCanFrameToProcess.xLddCanRxFrame.MessageID & cMFG_RESERVED_FIXED_RESPONSE_ID_MASK ) )
			{

#ifdef DEBUG_CAN_RX
				vAppDisplayRawCanPacket( &xCanFrameToProcess, FALSE );
#endif // DEBUG_CAN_RX

				// Pass the response payload back to the serial host
				//vHostQueueCanResponse( &xCanFrameToProcess.u8Data[0] );

			}
			else
			{
				// This must be an IDS-CAN packet...

#ifdef INCLUDE_IDS_CORE_CAN
#ifdef DEBUG_CAN_RX
				vAppDisplayRawCanPacket( &xCanFrameToProcess, FALSE );
#endif // DEBUG_CAN_RX

				// Convert to the IDS legacy packet format
				xLegacyPacket.ID = xCanFrameToProcess.xLddCanRxFrame.MessageID;
				memcpy( &xLegacyPacket.Data[0], &xCanFrameToProcess.u8Data[0], xCanFrameToProcess.xLddCanRxFrame.Length );
				xLegacyPacket.Length = xCanFrameToProcess.xLddCanRxFrame.Length;
				// Pass up to the IDS-CAN layer
				if( TRUE == boAppIDSCoreAcquireMux( 5 ) )   // Wait 5 ms max
				{
					IDS_CAN_OnCanMessageRx( &xLegacyPacket );
					vAppIDSCoreReleaseMux();    // release the mux
				}
#endif // INCLUDE_IDS_CORE_CAN

			}

		}

		// Kick the dog
		FRTOS1_taskENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		FRTOS1_taskEXIT_CRITICAL();

	}

	// Task function does not return
}

static void vAppTaskCan0_Tx( void *pvParams )
{
	BaseType_t xReturn;
	volatile uint32 *pu32TaskIsRunning;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCanFrameToProcess;
	LDD_TError u16Status;
	UBaseType_t NbItemsInQueue;

	// Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32 *)pvParams;

	for(;;)
	{
		if( FRTOS1_xQueuePeek( qCan0_TxQueue, &xCanFrameToProcess, cCAN0_WAIT_UP_TO_100_MS ) ) // Receive an item from a queue without the item being removed.
		{
			xCanFrameToProcess.xLddCanRxFrame.Data = &xCanFrameToProcess.u8Data[0]; // Put the address of the data buffer into the low-level frame
			u16Status = CAN0_SendFrame( gpCAN0_LowLevelDeviceState, cCAN0_TRANSMIT_BUFFER, &xCanFrameToProcess.xLddCanRxFrame );

			if( ERR_OK == u16Status )
			{
				// IDS CAN TX callback to execute?
				if( NULL != xCanFrameToProcess.pFnCallback )
				{
					xCAN_TX_MESSAGE_TYPE xIdsCanPacket; // Create an OldLowLevelCAN Packet

					xIdsCanPacket.ID = xCanFrameToProcess.xLddCanRxFrame.MessageID;
					xIdsCanPacket.Length = xCanFrameToProcess.xLddCanRxFrame.Length;
					memcpy( &xIdsCanPacket.Data[0], &xCanFrameToProcess.u8Data[0], xCanFrameToProcess.xLddCanRxFrame.Length );
					xCanFrameToProcess.pFnCallback( &xIdsCanPacket );
				}
				NbItemsInQueue = FRTOS1_uxQueueMessagesWaiting(qCan0_TxQueue); // number of items that are currently in a queue.
				if (NbItemsInQueue != 0)
				{
					xReturn = FRTOS1_xQueueReceive( qCan0_TxQueue, &xCanFrameToProcess, 0 ); // The item is read and then removed from the queue.
					ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
				}
			}
			else
			{
#ifdef DEBUG_WITH_ASSERT
				vAppCan0TxError(u16Status); // a CAN Send Error occurred.
#endif
			}
		}
		// Kick the dog
		FRTOS1_taskENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		FRTOS1_taskEXIT_CRITICAL();
	}
	// Task function does not return
}

static void vAppCan0TxError( LDD_TError u16CanError )
{
	switch( u16CanError )
	{
	// case ERR_OK : break;
	case ERR_DISABLED :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_SPEED :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_PARAM_RANGE :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_PARAM_INDEX :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_PARAM_LENGTH :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_PARAM_ATTRIBUTE_SET :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_PARAM_VALUE :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;
	case ERR_BUSY :
		ASSERT(u16CanError == ERR_OK); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;

	default :
		break;

	}

	return;
}


/**---------------------------------------------------------------------------------------------------------------------
 * @brief           Private (module-scope) function definitions
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifdef DEBUG_CAN_TX
static void vAppDisplayCanTxError( uint8 u8CanError )
{
	uint16 u16Discard;

	switch( u8CanError )
	{
	case ERR_OK :
		sprintf( &gstrOutData[0], "\nERR_OK\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 8, &u16Discard );
		break;
	case ERR_DISABLED :
		sprintf( &gstrOutData[0], "\nERR_DISABLED\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 14, &u16Discard );
		break;
	case ERR_SPEED :
		sprintf( &gstrOutData[0], "\nERR_SPEED\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 11, &u16Discard );
		break;
	case ERR_PARAM_RANGE :
		sprintf( &gstrOutData[0], "\nERR_PARAM_RANGE\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 17, &u16Discard );
		break;
	case ERR_PARAM_INDEX :
		sprintf( &gstrOutData[0], "\nERR_PARAM_INDEX\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 17, &u16Discard );
		break;
	case ERR_PARAM_LENGTH :
		sprintf( &gstrOutData[0], "\nERR_PARAM_LENGTH\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 18, &u16Discard );
		break;
	case ERR_PARAM_ATTRIBUTE_SET :
		sprintf( &gstrOutData[0], "\nERR_PARAM_ATTRIBUTE_SET\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 25, &u16Discard );
		break;
	case ERR_PARAM_VALUE :
		sprintf( &gstrOutData[0], "\nERR_PARAM_VALUE\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 17, &u16Discard );
		break;
	case ERR_BUSY :
		sprintf( &gstrOutData[0], "\nERR_BUSY\n" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 10, &u16Discard );
		break;

	default :
		break;

	}

	return;
}
#endif // DEBUG_CAN_TX




#ifdef DEBUG_CAN_TX
static void vAppDisplayTimeoutError(void)
{
	uint16 u16Discard;
	sprintf( &gstrOutData[0], "\nTx 3 Sec Timeout\n" );
	DEBUG_SERIAL_SendBlock( &gstrOutData[0], 18, &u16Discard );

	return;
}
#endif // DEBUG_CAN_TX




#if defined( DEBUG_CAN_RX ) || defined( DEBUG_CAN_TX )
static void vAppDisplayRawCanPacket( xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *pxCanPacket, bool boTx )
{
	uint16 u16Discard;
	uint8 i;

	if( boTx )
	{
		sprintf( &gstrOutData[0], "\nTx:" );
	}
	else
	{
		sprintf( &gstrOutData[0], "\nRx:" );
	}
	DEBUG_SERIAL_SendBlock( &gstrOutData[0], 4, &u16Discard );

	if( 0 != ( LDD_CAN_MESSAGE_ID_EXT & pxCanPacket->xLddCanRxFrame.MessageID ) )
	{
		// This is an extended packet
		sprintf( &gstrOutData[0], "\nExt:" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 5, &u16Discard );
		sprintf( &gstrOutData[0], "\nID = 0x%08x\n", ( 0x1FFFFFFF & pxCanPacket->xLddCanRxFrame.MessageID ) );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 17, &u16Discard );
	}
	else
	{
		sprintf( &gstrOutData[0], "\nStd:" );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 5, &u16Discard );
		sprintf( &gstrOutData[0], "\nID = 0x%03x\n", ( 0x7FF & pxCanPacket->xLddCanRxFrame.MessageID ) );
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], 12, &u16Discard );
	}

	sprintf( &gstrOutData[0], "FrameType: %d\n", pxCanPacket->xLddCanRxFrame.FrameType );
	DEBUG_SERIAL_SendBlock( &gstrOutData[0], 13, &u16Discard );

	sprintf( &gstrOutData[0], "Length: 0x%02x\nData: ", pxCanPacket->xLddCanRxFrame.Length );
	DEBUG_SERIAL_SendBlock( &gstrOutData[0], 19, &u16Discard );

	if( pxCanPacket->xLddCanRxFrame.Length <= 8 )
	{
		for( i = 0; i < pxCanPacket->xLddCanRxFrame.Length; i++ )
		{
			sprintf( &gstrOutData[i * 5], "0x%02x ", pxCanPacket->u8Data[i] );
		}
		DEBUG_SERIAL_SendBlock( &gstrOutData[0], ( i * 5 ), &u16Discard );  // Note. 96 byte serial output buffer.
	}
	DEBUG_SERIAL_SendChar( '\n' );
}
#endif // DEBUG_CAN_RX || DEBUG_CAN_TX
