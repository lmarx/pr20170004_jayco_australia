/**---------------------------------------------------------------------------------------------------------------------
 * @file            AppCANModule.h
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 7, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */
#ifndef SOURCES_APPCA1NMODULE_H_
#define SOURCES_APPCAN1MODULE_H_

typedef struct xAppCan1_Data
{
    uint8 u8All_output_off;
}xAPPCAN1_DATA;

extern QueueHandle_t qAppCan1_data_Queue;

void vAppCAN1TaskInit(void);
bool boCAN1_RxWatchdogTaskCheck(void);
bool boCAN1_TxWatchdogTaskCheck(void);
bool boCAN1_100MsWatchdogTaskCheck(void);
void vCAN1_RxWatchdogTaskClear(void);
void vCAN1_TxWatchdogTaskClear(void);
void vCAN1_100MsWatchdogTaskClear(void);

void vISR_AppCAN1_ModuleOnRxEvent( LDD_CAN_TMBIndex BufferIdx );

#endif // SOURCES_APPCAN1MODULE_H_
