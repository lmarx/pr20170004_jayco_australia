/**---------------------------------------------------------------------------------------------------------------------
 * @file            TesterCANModule.c
 *
 * @brief           <insert short description here>
 * 
 * @date            Feb 7, 2017
 *
 * @author          Mike Stroven
 * ---------------------------------------------------------------------------------------------------------------------
 */

#include "stdlib.h"  /* abs() */
#include "Application.h"
#include "IDS-CORE\global.h"

/* message received from J35 until communication is established */
#define CAN1_J35_RX_STARTCOM_LEN	8U

/* message to send: from Gateway to J35 - start communication */
/*12345678901234567890123456789012*/
#define CAN1_J35_TX_START_ID		0x03956706U
#define CAN1_J35_TX_START_LEN		8U

#define CAN1_J35_TX_ENA_DIS_OUT_ID	0x1E005069U
#define CAN1_J35_TX_ENA_DIS_OUT_LEN	2U

/* set capacity battery */
#define CAN1_J35_TX_BATCAP_ID		0x1E00506AU
#define CAN1_J35_TX_BATCAP_ID_LEN	4U

/* set OUTPUT ON/OFF */
#define CAN1_J35_TX_CMD_SLIDE_OUT		0x1E005040U
#define CAN1_J35_TX_CMD_SLIDE_OUT_LEN	1U
#define CAN1_J35_TX_CMD_SPARE_15A		0x1E005041U
#define CAN1_J35_TX_CMD_SPARE_15A_LEN	1U
#define CAN1_J35_TX_CMD_WATER_PUMP		0x1E005042U
#define CAN1_J35_TX_CMD_WATER_PUMP_LEN	1U
#define CAN1_J35_TX_CMD_HOT_HEATER		0x1E005043U
#define CAN1_J35_TX_CMD_HOT_HEATER_LEN	1U
#define CAN1_J35_TX_CMD_LIGHT_1			0x1E005044U
#define CAN1_J35_TX_CMD_LIGHT_1_LEN		1U
#define CAN1_J35_TX_CMD_LIGHT_2			0x1E005045U
#define CAN1_J35_TX_CMD_LIGHT_2_LEN		1U
#define CAN1_J35_TX_CMD_LIGHT_3			0x1E005046U
#define CAN1_J35_TX_CMD_LIGHT_3_LEN		1U
#define CAN1_J35_TX_CMD_12V_OUTLET1		0x1E005047U
#define CAN1_J35_TX_CMD_12V_OUTLET1_LEN	1U
#define CAN1_J35_TX_CMD_12V_OUTLET2		0x1E005048U
#define CAN1_J35_TX_CMD_12V_OUTLET2_LEN	1U
#define CAN1_J35_TX_CMD_SPARE10_10A		0x1E005049U
#define CAN1_J35_TX_CMD_SPARE10_10A_LEN	1U
#define CAN1_J35_TX_CMD_SPARE11_10A		0x1E00504AU
#define CAN1_J35_TX_CMD_SPARE11_10A_LEN	1U
#define CAN1_J35_TX_CMD_SPARE12_10A		0x1E00504BU
#define CAN1_J35_TX_CMD_SPARE12_10A_LEN	1U
#define CAN1_J35_TX_CMD_SPARE13_10A		0x1E00504CU
#define CAN1_J35_TX_CMD_SPARE13_10A_LEN	1U
#define CAN1_J35_TX_CMD_TABLET			0x1E00504DU
#define CAN1_J35_TX_CMD_TABLET_LEN		1U

#define CAN1_J35_TX_BATCAP_LIM_MAX    	600000U   // 600 AH
#define CAN1_J35_TX_BATCAP_LIM_MIN     	80000U   // 50 AH
#define CAN1_J35_TX_BATCAP_RET_VAL    	0xFFFFFFFFU   // return value from J35 when cmd setting is set

#define J35_RX_NB_DATA_TRANS_STATE	5U

#define J35_RX_TRANS_STATE_OUT1  	0x16005040U	// J35 - Output slide out Current
#define J35_RX_TRANS_STATE_OUT2  	0x16005041U	// J35 - Output Spare 15 A Current
#define J35_RX_TRANS_STATE_OUT3  	0x16005042U	// J35 - Output Water Pump Current
#define J35_RX_TRANS_STATE_OUT4  	0x16005043U	// J35 - Output Hot water Current
#define J35_RX_TRANS_STATE_OUT5  	0x16005044U	// J35 - Output Lights 1 Current
#define J35_RX_TRANS_STATE_OUT6  	0x16005045U	// J35 - Output Lights 2 Current
#define J35_RX_TRANS_STATE_OUT7  	0x16005046U	// J35 - Output Lights 3 Current
#define J35_RX_TRANS_STATE_OUT8  	0x16005047U	// J35 - Output 12 V outlet1 Current
#define J35_RX_TRANS_STATE_OUT9  	0x16005048U	// J35 - Output 12 V outlet2 Current
#define J35_RX_TRANS_STATE_OUT10 	0x16005049U	// J35 - Output Spare10 10A Current
#define J35_RX_TRANS_STATE_OUT11 	0x1600504AU	// J35 - Output Spare11 10A Current
#define J35_RX_TRANS_STATE_OUT12 	0x1600504BU	// J35 - Output Spare12 10A Current
#define J35_RX_TRANS_STATE_OUT13 	0x1600504CU	// J35 - Output Spare13 10A Current
#define J35_RX_TRANS_STATE_OUT14 	0x1600504DU	// J35 - Output Tablet Current
#define J35_RX_TRANS_STATE_ENA_DIS_OUT 	0x16005069U	// J35 - All outputs are enabled or disabled
#define J35_RX_TRANS_STATE_BAT_SET 	0x1600506AU	// J35 - Setting battery capacity(Ah)

#define J35_RX_NB_DATA_GET_STATE		5U

#define J35_RX_GET_STATE_OUT1  			0x18005040U	// J35 - Output slide out Current
#define J35_RX_GET_STATE_OUT2  			0x18005041U	// J35 - Output Spare 15 A Current
#define J35_RX_GET_STATE_OUT3  			0x18005042U	// J35 - Output Water Pump Current
#define J35_RX_GET_STATE_OUT4  			0x18005043U	// J35 - Output Hot water Current
#define J35_RX_GET_STATE_OUT5  			0x18005044U	// J35 - Output Lights 1 Current
#define J35_RX_GET_STATE_OUT6  			0x18005045U	// J35 - Output Lights 2 Current
#define J35_RX_GET_STATE_OUT7  			0x18005046U	// J35 - Output Lights 3 Current
#define J35_RX_GET_STATE_OUT8  			0x18005047U	// J35 - Output 12 V outlet1 Current
#define J35_RX_GET_STATE_OUT9  			0x18005048U	// J35 - Output 12 V outlet2 Current
#define J35_RX_GET_STATE_OUT10 			0x18005049U	// J35 - Output Spare10 10A Current
#define J35_RX_GET_STATE_OUT11 			0x1800504AU	// J35 - Output Spare11 10A Current
#define J35_RX_GET_STATE_OUT12 			0x1800504BU	// J35 - Output Spare12 10A Current
#define J35_RX_GET_STATE_OUT13 			0x1800504CU	// J35 - Output Spare13 10A Current
#define J35_RX_GET_STATE_OUT14 			0x1800504DU	// J35 - Output Tablet Current

//	    12345678901234567890123456789012 - 32 chars
#define J35_RX_GET_TBD0					0x18005060U	// TBD
#define J35_RX_NB_DATA_TBD0				4U
#define J35_RX_GET_BAT_CUR				0x18005061U	// BATTERY CURRENT
#define J35_RX_NB_DATA_BAT_CUR			4U
#define J35_RX_GET_BAT_VOLT				0x18005062U	// Main BATTERY Voltage
#define J35_RX_NB_DATA_BAT_VOLT			4U
#define J35_RX_GET_BAT_AUX_CUR			0x18005063U	// Aux (Veh BATTERY) Input CURRENT
#define J35_RX_NB_DATA_BAT_AUX_CUR		4U
#define J35_RX_GET_SOLAR_CUR			0x18005064U	// Solar input CURRENT - if value above 0.5Amps
#define J35_RX_NB_DATA_SOLAR_CUR		4U
#define J35_RX_GET_SOLAR_VOLT			0x18005065U	// Solar input VOLTAGE
#define J35_RX_NB_DATA_SOLAR_VOLT		4U
#define J35_RX_GET_PERCENT				0x18005066U	// TBD
#define J35_RX_NB_DATA_PERCENT			4U
#define J35_RX_GET_BAT_TIME_REMAIN		0x18005067U	// BATTERY Time Remaining
#define J35_RX_NB_DATA_BATT_TIME_REM	4U
#define J35_RX_GET_TBD3					0x18005068U	// TBD
#define J35_RX_NB_DATA_TBD3				4U
#define J35_RX_GET_TBD4					0x18005069U	// TBD
#define J35_RX_NB_DATA_TBD4				2U
#define J35_RX_GET_BAT_CAPACITY			0x1800506AU	// Setting BATTERY capacity(Ah)
#define J35_RX_NB_DATA_BATT_CAP			4U
#define J35_RX_GET_AUX_BAT_VOLTAGE		0x1800506BU	// Aux (Veh BATTERY) Voltage
#define J35_RX_NB_DATA_BAT_VOLTAGE		4U
#define J35_RX_GET_TOTAL_OUTPUT_CUR		0x1800506CU	// Total Output CURRENT (Loads)
#define J35_RX_NB_DATA_TOT_OUT_CUR		4U
#define J35_RX_GET_MAIN_AC_CURRENT		0x1800506DU	// MAIN AC CURRENT
#define J35_RX_NB_DATA_MAIN_AC_CURRENT		4U

static const uint8 cCAN1_TRANSMIT_BUFFER = 8;   // The number of the CAN message buffer used for transmitting

/* Transmit to J35 */
typedef struct {
	uint32 	u32Id;
	uint8 	u8Lenght;
	uint32  u32LinkId;		 	/* if not 0, link with other signal received */
	uint8 	u8LenghtLinkId;
} STRUCT_J35_TX_MSG_RCV_NEED_ANS;

#define J35_TX_MSG_RCV_NEED_ANS_NB_OF_MSG	1

#define J35_NB_OF_FIRST_MSG	3

/* First message from J35 */
typedef struct {
	uint32 	u32Id;
	//	uint8 	u8Lenght;
} STRUCT_J35_RX_FIRST_MSG;

/* Transmit to J35 */
typedef struct {
	uint32 u32Id;
	uint8 u8Lenght;
	uint8 u8OnDemand;		/* 0 - no request, others send a number of time link with the recurrence time */
} STRUCT_J35_TX;

/* message to send if requested */
const STRUCT_J35_TX Can1_TX_MsgSendIfReq[] = {
		{CAN1_J35_TX_ENA_DIS_OUT_ID,	CAN1_J35_TX_ENA_DIS_OUT_LEN,		 	1 },
		{CAN1_J35_TX_BATCAP_ID,			CAN1_J35_TX_BATCAP_ID_LEN, 				1 },
		{CAN1_J35_TX_CMD_SLIDE_OUT, 	CAN1_J35_TX_CMD_SLIDE_OUT_LEN,		 	1 },
		{CAN1_J35_TX_CMD_SPARE_15A, 	CAN1_J35_TX_CMD_SPARE_15A_LEN,		 	1 },
		{CAN1_J35_TX_CMD_WATER_PUMP, 	CAN1_J35_TX_CMD_WATER_PUMP_LEN,		 	1 },
		{CAN1_J35_TX_CMD_HOT_HEATER, 	CAN1_J35_TX_CMD_HOT_HEATER_LEN, 	 	1 },
		{CAN1_J35_TX_CMD_LIGHT_1, 		CAN1_J35_TX_CMD_LIGHT_1_LEN,    	 	1 },
		{CAN1_J35_TX_CMD_LIGHT_2, 		CAN1_J35_TX_CMD_LIGHT_2_LEN,		 	1 },
		{CAN1_J35_TX_CMD_LIGHT_3, 		CAN1_J35_TX_CMD_LIGHT_3_LEN,		 	1 },
		{CAN1_J35_TX_CMD_12V_OUTLET1, 	CAN1_J35_TX_CMD_12V_OUTLET1_LEN,	 	1 },
		{CAN1_J35_TX_CMD_12V_OUTLET2, 	CAN1_J35_TX_CMD_12V_OUTLET2_LEN,	 	1 },
		{CAN1_J35_TX_CMD_SPARE10_10A, 	CAN1_J35_TX_CMD_SPARE10_10A_LEN,	 	1 },
		{CAN1_J35_TX_CMD_SPARE11_10A, 	CAN1_J35_TX_CMD_SPARE11_10A_LEN,	 	1 },
		{CAN1_J35_TX_CMD_SPARE12_10A, 	CAN1_J35_TX_CMD_SPARE12_10A_LEN,	 	1 },
		{CAN1_J35_TX_CMD_SPARE13_10A, 	CAN1_J35_TX_CMD_SPARE13_10A_LEN,	 	1 },
		{CAN1_J35_TX_CMD_TABLET, 		CAN1_J35_TX_CMD_TABLET_LEN, 		 	1 },
};
#define CAN1_J35_TX_MSG_NB_OF_MSG	(uint8)(sizeof(Can1_TX_MsgSendIfReq)/sizeof(STRUCT_J35_TX))

typedef struct {
	uint32 id;
	uint8 lenght;
} STRUCT_CAN1_MSG_RX;

const STRUCT_CAN1_MSG_RX Can1_RX_IdExtAndlenght[] = {

		{J35_RX_TRANS_STATE_OUT1,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT2,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT3,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT4,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT5,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT6,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT7,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT8,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT9,  		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT10, 		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT11, 		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT12, 		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT13, 		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_OUT14, 		J35_RX_NB_DATA_TRANS_STATE},
		{J35_RX_TRANS_STATE_ENA_DIS_OUT,	0x02},
		{J35_RX_TRANS_STATE_BAT_SET,		0x04},

		{J35_RX_GET_STATE_OUT1,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT2,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT3,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT4,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT5,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT6,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT7,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT8,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT9,   			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT10,  			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT11,  			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT12,  			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT13,  			J35_RX_NB_DATA_GET_STATE},
		{J35_RX_GET_STATE_OUT14,  			J35_RX_NB_DATA_GET_STATE},

		{J35_RX_GET_TBD0,					J35_RX_NB_DATA_TBD0},
		{J35_RX_GET_BAT_CUR,				J35_RX_NB_DATA_BAT_CUR	},
		{J35_RX_GET_BAT_VOLT,				J35_RX_NB_DATA_BAT_VOLT	},
		{J35_RX_GET_BAT_AUX_CUR,			J35_RX_NB_DATA_BAT_AUX_CUR	},
		{J35_RX_GET_SOLAR_CUR,				J35_RX_NB_DATA_SOLAR_CUR},
		{J35_RX_GET_SOLAR_VOLT,				J35_RX_NB_DATA_SOLAR_VOLT},
		{J35_RX_GET_PERCENT,				J35_RX_NB_DATA_PERCENT},
		{J35_RX_GET_BAT_TIME_REMAIN,		J35_RX_NB_DATA_BATT_TIME_REM},
		{J35_RX_GET_TBD3,					J35_RX_NB_DATA_TBD3	},
		{J35_RX_GET_TBD4,					J35_RX_NB_DATA_TBD4	},
		{J35_RX_GET_BAT_CAPACITY,			J35_RX_NB_DATA_BATT_CAP	},
		{J35_RX_GET_AUX_BAT_VOLTAGE,		J35_RX_NB_DATA_BAT_VOLTAGE},
		{J35_RX_GET_TOTAL_OUTPUT_CUR,		J35_RX_NB_DATA_TOT_OUT_CUR},
		{J35_RX_GET_MAIN_AC_CURRENT,		J35_RX_NB_DATA_MAIN_AC_CURRENT},

};

#define CAN1_J35_RX_NB_OF_MSG	(uint8)(sizeof(Can1_RX_IdExtAndlenght)/sizeof(STRUCT_CAN1_MSG_RX))
#define J35_NB_OUTPUTS	14

typedef struct {
	uint8 u8state;	/* ON or OFF state */
	uint32 u32current;  /* value is in milli Amps */
} STRUCT_J35_RX_RAW_INFO_0;

#if defined(USE_DBG_GLOBAL_VAR)
static uint16 counter_unknown_msg_id = 0;
static uint16 counter_unknown_msg_id_ext = 0;

#define J35_ID_EXT_UNKNOWN_MAX_NB	10
#define J35_ID_UNKNOWN_MAX_NB		10

typedef struct {
	uint32 u32tbd0;
	int32  s32battery_current;
	uint32 u32battery_voltage;
	int32 u32aux_battery_current;
	uint32 u32solar_current;
	uint32 u32solar_voltage;
	uint32 u32percent; /* from 0 to 100 */
	uint32 u32battery_time_remain;
	uint32 u32tbd3;  /* time remaining when solar is on */
	uint16 u16tbd4;
	uint32 u32battery_capacity;
	uint32 u32aux_battery_voltage;
	uint32 u32total_out_current;
	uint32 u32main_ac_current;
} STRUCT_J35_RX_RAW_INFO_1;

typedef struct {
	uint32 id;
	uint8 lenght;
	uint8 u8data[8];
} STRUCT_J35_CARRY;

STRUCT_J35_RX_RAW_INFO_1 	J35_RXRawInfo_1_Get_dbg;							//use global variables to read variable
STRUCT_J35_RX_RAW_INFO_0	J35_RXRawInfo_0_TransState_dbg[J35_NB_OUTPUTS]; 	//use global variables to read variable
STRUCT_J35_RX_RAW_INFO_0	J35_RXRawInfo_0_GetState_dbg[J35_NB_OUTPUTS]; 		//use global variables to read variable
STRUCT_J35_CARRY 			J35_unknown_IdExt_dbg[J35_ID_EXT_UNKNOWN_MAX_NB]; 	//use global variables to read variable
STRUCT_J35_CARRY 			J35_unknown_Id_dbg[J35_ID_UNKNOWN_MAX_NB];    		//use global variables to read variable
#endif

// Input and output queues:

// Task parameters
static const uint8 cCAN1_100MS_TASK_PRIORITY = ( configMAX_PRIORITIES - 4 );
static const uint16 cCAN1_TASK_100MS_SIZE = 240;
//static const uint16 cCAN1_TASK_100_MS = ( 100 / portTICK_PERIOD_MS );

static volatile uint32 u32CAN1_100MsTaskWdgCheck = FALSE;
static TaskHandle_t xCAN1_100MsTaskHandle = NULL;

// Task parameters
static const uint8 cCAN1_RX_TASK_PRIORITY = ( configMAX_PRIORITIES - 3 );
static const uint8 cCAN1_TX_TASK_PRIORITY = ( configMAX_PRIORITIES - 3 );
static const uint16 cCAN1_RX_TASK_STACK_SIZE = 240;
static const uint16 cCAN1_TX_TASK_STACK_SIZE = 180;
static const uint16 cCAN1_WAIT_UP_TO_100_MS = ( 100 / portTICK_PERIOD_MS );

static volatile uint32 u32CAN1_RxTaskWdgCheck = FALSE;
static volatile uint32 u32CAN1_TxTaskWdgCheck = FALSE;
static TaskHandle_t xCAN1_RxTaskHandle = NULL;
static TaskHandle_t xCAN1_TxTaskHandle = NULL;

typedef struct {
	uint8 u8Init_CAN0_Once;
	uint8 u8Output_light_1;
} STRUCT_APPCAN1_GLOBAL;

static STRUCT_APPCAN1_GLOBAL AppCan1_global_data;

QueueHandle_t qAppCan1_data_Queue = NULL;
static const uint8 cAPPCAN1_DATA_QUEUE_ITEM_SIZE = sizeof(xAPPCAN1_DATA);
static const uint8 cAPPCAN1_DATA_QUEUE_LENGTH = 1;

// Input and output queues:
static QueueHandle_t qCan1_RxQueue = NULL;
static QueueHandle_t qCan1_TxQueue = NULL;

static const uint8 cCAN1_RX_QUEUE_ITEM_SIZE = sizeof(xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE);
static const uint8 cCAN1_RX_QUEUE_LENGTH = 32;

static const uint8 cCAN1_TX_QUEUE_ITEM_SIZE = sizeof(xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE);
static const uint8 cCAN1_TX_QUEUE_LENGTH = 32;

QueueHandle_t qPID_BatCap_DataQueue_R = NULL;
QueueHandle_t qPID_BatCap_DataQueue_W = NULL;
static const uint8 cPID_DataQUEUE_ITEM_SIZE = sizeof(xPID_DATA_LINK_WITH_DEVICE);
static const uint8 cPID_DataQUEUE_LENGTH = 1;

static LDD_TDeviceData *gpCAN1_LowLevelDeviceState = NULL;


static void vAppTaskCan1_Rx( void *pvParams ) __attribute__ ((noreturn));
static void vAppTaskCan1_Tx( void *pvParams ) __attribute__ ((noreturn));

static void vAppTaskCan1_100Ms( void *pvParams ) __attribute__ ((noreturn));

void TestTheNumbersOfCarateresICanHaveInAFile0123456789012345678901234567890123456789(void)
{

}

void vAppCAN1TaskInit(void)
{
	BaseType_t xReturn;

	// Create the Queue
	qCan1_RxQueue = FRTOS1_xQueueCreate( cCAN1_RX_QUEUE_LENGTH, cCAN1_RX_QUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qCan1_RxQueue, "CAN1_Rx" );

	qCan1_TxQueue = FRTOS1_xQueueCreate( cCAN1_TX_QUEUE_LENGTH, cCAN1_TX_QUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qCan1_TxQueue, "CAN1_Tx" );

	qPID_BatCap_DataQueue_R = FRTOS1_xQueueCreate( cPID_DataQUEUE_LENGTH, cPID_DataQUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qPID_BatCap_DataQueue_R, "PID_BatCap_DATA_R" );

	qPID_BatCap_DataQueue_W = FRTOS1_xQueueCreate( cPID_DataQUEUE_LENGTH, cPID_DataQUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qPID_BatCap_DataQueue_W, "PID_BatCap_DATA_W" );

	qAppCan1_data_Queue = FRTOS1_xQueueCreate( cAPPCAN1_DATA_QUEUE_LENGTH, cAPPCAN1_DATA_QUEUE_ITEM_SIZE );
	FRTOS1_vQueueAddToRegistry( qAppCan1_data_Queue, "APP_CAN1_DATA" );

	// Setup the Receive task
	xReturn = FRTOS1_xTaskCreate( vAppTaskCan1_Rx,
			"CAN1_RxProc",
			cCAN1_RX_TASK_STACK_SIZE,
			(void *)&u32CAN1_RxTaskWdgCheck,
			cCAN1_RX_TASK_PRIORITY,
			&xCAN1_RxTaskHandle );

	ASSERT(xReturn == pdTRUE); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	// Setup the Transmit task
	xReturn = FRTOS1_xTaskCreate( vAppTaskCan1_Tx,
			"CAN1_TxProc",
			cCAN1_TX_TASK_STACK_SIZE,
			(void *)&u32CAN1_TxTaskWdgCheck,
			cCAN1_TX_TASK_PRIORITY,
			&xCAN1_TxTaskHandle );

	ASSERT(xReturn == pdTRUE); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	// Initialize the low-level driver
	gpCAN1_LowLevelDeviceState = CAN1_DeviceData;


	// Setup the 100ms task
	xReturn = FRTOS1_xTaskCreate( vAppTaskCan1_100Ms,
			"CAN1_Task_100Ms",
			cCAN1_TASK_100MS_SIZE,
			(void *)&u32CAN1_100MsTaskWdgCheck,
			cCAN1_100MS_TASK_PRIORITY,
			&xCAN1_100MsTaskHandle );

	ASSERT(xReturn == pdTRUE); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

	return;
}

void vISR_AppCAN1_ModuleOnRxEvent( LDD_CAN_TMBIndex u8RxBufferIdx )
{
	BaseType_t xReturn;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCan1_RxReadFrame;
	LDD_TError u16Status;

	xCan1_RxReadFrame.xLddCanRxFrame.Data = &xCan1_RxReadFrame.u8Data[0]; // set the payload pointer to our byte array
	u16Status = CAN1_ReadFrame( gpCAN1_LowLevelDeviceState, u8RxBufferIdx, &xCan1_RxReadFrame.xLddCanRxFrame );

	if( ERR_OK == u16Status ) // Don't try to use a non-existent queue
	{
		xReturn = FRTOS1_xQueueSendToBackFromISR( qCan1_RxQueue, &xCan1_RxReadFrame, 0 ); // queue the CAN frame
		ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
	}
	else
	{
		// Some error occurred in the CAN reception
	}

	return;
}


uint8 vAppTaskCan1_Rx_MsgTransition(uint32 u32_msg_id,  const xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *xCan1_FrameToProcess)
{
	uint8 u8RxSuccess = FALSE;
	uint32 u32result;
	static STRUCT_J35_RX_RAW_INFO_0	J35_RXRawInfo_0_TransState[J35_NB_OUTPUTS]; /* J35 has 16 outputs */

	if (( u32_msg_id >=J35_RX_TRANS_STATE_OUT1) && (u32_msg_id <= J35_RX_TRANS_STATE_OUT14))
	{
		u8RxSuccess = TRUE;

		u32result = u32_msg_id - J35_RX_TRANS_STATE_OUT1;
		/* from 0 to 13 */
		if (u32result < J35_NB_OUTPUTS)
		{
			/* values are updated only once when a request is send to J35 to change value */
			J35_RXRawInfo_0_TransState[u32result].u8state = xCan1_FrameToProcess->u8Data[0];

			J35_RXRawInfo_0_TransState[u32result].u32current  = (xCan1_FrameToProcess->u8Data[1] << 24)+
					(xCan1_FrameToProcess->u8Data[2] << 16)+
					(xCan1_FrameToProcess->u8Data[3] << 8)+
					xCan1_FrameToProcess->u8Data[4];

			J35_RXRawInfo_0_TransState[u32result].u8state = J35_RXRawInfo_0_TransState[u32result].u8state; // avoid lint warning, data not accessed

#if defined(USE_DBG_GLOBAL_VAR)
			J35_RXRawInfo_0_TransState_dbg[u32result].u8state = J35_RXRawInfo_0_TransState[u32result].u8state;
			J35_RXRawInfo_0_TransState_dbg[u32result].u32current = J35_RXRawInfo_0_TransState[u32result].u32current;
			J35_RXRawInfo_0_TransState_dbg[u32result].u8state = J35_RXRawInfo_0_TransState_dbg[u32result].u8state;
#endif
		}
	}
	return u8RxSuccess;
}

uint8 vAppTaskCan1_Rx_MsgState(uint32 u32_msg_id, const xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *xCan1_FrameToProcess, STRUCT_J35_RX_RAW_INFO_0	*J35_RXRawInfo_0_GetState)
{
	uint8 u8RxSuccess = FALSE;
	uint32 u32result;


	/* look for message link with STATE */
	if (( u32_msg_id >=J35_RX_GET_STATE_OUT1) && (u32_msg_id <= J35_RX_GET_STATE_OUT14))
	{
		u8RxSuccess = TRUE;

		u32result = u32_msg_id - J35_RX_GET_STATE_OUT1;
		/* from 0 to 13 */
		if (u32result < J35_NB_OUTPUTS)
		{
			J35_RXRawInfo_0_GetState[u32result].u8state = xCan1_FrameToProcess->u8Data[0];
			/* value is in milli Amps */
			J35_RXRawInfo_0_GetState[u32result].u32current = (xCan1_FrameToProcess->u8Data[1] << 24)+
					(xCan1_FrameToProcess->u8Data[2] << 16)+
					(xCan1_FrameToProcess->u8Data[3] << 8)+
					xCan1_FrameToProcess->u8Data[4];

			if ((AppCan1_global_data.u8Init_CAN0_Once == 0) && (J35_RXRawInfo_0_GetState[13].u8state) == 0x01)
			{
#if defined(USE_DBG_PIN)
DEBUG_SPARE_GPIO1_PutVal(1);
#endif
				/* init. CAN0 driver and IDS_CAN after J35 power the output TABLET */
				AppCan1_global_data.u8Init_CAN0_Once = 1;
				(void)CAN0_Init(NULL);
				IDS_CAN_Init();
#if defined(USE_DBG_PIN)
DEBUG_SPARE_GPIO1_PutVal(0);
#endif
			}


#if defined(USE_DBG_GLOBAL_VAR)
			J35_RXRawInfo_0_GetState_dbg[u32result].u8state = J35_RXRawInfo_0_GetState[u32result].u8state;
			J35_RXRawInfo_0_GetState_dbg[u32result].u32current = J35_RXRawInfo_0_GetState[u32result].u32current;
			J35_RXRawInfo_0_GetState_dbg[u32result].u32current = J35_RXRawInfo_0_GetState_dbg[u32result].u32current;
#endif
		}
	}
	return u8RxSuccess;
}

uint8 vAppTaskCan1_Rx_Msg(uint32 u32_msg_id, const xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE *xCan1_FrameToProcess, const STRUCT_J35_RX_RAW_INFO_0 *J35_RXRawInfo_0_GetState, STRUCT_J35_POWER_MONITOR_DATA *J35_Monitor)
{
	BaseType_t xReturn;

	xPID_DATA_LINK_WITH_DEVICE xPID_DataRead;
	xAPPCAN1_DATA xAppCan1_Data;

	uint8 u8RxSuccess = FALSE;
	uint32 index_loop_1 = 0;
	uint32 u32result = 0;
	int32 s32result;
	uint32 u32Temp;
	UBaseType_t NbItemsInQueue;

	switch(u32_msg_id)
	{
	case J35_RX_TRANS_STATE_ENA_DIS_OUT:
		NbItemsInQueue = FRTOS1_uxQueueMessagesWaiting(qAppCan1_data_Queue); // number of items that are currently in a queue.
		if (NbItemsInQueue!=0)
		{
			xReturn = FRTOS1_xQueueReceive( qAppCan1_data_Queue, &xAppCan1_Data, 0 ); // The item is read and then removed from the queue.
			ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		}
		xAppCan1_Data.u8All_output_off = 0xFF; /* msg is received */
		xReturn = FRTOS1_xQueueSendToFront( qAppCan1_data_Queue, &xAppCan1_Data, 0 ); // Send data to the front (head) of a queue.
		ASSERT(xReturn == pdPASS);  // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file

		u8RxSuccess = TRUE;
		/* this msg received tell us if message was received, not if outputs device are OFF or not, use current output */
		break;

	case J35_RX_TRANS_STATE_BAT_SET:
		/* J35 transmit 0XFFFFFFFF when the message to change the battery setting is set */
		u32result = (xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

		if (u32result == CAN1_J35_TX_BATCAP_RET_VAL)
		{
			/* battery setting is set */
		}
		u8RxSuccess = TRUE;
		break;


	case J35_RX_GET_BAT_CUR:
		u8RxSuccess = TRUE;
		/*!!! Careful - value is signed !!!*/
		s32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.s32battery_current = s32result;
		J35_RXRawInfo_1_Get_dbg.s32battery_current = J35_RXRawInfo_1_Get_dbg.s32battery_current; // to avoid lint warning, use it!
#endif

		if ( s32result < 0)
		{
			J35_Monitor->dc_bat.u8Val.bits.charging = 0; /* bits 7: --- 0 = battery is discharging */
		}
		else
		{
			J35_Monitor->dc_bat.u8Val.bits.charging = 1; /* bits 7: --- 1 = battery is charging */
		}
		u32result = (uint32)abs(s32result);

		u32Temp = (uint32)(u32result / 1000);
		u32result =  (uint32)(u32result - (u32Temp * 1000U));

		J35_Monitor->dc_bat.u16Current.val.u8i = (uint8)u32Temp;
		J35_Monitor->dc_bat.u16Current.val.u8q = (uint8)((u32result * 256 ) / 1000);

		break;

	case J35_RX_GET_BAT_VOLT:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32battery_voltage = u32result;
#endif

		u32Temp = u32result / 1000;
		u32result = u32result - (u32Temp * 1000);

		J35_Monitor->dc_bat.u16Voltage.val.u8i = (uint8)u32Temp;
		J35_Monitor->dc_bat.u16Voltage.val.u8q = (uint8)((u32result * 256 ) / 1000);

		break;

	case J35_RX_GET_BAT_AUX_CUR:
		u8RxSuccess = TRUE;
		s32result =	(xCan1_FrameToProcess->u8Data[0] << 24) +
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32aux_battery_current = s32result;
#endif

		if ( s32result < 0)
		{
			J35_Monitor->dc_bat_aux.u8Val.bits.charging = 0; /* bits 7: --- 0 = battery is discharging */
		}
		else
		{
			J35_Monitor->dc_bat_aux.u8Val.bits.charging = 1; /* bits 7: --- 1 = battery is charging */
		}

		u32result = (uint32)abs(s32result);
		u32Temp = u32result / 1000;
		u32result = u32result - (u32Temp * 1000);

		J35_Monitor->dc_bat_aux.u16Current.val.u8i = (uint8)u32Temp;
		J35_Monitor->dc_bat_aux.u16Current.val.u8q = (uint8)((u32result * 256 ) / 1000);
		J35_Monitor->dc_bat_aux.u8Val.bits.capacity = 0x7F; /* invalid */
		J35_Monitor->dc_bat_aux.u16Time_discharge = 0xFFFF;
		break;

	case J35_RX_GET_SOLAR_CUR:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32solar_current = u32result;
#endif

		u32Temp = u32result / 1000;
		u32result = u32result - (u32Temp * 1000);

		J35_Monitor->dc_solar.u16Current.val.u8i = (uint8)u32Temp;
		J35_Monitor->dc_solar.u16Current.val.u8q = (uint8)((u32result * 256 ) / 1000);

		J35_Monitor->dc_solar.u8Val.all = 0xFF;
		J35_Monitor->dc_solar.u16Time_discharge = 0xFFFF;

		break;

	case J35_RX_GET_SOLAR_VOLT:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32solar_voltage = u32result;
#endif
		u32Temp = u32result / 1000;
		u32result = u32result - (u32Temp * 1000);

		J35_Monitor->dc_solar.u16Voltage.val.u8i = (uint8)u32Temp;
		J35_Monitor->dc_solar.u16Voltage.val.u8q = (uint8)((u32result * 256 ) / 1000);

		break;

	case J35_RX_GET_BAT_TIME_REMAIN:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32battery_time_remain = u32result;
#endif
		if (u32result < 0xFFFF)
		{
			J35_Monitor->dc_bat.u16Time_discharge = (uint16)u32result;
		}
		else
		{
			J35_Monitor->dc_bat.u16Time_discharge = 0xFFFF;
		}

		break;

	case J35_RX_GET_BAT_CAPACITY:
		u8RxSuccess = TRUE;
		u32result = 	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32battery_capacity = u32result;
#endif

		NbItemsInQueue = FRTOS1_uxQueueMessagesWaiting(qPID_BatCap_DataQueue_R); // number of items that are currently in a queue.
		if (NbItemsInQueue != 0)
		{
			xReturn = FRTOS1_xQueueReceive( qPID_BatCap_DataQueue_R, &xPID_DataRead, 0 ); // clear this queue entry
			ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		}

		xPID_DataRead.u32Data[(uint8)DC_POWER_MONITOR_1] = u32result;
		xReturn = FRTOS1_xQueueSendToFront( qPID_BatCap_DataQueue_R, &xPID_DataRead, 0 );
		ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
		break;

	case J35_RX_GET_AUX_BAT_VOLTAGE:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32aux_battery_voltage = u32result;
#endif
		u32Temp = u32result / 1000;
		u32result = u32result - (u32Temp * 1000);

		J35_Monitor->dc_bat_aux.u16Voltage.val.u8i = (uint8)u32Temp;
		J35_Monitor->dc_bat_aux.u16Voltage.val.u8q = (uint8)((u32result * 256 ) / 1000);
		break;

	case J35_RX_GET_TOTAL_OUTPUT_CUR:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];

#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32total_out_current = u32result;
#endif

		/* Are all outputs at 0 ? (except tablet which does not report data)*/

		J35_Monitor->manager.u8Mode = 1; /* Isolation switch is ON */
		for (index_loop_1 = 0; index_loop_1 < J35_NB_OUTPUTS; index_loop_1++)
		{
			if (J35_RXRawInfo_0_GetState[index_loop_1].u8state != 0)
			{
				J35_Monitor->manager.u8Mode = 0; /* Isolation switch is OFF */
			}
		}
		break;

	case J35_RX_GET_MAIN_AC_CURRENT:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32main_ac_current = u32result;
#endif

		u32Temp = u32result / 1000;
		u32result = u32result - (u32Temp * 1000);

		J35_Monitor->ac.u16Current.val.u8i = (uint8)u32Temp;
		J35_Monitor->ac.u16Current.val.u8q = (uint8)((u32result * 256 ) / 1000);

		/* not use for now, init. */
		J35_Monitor->ac.u16Voltage.val.u8i = (uint8)0xFF;
		J35_Monitor->ac.u16Voltage.val.u8q = (uint8)0xFF;
		J35_Monitor->ac.u8Val.bits.u4l = (uint8)0;

		break;

	case J35_RX_GET_TBD0:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32tbd0 = u32result;
#endif
		break;

	case J35_RX_GET_PERCENT:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32percent = u32result;
#endif

		J35_Monitor->dc_bat.u8Val.bits.capacity = u32result;

		break;


	case J35_RX_GET_TBD3:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u32tbd3 = u32result;
#endif
		break;

	case J35_RX_GET_TBD4:
		u8RxSuccess = TRUE;
		u32result =	(xCan1_FrameToProcess->u8Data[0] << 24)+
				(xCan1_FrameToProcess->u8Data[1] << 16)+
				(xCan1_FrameToProcess->u8Data[2] << 8)+
				xCan1_FrameToProcess->u8Data[3];
#if defined(USE_DBG_GLOBAL_VAR)
		J35_RXRawInfo_1_Get_dbg.u16tbd4 = (uint16)u32result;
#endif
		break;


	default:
		/* TBD */
		break;

	}

	return u8RxSuccess; //lint !e438 #disable one line because Last value assigned to variable 'u32result' is not used which is correct
}

static void vAppTaskCan1_Rx( void *pvParams )
{
	volatile uint32 *pu32TaskIsRunning;

	BaseType_t xReturn;

	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCan1_FrameToProcess;

	STRUCT_J35_POWER_MONITOR_DATA J35_Monitor;

	// Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32 *)pvParams;

	STRUCT_J35_RX_RAW_INFO_0	J35_RXRawInfo_0_GetState[J35_NB_OUTPUTS]; /* J35 has 16 outputs */
	uint8 u8RxSuccess;
	uint32 u32_msg_id = 0;
	uint32 index_loop_1 = 0;
	uint32 index_loop_2 = 0;

	/*  put in local after test */
	/* If message received need to send an answer */
	STRUCT_J35_TX_MSG_RCV_NEED_ANS Can1_TX_MsgRcvNeedAns[1];
	STRUCT_J35_RX_FIRST_MSG j35_rx_first_msg[J35_NB_OF_FIRST_MSG];
	uint8 j35_rx_first_msg_index;


#if 0
	index_loop_1 = 0;
	do
	{
		Can1_Msg_Rx[index_loop_1].id = Can1_RX_IdExtAndlenght[index_loop_1].id;
		Can1_Msg_Rx[index_loop_1].lenght = Can1_RX_IdExtAndlenght[index_loop_1].lenght;
		index_loop_1++;
	}while(index_loop_1 < CAN1_J35_RX_NB_OF_MSG);
#endif

#if defined(USE_DBG_PIN)
	DEBUG_SPARE_GPIO1_PutVal(1);
	DEBUG_SPARE_GPIO2_PutVal(1);
	DEBUG_SPARE_GPIO3_PutVal(1);
	DEBUG_SPARE_GPIO4_PutVal(1);

	DEBUG_SPARE_GPIO5_PutVal(1);
	DEBUG_SPARE_GPIO6_PutVal(1);
	DEBUG_SPARE_GPIO7_PutVal(1);
	DEBUG_SPARE_GPIO8_PutVal(1);


	DEBUG_SPARE_GPIO1_PutVal(0);
	DEBUG_SPARE_GPIO2_PutVal(0);
	DEBUG_SPARE_GPIO3_PutVal(0);
	DEBUG_SPARE_GPIO4_PutVal(0);

	DEBUG_SPARE_GPIO5_PutVal(0);
	DEBUG_SPARE_GPIO6_PutVal(0);
	DEBUG_SPARE_GPIO7_PutVal(0);
	DEBUG_SPARE_GPIO8_PutVal(0);
#endif

	j35_rx_first_msg_index = 0;
	Can1_TX_MsgRcvNeedAns[0].u8Lenght = CAN1_J35_RX_STARTCOM_LEN; // message with 8 bytes data
	Can1_TX_MsgRcvNeedAns[0].u32LinkId = CAN1_J35_TX_START_ID;
	Can1_TX_MsgRcvNeedAns[0].u8LenghtLinkId = CAN1_J35_TX_START_LEN;

	AppCan1_global_data.u8Init_CAN0_Once = 0;
	AppCan1_global_data.u8Output_light_1 = 0xFF;

	for(;;)
	{
		if( FRTOS1_xQueueReceive( qCan1_RxQueue, &xCan1_FrameToProcess, 10 ) ) // Wait for up to 10mS for data.
		{
			if( 0 != ( LDD_CAN_MESSAGE_ID_EXT & xCan1_FrameToProcess.xLddCanRxFrame.MessageID ) )
			{
				// This message is part of messages with extended packet
				// init message from J35 is 8 data length
				if((j35_rx_first_msg_index < J35_NB_OF_FIRST_MSG) && (xCan1_FrameToProcess.xLddCanRxFrame.Length == Can1_TX_MsgRcvNeedAns[0].u8Lenght))
				{
					/* decode first message sent by J35 ...requesting for an init. this is the only message with 8 bytes data, send every 10 sec. */
					j35_rx_first_msg[j35_rx_first_msg_index].u32Id = xCan1_FrameToProcess.xLddCanRxFrame.MessageID & ~LDD_CAN_MESSAGE_ID_EXT;

					if (j35_rx_first_msg_index > 1)
					{
						if ( j35_rx_first_msg[0].u32Id == j35_rx_first_msg[j35_rx_first_msg_index].u32Id )
						{
							j35_rx_first_msg_index++;
						}
						else
						{
							j35_rx_first_msg_index = 0;
						}
					}
					else
					{
						/* j35_rx_first_msg_index = 0 */
						j35_rx_first_msg_index++;
					}

					if(j35_rx_first_msg_index == (J35_NB_OF_FIRST_MSG-1))
					{
						Can1_TX_MsgRcvNeedAns[0].u32Id = j35_rx_first_msg[0].u32Id;
					}
				}
				else
				{
					u8RxSuccess = FALSE;
					index_loop_1 = 0;

					u32_msg_id = xCan1_FrameToProcess.xLddCanRxFrame.MessageID & ~LDD_CAN_MESSAGE_ID_EXT;
					do
					{
#if (J35_TX_MSG_RCV_NEED_ANS_NB_OF_MSG == 1)
						/* Init communication with J25 box */
						if ((u32_msg_id == Can1_TX_MsgRcvNeedAns[0].u32Id) && (xCan1_FrameToProcess.xLddCanRxFrame.Length == Can1_TX_MsgRcvNeedAns[0].u8Lenght ))
						{
							u8RxSuccess = TRUE;
							/* initialize TX */
							xCan1_FrameToProcess.xLddCanRxFrame.MessageID = Can1_TX_MsgRcvNeedAns[0].u32LinkId | LDD_CAN_MESSAGE_ID_EXT;
							xCan1_FrameToProcess.xLddCanRxFrame.Length = Can1_TX_MsgRcvNeedAns[0].u8LenghtLinkId;
							xCan1_FrameToProcess.xLddCanRxFrame.FrameType = LDD_CAN_DATA_FRAME;
							xCan1_FrameToProcess.xLddCanRxFrame.LocPriority = 0;

							for (index_loop_2=0; index_loop_2 < xCan1_FrameToProcess.xLddCanRxFrame.Length; index_loop_2++)
							{
								if (index_loop_2 == 0x03 || index_loop_2==0x05)
								{
									xCan1_FrameToProcess.u8Data[index_loop_2] &= ~0x10;
									xCan1_FrameToProcess.u8Data[index_loop_2] |= 0x20;
								}
							}
							if( NULL != qCan1_TxQueue ) // Don't try to use a non-existent queue
							{
								xReturn = FRTOS1_xQueueSendToBackFromISR( qCan1_TxQueue, &xCan1_FrameToProcess, 0 ); // queue the CAN frame
								ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
							}
						}
#else
						// TBD, in case we have more than one frame to send link with a message received
#endif

						/* check if the message is part of the message expected */
						if ( (u8RxSuccess != TRUE) && (u32_msg_id == Can1_RX_IdExtAndlenght[index_loop_1].id ))
						{
							if ( xCan1_FrameToProcess.xLddCanRxFrame.Length == Can1_RX_IdExtAndlenght[index_loop_1].lenght )
							{
								/* look for message link with Output Transition */
								u8RxSuccess = vAppTaskCan1_Rx_MsgTransition(u32_msg_id, &xCan1_FrameToProcess);

								/* look for message link with Output State */
								if (u8RxSuccess != TRUE)
								{
									u8RxSuccess = vAppTaskCan1_Rx_MsgState(u32_msg_id, &xCan1_FrameToProcess, &J35_RXRawInfo_0_GetState[0]);
								}

								if (u8RxSuccess != TRUE)
								{
									u8RxSuccess = vAppTaskCan1_Rx_Msg(u32_msg_id, &xCan1_FrameToProcess, &J35_RXRawInfo_0_GetState[0], &J35_Monitor); //lint !e645 #disable one line because J35_RXRawInfo_0_GetState is initialized above
								}
							}
						}
						index_loop_1++;

					} while( (u8RxSuccess == FALSE) && (index_loop_2 < CAN1_J35_RX_NB_OF_MSG) );

					if (u8RxSuccess == TRUE)
					{
						if( qJ35Monitor_Queue != NULL )   // Don't try to use a non-existent queue
						{
							xReturn = FRTOS1_xQueueSendToBack( qJ35Monitor_Queue, &J35_Monitor, 0 ); //lint !e645 #disable one line because J35_Monitor is initialized above
							ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
						}
						else { /* Queue is not there*/ }
					}

#if defined(USE_DBG_GLOBAL_VAR)
					/* capture extended packet message not handle...new message ??? */
					if (u8RxSuccess == FALSE)
					{
						index_loop_1 = 0;
						do
						{
							if (u32_msg_id == J35_unknown_IdExt_dbg[index_loop_1].id)
							{
								index_loop_1 = J35_ID_EXT_UNKNOWN_MAX_NB + 1;
							}
							index_loop_1++;
						}
						while(index_loop_1 < J35_ID_EXT_UNKNOWN_MAX_NB);

						if (index_loop_1 <= J35_ID_EXT_UNKNOWN_MAX_NB)
						{
							/* message is unknown */
							J35_unknown_IdExt_dbg[counter_unknown_msg_id_ext].id = u32_msg_id;
							J35_unknown_IdExt_dbg[counter_unknown_msg_id_ext].lenght = xCan1_FrameToProcess.xLddCanRxFrame.Length;
							for(index_loop_1 = 0; index_loop_1 < xCan1_FrameToProcess.xLddCanRxFrame.Length; index_loop_1++)
							{
								J35_unknown_IdExt_dbg[counter_unknown_msg_id_ext].u8data[index_loop_1]=xCan1_FrameToProcess.u8Data[index_loop_1];
							}

							if (counter_unknown_msg_id_ext < J35_ID_EXT_UNKNOWN_MAX_NB)
							{
								counter_unknown_msg_id_ext++;
							}
						}
					}
#endif
				}
			}
			else
			{
				// This is an standard packet
				/* message is unknown */
#if defined(USE_DBG_GLOBAL_VAR)
				/* capture message not handle...new message ??? */
				J35_unknown_Id_dbg[counter_unknown_msg_id].id = u32_msg_id;
				J35_unknown_Id_dbg[counter_unknown_msg_id].lenght = xCan1_FrameToProcess.xLddCanRxFrame.Length;
				J35_unknown_Id_dbg[counter_unknown_msg_id].id = J35_unknown_Id_dbg[counter_unknown_msg_id].id;
				for(index_loop_1 = 0; index_loop_1 < xCan1_FrameToProcess.xLddCanRxFrame.Length; index_loop_1++)
				{
					J35_unknown_Id_dbg[counter_unknown_msg_id].u8data[index_loop_1]=xCan1_FrameToProcess.u8Data[index_loop_1];
				}
				if (counter_unknown_msg_id < J35_ID_UNKNOWN_MAX_NB)
				{
					counter_unknown_msg_id++;
				}
#endif
			}
		} /* end FRTOS1_xQueueReceive */

		// Kick the dog
		FRTOS1_taskENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		FRTOS1_taskEXIT_CRITICAL();
	} /* end of for(;;) */
	// Task function does not return
}

#if 0
/* Transmit to J35 */
typedef struct {
	uint32 u32Id;
	uint16 u16Reccurency; 	/* 0 means no recurrence,  others is recurrence time in ms*/
	uint8 u8OnDemand;		/* 0 - no request, others send a number of time link with the recurrence time */
} STRUCT_J35_TX_TOSEND;
#endif

static void vAppTaskCan1_Tx( void *pvParams ) /* call by O.S every 10MS */
{
	BaseType_t xReturn;
	//portTickType xLastWakeTime;

	volatile uint32 *pu32TaskIsRunning;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCan1_FrameToProcess;

	//uint16 u16Reccurency_dbg[CAN1_J35_TX_MSG_NB_OF_MSG];
	uint16 counter_send_TX_with_no_success = 0;

	// Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32 *)pvParams;

	LDD_TError u16TxSuccess;
	UBaseType_t NbItemsInQueue;


#if 0 // just for debug
	for(index_loop_1 = 0; index_loop_1 < CAN1_J35_TX_MSG_NB_OF_MSG; index_loop_1++)
	{
		u16Reccurency_dbg[index_loop_1] = Can1_TX_MsgSendIfReq[index_loop_1].u16Reccurency;
	}
#endif

	//xLastWakeTime = xTaskGetTickCount();

	for(;;)
	{


#if 0
		for(index = 0; index < CAN1_J35_TX_MSG_NB_OF_MSG; index++)
		{

			if (Can1_TX_MsgSendIfReq[index].u8OnDemand != 0)
			{

			}

			if (Can1_TX_MsgSendIfReq[index].u16Reccurency != 0)
			{

			}
		}
#endif

		if( FRTOS1_xQueuePeek( qCan1_TxQueue, &xCan1_FrameToProcess, cCAN1_WAIT_UP_TO_100_MS ) ) // Receive an item from a queue without the item being removed.
		{
			/* Do we have to send a message */
#if defined(USE_DBG_PIN)
			DEBUG_SPARE_GPIO5_PutVal(1);
#endif
			xCan1_FrameToProcess.xLddCanRxFrame.Data = &xCan1_FrameToProcess.u8Data[0]; // Put the address of the data buffer into the low-level frame
			u16TxSuccess = CAN1_SendFrame( gpCAN1_LowLevelDeviceState, cCAN1_TRANSMIT_BUFFER, &xCan1_FrameToProcess.xLddCanRxFrame );

			if (u16TxSuccess != ERR_OK) {
				/* msg not send yet */

				switch(u16TxSuccess)
				{
				case ERR_SPEED:
					__asm("nop");
					break;

				case ERR_PARAM_RANGE:
					__asm("nop");
					break;

				case ERR_PARAM_INDEX:
					__asm("nop");
					break;

				case ERR_PARAM_LENGTH:
					__asm("nop");
					break;

				case ERR_PARAM_ATTRIBUTE_SET:
					__asm("nop");
					break;

				case ERR_PARAM_VALUE:
					__asm("nop");
					break;

				case ERR_BUSY:
					__asm("nop");
					break;

				default:
					break;


				}

				counter_send_TX_with_no_success++;
				if (counter_send_TX_with_no_success > 20)
				{
					counter_send_TX_with_no_success = 0;
					NbItemsInQueue = FRTOS1_uxQueueMessagesWaiting(qCan1_TxQueue); // number of items that are currently in a queue.
					if (NbItemsInQueue != 0)
					{
						xReturn = FRTOS1_xQueueReceive( qCan1_TxQueue, &xCan1_FrameToProcess, 0 ); // Don't wait for Tx data.  Just clear this queue entry
						ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
					}
				}
#if defined(USE_DBG_PIN)
				DEBUG_SPARE_GPIO7_PutVal(1);
				DEBUG_SPARE_GPIO7_PutVal(0);
#endif
			}
			else
			{
				counter_send_TX_with_no_success = 0;
				NbItemsInQueue = FRTOS1_uxQueueMessagesWaiting(qCan1_TxQueue); // number of items that are currently in a queue.
				if (NbItemsInQueue != 0)
				{
					xReturn = FRTOS1_xQueueReceive( qCan1_TxQueue, &xCan1_FrameToProcess, 0 ); // Don't wait for Tx data.  Just clear this queue entry
					ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
				}
			}
#if defined(USE_DBG_PIN)
			DEBUG_SPARE_GPIO5_PutVal(0);
#endif
		}

		//vTaskDelayUntil( &xLastWakeTime, ( 10 / portTICK_RATE_MS ) );

		// Kick the dog
		FRTOS1_taskENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		FRTOS1_taskEXIT_CRITICAL();
	}

	// Task function does not return
}

static void vAppTaskCan1_100Ms( void *pvParams )
{
	BaseType_t xReturn;
	xCAN_WITH_PAYLOAD_AND_CALLBACK_TYPE xCan1_FrameToProcess;
	volatile uint32 *pu32TaskIsRunning;
	portTickType xLastWakeTime;

	//uint8 u8TxSuccess;
	uint8 u8ReqTransmit;
	uint8 index_loop_1;

	uint32 data_setting_battery;

	xPID_DATA_LINK_WITH_DEVICE xPID_DataWrite;
	xAPPCAN1_DATA xAppCan1_Data;

	// Set our local pointer to reference the incoming memory location to give the task access to the watchdog variable
	pu32TaskIsRunning = (volatile uint32 *)pvParams;

	xLastWakeTime = xTaskGetTickCount();

	xCan1_FrameToProcess.u8Data[0] = 0;
	xCan1_FrameToProcess.u8Data[1] = 0;
	xCan1_FrameToProcess.u8Data[2] = 0;
	xCan1_FrameToProcess.u8Data[3] = 0;

	for(;;)
	{

		u8ReqTransmit = FALSE;

		/****************************************/
		/* beginning CAN1_J35_TX_ENA_DIS_OUT_ID */
		for( index_loop_1 = 0; index_loop_1 < (CAN1_J35_TX_MSG_NB_OF_MSG-1); index_loop_1++)
		{
			if (CAN1_J35_TX_ENA_DIS_OUT_ID == Can1_TX_MsgSendIfReq[index_loop_1].u32Id)
			{
				break;
			}
		}

		xReturn = FRTOS1_xQueuePeek( qAppCan1_data_Queue, &xAppCan1_Data, 0 ); // Receive an item from a queue without the item being removed.
		if ((Can1_TX_MsgSendIfReq[index_loop_1].u8OnDemand == 1) && (xAppCan1_Data.u8All_output_off != 0xFF) )
		{
			/* initialize TX */
			xCan1_FrameToProcess.xLddCanRxFrame.MessageID = Can1_TX_MsgSendIfReq[index_loop_1].u32Id | LDD_CAN_MESSAGE_ID_EXT;
			xCan1_FrameToProcess.xLddCanRxFrame.Length = Can1_TX_MsgSendIfReq[index_loop_1].u8Lenght;
			xCan1_FrameToProcess.xLddCanRxFrame.FrameType = LDD_CAN_DATA_FRAME;
			xCan1_FrameToProcess.xLddCanRxFrame.LocPriority = 0;
			if (xAppCan1_Data.u8All_output_off == 0)
			{	/* All outputs disabled */
#if defined(USE_DBG_PIN)
				DEBUG_SPARE_GPIO2_PutVal(1);
#endif
				xCan1_FrameToProcess.u8Data[0] = 0x02;
				xCan1_FrameToProcess.u8Data[1] = 0x00;
#if defined(USE_DBG_PIN)
				DEBUG_SPARE_GPIO2_PutVal(0);
#endif
			}
			else
			{	/* All outputs enabled */
#if defined(USE_DBG_PIN)
				DEBUG_SPARE_GPIO3_PutVal(1);
#endif
				xCan1_FrameToProcess.u8Data[0] = 0x00;
				xCan1_FrameToProcess.u8Data[1] = 0x00;
#if defined(USE_DBG_PIN)
				DEBUG_SPARE_GPIO3_PutVal(0);
#endif
			}
			u8ReqTransmit = TRUE;
		}
		/* end CAN1_J35_TX_ENA_DIS_OUT_ID */
		/****************************************/

		/****************************************/
		/* beginning CAN1_J35_TX_BATCAP_ID */
		for(index_loop_1=0; index_loop_1 < (CAN1_J35_TX_MSG_NB_OF_MSG-1); index_loop_1++)
		{
			if (CAN1_J35_TX_BATCAP_ID == Can1_TX_MsgSendIfReq[index_loop_1].u32Id)
			{
				break;
			}
		}

		if( FRTOS1_xQueueReceive( qPID_BatCap_DataQueue_W, &xPID_DataWrite, 100 ) ) // Wait for up to 100mS for data.
		{
			/* 1 - DC_POWER_MONITOR_1 */
			data_setting_battery = ((xPID_DataWrite.u32Data[1] >> 16 ) & 0xFFFF) * 1000;
			if ( (data_setting_battery >= CAN1_J35_TX_BATCAP_LIM_MIN )
					&& (data_setting_battery <= CAN1_J35_TX_BATCAP_LIM_MAX)
					//&& (flag_data_test_setting_battery != CAN1_J35_TX_BATCAP_RET_VAL)
					&& (Can1_TX_MsgSendIfReq[index_loop_1].u8OnDemand == 1)
			)
			{
				/* change can be made */
				/* Note:  a management can be done, check value read on the CAN J35_RX_GET_BAT_CAPACITY and also the message J35_RX_TRANS_STATE_BAT_SET */
				/* initialize TX */
				xCan1_FrameToProcess.xLddCanRxFrame.MessageID = Can1_TX_MsgSendIfReq[index_loop_1].u32Id | LDD_CAN_MESSAGE_ID_EXT;
				xCan1_FrameToProcess.xLddCanRxFrame.Length = Can1_TX_MsgSendIfReq[index_loop_1].u8Lenght;
				xCan1_FrameToProcess.xLddCanRxFrame.FrameType = LDD_CAN_DATA_FRAME;
				xCan1_FrameToProcess.xLddCanRxFrame.LocPriority = 0;

				xCan1_FrameToProcess.u8Data[0] = (data_setting_battery & 0xFF000000) >> 24;
				xCan1_FrameToProcess.u8Data[1] = (data_setting_battery & 0x00FF0000) >> 16;
				xCan1_FrameToProcess.u8Data[2] = (data_setting_battery & 0x0000FF00) >> 8;
				xCan1_FrameToProcess.u8Data[3] = (data_setting_battery & 0x000000FF) ;

				u8ReqTransmit = TRUE;
			}
		}

		/* end CAN1_J35_TX_ENA_DIS_OUT_ID */
		/****************************************/

		/****************************************/
		/* beginning CAN1_J35_TX_ENA_DIS_OUT_ID */
		for(index_loop_1=0; index_loop_1 < (CAN1_J35_TX_MSG_NB_OF_MSG-1); index_loop_1++)
		{
			if (CAN1_J35_TX_CMD_LIGHT_1 == Can1_TX_MsgSendIfReq[index_loop_1].u32Id)
			{
				break;
			}
		}

		if ( (Can1_TX_MsgSendIfReq[index_loop_1].u8OnDemand == 1) && (AppCan1_global_data.u8Output_light_1 != 0xFF) )
		{
			/* initialize TX */
			xCan1_FrameToProcess.xLddCanRxFrame.MessageID = Can1_TX_MsgSendIfReq[index_loop_1].u32Id | LDD_CAN_MESSAGE_ID_EXT;
			xCan1_FrameToProcess.xLddCanRxFrame.Length = Can1_TX_MsgSendIfReq[index_loop_1].u8Lenght;
			xCan1_FrameToProcess.xLddCanRxFrame.FrameType = LDD_CAN_DATA_FRAME;
			xCan1_FrameToProcess.xLddCanRxFrame.LocPriority = 0;

			xCan1_FrameToProcess.u8Data[0] = AppCan1_global_data.u8Output_light_1 ;

			AppCan1_global_data.u8Output_light_1 = 0xFF;

			u8ReqTransmit = TRUE;
		}
		/* end CAN1_J35_TX_ENA_DIS_OUT_ID */
		/****************************************/



		if( ( u8ReqTransmit == TRUE ) && ( NULL != qCan1_TxQueue ) ) // Don't try to use a non-existent queue
		{
#if defined(USE_DBG_PIN)
			DEBUG_SPARE_GPIO4_PutVal(1);
#endif
			xReturn = FRTOS1_xQueueSendToBackFromISR( qCan1_TxQueue, &xCan1_FrameToProcess, 0 ); // queue the CAN frame
			ASSERT(xReturn == pdPASS); // If condition is false, called _IDSCore_AssertFailed() then watch memory at *file
#if defined(USE_DBG_PIN)
			DEBUG_SPARE_GPIO4_PutVal(0);
#endif
		}

		// Kick the dog
		FRTOS1_taskENTER_CRITICAL();
		*pu32TaskIsRunning = TRUE;
		FRTOS1_taskEXIT_CRITICAL();

		vTaskDelayUntil( &xLastWakeTime, ( 100 / portTICK_RATE_MS ) );
	}

	// Task function does not return
}

bool boCAN1_RxWatchdogTaskCheck(void)
{
	bool boReturn = FALSE;

	// Verify that our task is still running by testing its watchdog variable
	if( u32CAN1_RxTaskWdgCheck )
	{
		// The watchdog has changed so everything is good.
		boReturn = TRUE;
	}

	return boReturn;
}

bool boCAN1_TxWatchdogTaskCheck(void)
{
	bool boReturn = FALSE;

	// Verify that our task is still running by testing its watchdog variable
	if( u32CAN1_TxTaskWdgCheck )
	{
		// The watchdog has changed so everything is good.
		boReturn = TRUE;
	}

	return boReturn;
}

bool boCAN1_100MsWatchdogTaskCheck(void)
{
	bool boReturn = FALSE;

	// Verify that our task is still running by testing its watchdog variable
	if( u32CAN1_100MsTaskWdgCheck )
	{
		// The watchdog has changed so everything is good.
		boReturn = TRUE;
	}
	return boReturn;
}

void vCAN1_RxWatchdogTaskClear(void)
{
	// Reset the check variable so we can tell if it has been set by the next time around.
	u32CAN1_RxTaskWdgCheck = FALSE;
}

void vCAN1_TxWatchdogTaskClear(void)
{
	// Reset the check variable so we can tell if it has been set by the next time around.
	u32CAN1_TxTaskWdgCheck = FALSE;
}

void vCAN1_100MsWatchdogTaskClear(void)
{
	// Reset the check variable so we can tell if it has been set by the next time around.
	u32CAN1_100MsTaskWdgCheck = FALSE;
}


